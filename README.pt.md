[English](README.md)

        Verificatrix, assistente de demonstração para matemáticos.
        (EM ESTADO BEM INICIAL!)
        Copyright (C) 2021, Yuri da Silva.

        Este programa é um software livre: você pode redistribuí-lo e/ou
        modificá-lo sob os termos da Licença Pública Geral GNU, conforme
        publicado pela Free Software Foundation, seja a versão 3 da Licença
        ou (a seu critério) qualquer versão posterior.

        Este programa é distribuído na esperança de que seja útil,
        mas SEM QUALQUER GARANTIA; sem a garantia implícita de
        COMERCIALIZAÇÃO OU ADEQUAÇÃO A UM DETERMINADO PROPÓSITO. Veja a
        Licença Pública Geral GNU para obter mais detalhes.

        Você deve ter recebido uma cópia da Licença Pública Geral GNU
        junto com este programa. Se não, veja <https://www.gnu.org/licenses/>.

## Introdução

Um _assistente de demonstração_ é um programa de computador que lê deduções
matemáticas, escritas numa linguagem lógica completamente rigorosa,
verificando-as, e possivelmente auxiliando a escrita dessas deduções. Sendo
assim, com esforço suficiente, a compreensibilidade de demonstrações comuns
pode ser complementada pela certeza providenciada por um computador, desde que
o programa esteja corretamente escrito.

Se, por um lado, há um rápido avanço do conhecimento matemático nas décadas
passadas, cada vez mais há erros que demoram anos a serem descobertos. (Uma
lista interessante de erros que se mantiveram por décadas, muitos no século 20,
se encontra no
[MathOverflow](https://mathoverflow.net/questions/35468/widely-accepted-mathematical-results-that-were-later-shown-to-be-wrong).)
Apesar desses erros não serem catastróficas, pelo costume de confirmar
resultados de várias maneiras, pode ser frustrante ter de lidar com eles.

Sendo assim, fica cada vez mais desejável saber como escrever (de forma
prática) demonstrações rigorosas e como verificá-las automaticamente.
(Descartes aparentava querer isso muito antes de existirem computadores;
Whitehead e Russell tentaram fazer isso em papel.) E realmente é isso que
Nicolas de Bruijn tentou fazer em 1967 (com o programa Automath), além de
[muitas pessoas](https://en.wikipedia.org/wiki/Category:Proof_assistants), com
o passar das décadas, trabalhando no LCF, Nuprl, Isabelle, Coq, Metamath, Agda
e Lean.

Houve muitos avanços no conhecimento de lógica, formalizações de alguns
resultados importantes (como o teorema das quatro cores, o teorema de
Feit–Thompson e a prova da conjectura de Kepler) e alguns assistentes de
demonstração são muito usados em verificação de _hardware_. _Porém_,
assistentes de demonstração são pouco usados por matemáticos, sendo bem menos
usados do que sistemas de álgebra computacional, que surgiram quase na mesma
época. (Sistemas de álgebra computacional, apesar de tipicamente usarem
heurísticas, sem garantia de precisão de suas respostas, são muito úteis para
investigação.)

Assistentes de demonstração são pouco usados por serem difíceis (isto é, o
custo excede o benefício). Mas por que, exatamente, são tão difíceis? Não
bastaria ter um conhecimento profundo, muita organização e muito papel de
rascunho?

Decidi então investigar para saber melhor o porquê. Realmente quase não tenho
experiência em lógica, mas não gostaria de esperar outros responderem para mim.
Pelo menos, tento ficar a pesquisar artigos relevantes; apresento antes os
pensamentos que tive após essas várias leituras, os quais pretendo testar.

## Alguns pensamentos

Esta seção é resumida por:

* focar na simplicidade da implementação, para evitar erros;
* usar sistemas lógicos bem compreendidos e bem comportados;
* facilitar que terceiros implementem outros verificadores, para que erros por
  uma implementação sejam detectados a partir de outra;
* permitir o trabalho descentralizado, com foco na compatibilidade;
* separar a matemática dos detalhes notacionais.

A esses cinco itens, que acho essenciais para que a formalização da matemática
seja prática, ainda não é dada atenção suficiente.

### Simplicidade de implementação

Assim como matemáticos têm o costume de conhecer as demonstrações dos teoremas
que citam (ou pelo menos parte), seria razoável que lessem o código-fonte de um
assistente de demonstração antes de usá-lo. Para isso ser prático, o
código-fonte deve ser breve, corresponder em parte à linguagem de matemáticos
não programadores e ser bem explicado. (Muitos assistentes de demonstração têm
códigos-fonte com um comprimento da ordem de dezenas de milhares de linhas, com
poucos comentários, e sem ter resumo de sua estrutura.)

Um receio típico de matemáticos em relação a assistentes de demonstração é as
implementações serem incorretas. Esses receios podem ser mitigados não só por
incentivar a leitura do código-fonte, mas também por usar uma linguagem de
programação com um sistema bem elaborado de detecção de enganos; de
preferência, tem de ser algo como “se compila, funciona”. Com base nisso e em
outros aspectos, a melhor opção é a linguagem de programação Haskell.

### Fundações

Um dos formalismos lógicos mais adequados a assistentes de demonstração (e
também um dos mais estudados) é a _teoria de tipos Martin–Löf com famílias
indutivas e infinitos universos_ (MLWω). (Para mais detalhes, ver a
[Wikipédia](https://en.wikipedia.org/wiki/Type_theory).) É similar a teorias de
conjuntos, mas inclui o conceito de “redução”, um método totalmente sistemático
de automatizar “provas triviais”; por exemplo, a prova de que pi é menor do que
3.1416 é bem breve em MLWω, pois basta calcular pi a precisão suficiente. Ainda
mais, MLWω assemelha-se à matemática informal, em especial os aspectos
computacionais, e é razoavelmente fácil de implementá-la num assistente de
demonstração.

Extensões de MLWω são os formalismos usados em Coq, Agda e Lean.

<details>
<summary>Clique para ver pensamentos sobre a “igualdade proposicional” (requer
conhecimentos em MLWω).</summary>

A teoria MLWω tem um conceito de “igualdade proposicional”, exigindo provas,
opondo-se à “igualdade definicional”, cuja determinação é automática e usa a
redução. Porém, a igualdade proposicional em MLWω é inutilizável para a maioria
dos tipos; por exemplo, não há como provar a “extensionalidade funcional”, isto
é, que $(∀ x, f x = g x) → f = g$. Foram investigadas várias extensões
incompatíveis que pudessem implicar esse princípio, uma delas sendo a “teoria
univalente de tipos”, ou “teoria homotópica de tipos” (HOTT). Esta é uma teoria
bem interessante, pois as igualdades proposicionais em geral acabam coincidindo
com o conceito “correto”; por exemplo, igualdade proposicional entre grupos é
isomorfismo de grupos, e igualdade proposicional entre espaços métricos é
bijeção isométrica. Porém, ela por si só perde as propriedades computacionais
da MLWω. É possível estendê-la mais ainda, dando as “teorias cúbicas de tipos”,
que recuperam as propriedades computacionais, sob a pena de serem
consideravelmente mais complicadas.

As pessoas do Lean decidiram usar uma extensão de MLWω com “quocientes
definicionais”, que também perde as propriedades computacionais.

Diante disso, enquanto não descobrirem uma maneira mais simples de resolverem
esses problemas, aparenta ser melhor ter cautela e imitar a matemática
informal, que define o conceito de igualdade para cada conceito em
consideração. Isso é o método dos “conjuntos de Bishop”. Algumas pessoas (numa
[discussão acalorada](https://github.com/coq/coq/issues/10871)) demonstram
desconforto em usá-los em assistentes de demonstração, mas esse conforto
aparenta-me ser resolvível por uma boa interface gráfica.

</details>

### Bibliotecas de matemática formalizada

Vamos agora tratar de detalhes mais burocráticos. Sugiro um formato de
armazenamento de demonstrações (não de leitura humana!) como a seguir.

* Formato bem simples, de modo a facilitar a escrita de implementações por
  outras pessoas; deve ser possível escrever uma breve implementação que só
  verifica, sem auxiliar na escrita de demonstrações.
* Formato que só contém os detalhes matemáticos, não os detalhes de
  apresentação (notações e nomes).
* Identifica os termos (definições e demonstrações) _estavelmente_, isto é, sem
  “quebrar” ao atualizar versões; a ideia é identificar termos por meio de
  _hashes_ criptográficas, globalmente determinadas e únicas na prática (como
  em [Unison](https://unisonweb.org)), e incentivar matemáticos a nunca remover
  termos uma vez postos, nem modificá-los (exceto por detalhes não matemáticos,
  como notações e nomes). (Modificar o termo alteraria a _hash_.)

É uma reclamação típica a incompatibilidade das demonstrações entre diferentes
assistentes de demonstração e até entre versões diferentes. Espero que esses
três itens resolvam as questões de compatibilidade, que são muito importantes
para a construção _descentralizada_ de grandes bibliotecas de matemática
formalizada, imitando a matemática informal. O último item, em especial, pode
parecer uma regra muito rígida, mas notar que a matemática não muda, o que muda
são os nossos interesses; duas definições diferentes da mesma coisa recebem
_hashes_ diferentes, e isso não é problema, pois é digno provar a equivalência
entre elas.

Claro que pessoas não vão nomear termos por sequências aleatórias de
caracteres, nem usar notações ilegíveis. Por isso, sugiro que, ao lado dos
ficheiros de demonstração, haja informações como nomes e descrições, em várias
línguas, que podem ser alteradas, pois elas não são incluídas nas _hashes_. Já
as notações seriam específicas a cada implementação e a cada configuração;
matemáticos teriam a opção de usar a mesma notação ou não, e, independentemente
da escolha, manteriam toda compatibilidade com o trabalho de outros
matemáticos. Em suma, isso é tudo detalhes de interface gráfica.

### Outros pensamentos

* Às vezes, imitando-se programação, procura-se “esconder definições”, mas isso
  é inadequado na matemática; às vezes, até detalhes de demonstrações são
  importantes (mesmo que muitas vezes ”não importe qual demonstração”). Se
  quiser não depender de uma definição específica, parametrize.
* Não tem problema definir uma mesma coisa duas vezes; serão sempre
  intercambiáveis. Porém, isso não vale para muitos assistentes de
  demonstração, que empregam “tipagem nominal”. Argumentos por programadores a
  favor para tipagem nominal, como “evitar confundir coisas acidentalmente
  isomorfas”, não se aplicam a matemática pois tudo é demonstrado.
* Há o conceito de “táticas”, que é um algoritmo, fora do formalismo lógico,
  para gerar demonstrações. Muitas vezes táticas deixam a verificação mais
  lenta, por causa do trabalho combinado de gerar e checar, e às vezes a
  geração é feita repetidamente; por isso, o formato de armazenamento de
  demonstração não deve incluir táticas. Porém, táticas podem ser uma útil
  funcionalidade adicional de implementações, complementando as propriedades
  computacionais da MLWω.
* A lógica categórica apresenta maneiras de “internalizar” provas em teorias de
  tipos em diversas categorias para obter outros teoremas interessantes mais
  facilmente, mas não sei se (ou como) seria uma boa ideia aproveitar isso em
  assistentes de demonstração.
* Seria muito útil ter um livro explicando como escrever demonstrações
  totalmente rigorosas; por exemplo, como demonstrar o teorema da curva de
  Jordan sem fazer desenhos, ou como responder à questão de [Darij
  Grinberg sobre rigor em algoritmos combinatóricos](https://mathoverflow.net/questions/309191/how-to-be-rigorous-about-combinatorial-algorithms).
  No máximo há uns livros breves explicando como usar alguns assistentes de
  demonstração, que focam demasiado em coisas simples. A escrita de tal
  material didático aparenta-me ser uma urgência, mas difícil de resolver.

## Plano para agora

A fim de saber o porquê da dificuldade em formalizar ou até contribuir para a
formalização da matemática, pretendo fazer o seguinte.

- [x] Fazer uma implementação bem rudimentar e mínima.
- [ ] Especificar o formato “VERI”.
- [ ] Fazer uma interface gráfica.
- [ ] Polir as coisas.
- [ ] Experimentar e identificar motivos de desconforto.
- [ ] ???

Estou aberto a sugestões; porém, nem sei ainda mexer direito com Git e essas
coisas de colaboração à distância.
