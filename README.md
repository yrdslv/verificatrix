2023-11-10.

It's been one year and half since the last commit. But the plans haven't stopped, as new ideas got born (but not matured).

It would be "hypocritical" to make a proof checker without decently checking its source code, and now I consider that writing it in Haskell is not enough. With the prospect of parametrizing the reduction method (possibly including some form of optimal reduction), the source would become even more complex and have its chance of critical mistakes increased. It would be even more complex after extending the type theory, adding some form of cubical types. (And the compiler GHC is very very large and could generate incorrect code.)

(I changed my mind about univalence, and now I think it's necessary, because it organizes the already needed work of proving coherences. That work is for instance proving that certain composition A → B → C is the same as A → C, which is frequently swept under the carpet, in sentences such as "let's consider A as included in B" or "identify A with B".)

Decently checking the source would be writing it in a simpler proof checker, which in turn would be written in an even simpler one, and so on, until I am satisfied. The simplest proof checker would be written in assembly code (although I wouldn't make an OS kernel nor an assembler nor a CPU), and have a very simple logic system, such as Primitive Recursive Arithmetic, where certain terms would be able to be computed (so to permit input, output and allocation). A Metamath-style checker would be a good idea, and even if it could be made even simpler (omitting variables, that is, using the internal logic of products and NNO), such an extreme simplicity would make the proofs too long.

The Primitive Recursive Arithmetic is a very weak theory, but (hopefully) strong enough to prove properties like "each arithmetical Π-1 sentence provable by MLIω +cubical +optimal-reduction is provable by ZF too". It would be enough to convince me that the specific implementation is correct (without proving that it is ω-consistent); proving subject reduction and canonicity would be an extra point.

Formalizing metalogical properties would be without doubt a big work, but the papers on those type systems can not be trusted to be completely correct (unless they were formally verified) and I cannot trust myself to have understood them correctly.

How much time it would take, I don't guess.

---

[Português](README.pt.md)

        Verificatrix, proof assistant for mathematicians.
        (STILL VERY RUDIMENTARY!)
        Copyright (C) 2021, Yuri da Silva.

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Introduction

A _proof assistant_ is a computer software which reads mathematical deductions,
written in a completely rigorous logical language, verifying them, and possibly
aiding the users in writing these deductions. Thus, with sufficient effort, the
understandability of commons demonstrations can be complemented by the
certainty provided by a computer, as long as that the software is correctly
written.

If, on the one hand, there is a quick advance in the mathematical knowledge in
the past decades, increasingly there are mistakes which take years to be
discovered. (An interesting list of mistakes which stood for decades, many in
20th century, is on
[MathOverflow](https://mathoverflow.net/questions/35468/widely-accepted-mathematical-results-that-were-later-shown-to-be-wrong).)
Despite that these mistakes are not catastrophic, due to habit of confirming
results in many ways, dealing with them may be frustrating.

Therefore, it is increasingly desirable to know how write (in a practical way)
rigorous demonstrations and how to verify them automatically. (Descartes
apparently wanted this well before the existence of computers; Whitehead and
Russell tried that on paper.) And really that is what Nicolas de Bruijn tried
in 1967 (with the software Automath), besides [many
logicians](https://en.wikipedia.org/wiki/Category:Proof_assistants) by the
decades working on LCF, Nuprl, Isabelle, Coq, Metamath, Agda and Lean.

There were many advances in logic, formalizations of some important results
(such as the four color theorem, the Feit–Thompson theorem and the proof of
Kepler conjecture) and some proof assistants are much used in hardware
verification. _However_, proof assistants are still little used by
mathematicians, much less than computational algebra systems, which were
created in almost the same year. (Computational algebra systems, despite the
use of heuristics, without guarantee of results exactness, are very useful for
investigation.)

Proofs assistants are still little used because of difficulty (that is, the
cost exceeds the gain). But why, exactly, they are so difficult? Isn't enough
to have deep knowledge, much organization and much sketch paper?

I then decided to analyze in order to better know why. Really I have almost no
experience in logic, but I would not want to wait others to answer me that
question. At least, I try to keep searching many relevant articles; firstly,
after that much reading, I present my thoughts, which I want to test.

## Some thoughts

This section is summarized by:

* focus on the implementation simplicity, avoiding bugs;
* use logical systems which are well understood and well behaved;
* ease alternative implementations by third-parties, so that bugs of a first
  implementation can be detected from comparison with a second;
* permit decentralized work, with focus on compatibility;
* separate mathematics from notational details.

For these five items, which I consider essential to the practicality of
mathematics formalization, still insufficient attention is given.

### Implementation simplicity

Similarly to when mathematicians have the habit of knowing the proofs for the
theorems they cite (or at least a part), it would be reasonable to read the
source code of a proof assistant before using it. In order to that reading be
practical, the source code has to be brief, to correspond in part to a language
used by mathematicians which are not programmers, and to be well commented.
(The source codes of many proof assistants have dozens of thousands lines, with
few comments, and without summary of code organization.)

A typical mathematicians' concern about proof assistants is that the
implementations are wrong. That concern can be mitigated not only by
encouraging to read the source code, but also by using a programming language
with a pretty elaborate system for mistake detection; preferentially, something
like “if it compiles, it works”. With that and other considerations, the best
option is the Haskell programming language.

## Foundations

One of the logical frameworks which are more adequate for proof assistants (and
also one of the most studied) is the _Martin–Löf type theory with type families
and infinitely many universes_ (MLWω). (For more details, read the
[Wikipedia](https://en.wikipedia.org/wiki/Type_theory).) It resembles the set
theories, but also includes a “reduction” concept, a completely systematic
method for automatizing “trivial proofs”; for instance, the proof that pi is
less than 3.1416 is very brief in MLWω, as it is enough to compute pi to
sufficient precision. Even more, MLWω is similar to informal mathematics, in
special the computational aspects, and is reasonably easy to implement the
theory in proof assistants.

MLWω extensions are the systems used in Coq, Agda and Lean.

<details>
<summary>Click here for thoughts about “propositional equality” (requires
knowledge in MLWω).</summary>

The MLWω theory has a “propositional equality” concept, demanding proofs,
differently from “definitional equality”, whose determination is automatic and
uses the reduction. However, the propositional equality in MLWω is unusable for
most types; for example, we can't prove the “functional extensionality”, that
is, that $(∀ x, f x = g x) → f = g$. Logicians investigated many pairwise
incompatible extensions which imply that principle, one of them being the
“univalent type theory”, or “homotopy type theory” (HOTT). That is a very
interesting theory, because therein propositional equalities in general
coincide with the “correct” concepts; for instance, propositional equality
between groups is group isomorphism, and propositional equality between metric
spaces is isometric bijection. However, HOTT by itself loses the computational
properties of MLWω. It is possible to extend it even more, giving the “cubical
type theories”, which recover the computational properties, but they are
considerably more complex.

The Lean creators decided to use an extension of MLWω by “definitional
quotients”, which also loses the computational properties.

Given these considerations, while a simpler way to amend the propositional
equality is not found, it is apparently better to be cautious and to imitate
informal mathematics, in which a new equality is defined for each class of
objects. That is the method of “setoids”. Some people (in a [heated
discussion](https://github.com/coq/coq/issues/10871)) have discomfort in using
them in proof assistants, but that discomfort seems for me to be resolvable by
a good graphical interface.

</details>

### Libraries of formalized mathematics

Let us treat now about more bureaucratic details. I suggest a proof storage
file format (not for human reading!) as follows.

* A very simple format, in order to ease implementations by third-parties; it
  shall be practical to write a small implementation which only verifies,
  without aiding in the creation of formalized proofs.
* A format which only includes mathematical details, not presentation details
  (notations and names).
* A format identifying the terms (definitions and demonstrations) _stably_,
  that is, without “breaking” on version updates; the idea is to identify terms
  by its cryptographic hashes, globally determined and practically unique (as
  [Unison](https://unisonweb.org)), and encourage mathematicians to never
  remove terms once inserted, nor modify them (except by non mathematical
  details, such as notations and names). (Modifying the term would change its
  hash.)

A common complaint is incompatibility of demonstrations between different proof
assistants and even between its different versions. I hope that these three
items are a solution for the compatibility problems, which is very important
for the _decentralized_ construction of large formalized mathematics libraries,
like it happens with informal mathematics. The last item, in special, may
appear to be excessively rigid, but note that the mathematics does not change,
what may change are our interests; two different definitions of the same thing
have different hashes, and that is not a problem, because the equivalence
between them deserves a proof.

Of course people will not name terms by random characters sequences, nor use
illegible notations. Thus, I suggest to, besides the demonstration files, store
some information such as names and descriptions, in many languages, which can
be modified, as they are not included in the hashes. Even more, the notations
could be specific to each implementation and each setting; mathematicians could
use different notations from his colleagues or not, and, independently of the
choice, would maintain complete compatibility with others' work. On the whole,
this is all details about graphical interfaces.

### Other thoughts

* Sometimes, imitating programming, it is attempted to “hide definitions”, but
  that is inadequate with mathematics; sometimes, even details in proofs are
  relevant (despite that many times “which proof is chosen does not matter”).
  If you want to not depend on a specific definition, parametrize.
* There is no problem in defining the same thing two times; they are always
  interchangeable. However that does not hold for many proof assistants, which
  use “nominal typing”. Arguments by programmers in support of nominal typing,
  such as “avoid mistaking accidentally isomorphic things”, do not apply to
  mathematics as everything is proved.
* There is the concept of “tactics”, which are algorithms, outside the logical
  system, for generating proofs. Frequently tactics make the verification
  slower, because of the combined work of generating and checking, and
  sometimes that generation is done repeatedly; thus, the proof storage file
  format shall not include tactics. However, tactics may be a useful additional
  feature of implementations, complementing MLWω computational properties.
* Categorical logic presents ways to “internalize” proofs written in type
  theories in many categories, in order to obtain interesting theorems easily,
  but I do not known if (or how) it would be a good idea to take advantage of
  that in proof assistants.
* It would be very useful to have a book explaining how to write totally
  rigorous proofs; for instance, how to prove the Jordan curve theorem without
  drawings, or how to answer [Darij Grinberg's question on rigor about
  combinatorial
  algorithms](https://mathoverflow.net/questions/309191/how-to-be-rigorous-about-combinatorial-algorithms).
  At most there are brief books explaining how to use some proof assistants,
  containing only simple things. Writing such a textbook appears an urgency for
  me, but it would be a hard job.

## Plan for now

In order to know why that difficulty in formalizing mathematics, I plan the
following.

- [x] Make a very rudimentary and minimal implementation.
- [ ] Specify the “VERI” format.
- [ ] Make a graphical interface.
- [ ] Polish the things.
- [ ] Experiment and identify discomfort reasons.
- [ ] ???

I am open to suggestions; however, I still lack knowledge in Git and those
things for collaboration from distance, so no pull requests, only issues.
