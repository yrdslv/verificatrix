---
title: Reading Verificatrix source code
author: Yuri da Silva
abstract: |
  The proof assistant Verificatrix has a simple source code, hopefully readable
  by non programmers. In order to enhance your understanding, this document
  explain basic tricks of Haskell programming. Basic category theory knowledge
  is needed.
date: 2021
---

<!--
Command:
    pandoc -""-pdf-engine xelatex ….md -o ….pdf
-->

# Introduction

Verificatrix is a proof assistant, that is, a software which can be used to
automatically verify the correctness of proofs written in a logical notation,
and which can be used to ease the translation from informal reasoning to that
logical notation. More details in the documentation; here, we explain how to
understand the implementation.

It is very important that you read all the source code, as Verificatrix shall
not be treated like a black box.

Verificatrix is written in Haskell. We chose that programming language because:

* it has a brief notation, resembling mathematics;
* it is a functional language with lazy evaluation, that, as you will see,
  eases writing algorithms involving logical languages manipulation.

If you want to experiment with Haskell programs (and not only read them), you
can use the Glasgow Haskell Compiler (GHC); in Debian and Ubuntu, install with
`apt install ghc` as root. Here we do not explain how to use that compiler;
more information at
<https://haskell.org/ghc>.

It may be useful to search definitions with Hoogle. Access in
<https://hoogle.haskell.org>
or install (Debian-like) with `apt install hoogle`, and individual
documentation packages `libghc-...-doc`.

## GHC bugs

We use Haskell partially to avoid mistakes (as the language is very advanced,
we can ask many things to be checked before code generation). But GHC has a
very complex (but readable) source code, and many bugs have been found and will
be found (as it is not formalized). It is reasonable to worry about bugs in GHC
which cause incorrect proofs to be accepted. Thus, we have to be sceptic,
analyzing output code and testing Verificatrix in many ways. You may also want
to write an alternative implementation in other programming language.

# Types in Haskell

The page <https://haskell.org> describes Haskell as “an advanced, purely
functional programming language”, for “declarative, statically typed code”. Let
us see what this means.

In Haskell, we write `t :: T` to denote that a _term_ `t` has _type_ `T`. We
can ask many checks to be performed in the type language, so that GHC
refuses to generate the output code if not all checks pass.

The simplest terms in Haskell are _parametrically polymorphic functions_, as
below.

```haskell
id :: a -> a
id = \ x -> x

const :: a -> b -> a
const = \ x _ -> x

ap :: (a -> b -> c) -> (a -> b) -> (a -> c)
ap = \ s f x -> s x (f x)
```

Here, `\ x -> ...` is the “lambda notation” for functions. Reasonably,
`(\ x -> e(x)) :: a -> b` iff `x :: a` implies `e(x) :: b`.
Also, the rule for function application is, if `f :: a -> b` and `x :: a`, then
`f x :: b`.

The definitions above can be abbreviated.

```haskell
id x = x
const x _ = x
ap s f x = s x (f x)
```

We can name functions with non letter symbols, as in the function composition.

```haskell
(.) :: (b -> c) -> (a -> b) -> (a -> c)
g . f = \ x -> g (f x)
infixr 9 . -- “Binds less” than application (precedence 10).

($) :: (a -> b) -> a -> b
f $ x = f x
infixr 0 $  -- Useful for avoiding parentheses.
```

We can define our own _algebraic data types_ (ADTs), which are combinations of
products, coproducts and some form of recursion. The elements are scrutinized
by case analysis.

```haskell
data Bool = False | True

if' :: Bool -> a -> a -> a
if' b x y = case b of
  False -> y
  True -> x

(||) :: Bool -> Bool -> Bool
infixr 2 ||
True || _ = True
_    || b = b

data () = ()
data (,) a b = (,) a b

uncurry :: (a -> b -> c) -> (a, b) -> c
uncurry f (a, b) = f a b
-- No problem in the term and type having the same name.
-- Namespaces are separate.

data Void
absurd :: Void -> a
absurd x = case x of

data Either a b = Left a | Right b
either :: (a -> b) -> (b -> c) -> Either a b -> c
either f _ (Left a)  = f a
either _ g (Right b) = g b
```

Consecutive patterns are tried (semantically) left to right.

```haskell
h False False = False
h _     _     = True

-- Means:
h x y = case x of
  False -> case y of
    False -> False
    True -> True
  True -> True
-- Asymmetric; same as @(||)@ above.
-- Not strict in @y@ (see later).
```

Newtypes are like single constructor and single argument ADTs, “enclosing” a
type, but _in runtime_ there is no distinction between the enclosed type and
the newtype. They are useful for distinguishing, e.g., the integers as addition
monoid and the integers as product monoid, and for giving instances (see later)
for these variants.

```haskell
newtype ListTuple a = ListTuple {unListTuple :: [(a, a)]}
-- Zero-cost coercions (that is, use zero memory and zero time):
ListTuple :: [(a, a)] -> ListTuple a
unListTuple :: ListTuple a -> [(a, a)]
```

## Lists

A very important type is the simply linked list type.

```haskell
data List a = [] | a : List a
type [a] = List a
type [] = List
infixr 5 :
```

The term `[x, y]` means `x : y : []`. We have induction:

```haskell
foldr :: (a -> b -> b) -> b -> [a] -> b
foldr _ n []       = n
foldr f n (x : xs) = f x (foldr f n xs)
```

For example, if `infixr 5 &`, then `foldr (&) n (a : b : [])` is equal to
`a & b & n`.

We could use it to define length of lists as
`length = foldr (\ _ n -> n + 1) (0 :: Int)`. (Here, `Int` is the type of
integers which fit in a CPU register.) But see the evaluation:

```haskell
length [a, b, c] = length [b, c] + 1
                 = (length [c] + 1) + 1
                 = ((length [] + 1) + 1) + 1
                 = ((0 + 1) + 1) + 1 -- Wastes memory.
                 = 3
```

The following is better:

```haskell
length = lenAcc (0 :: Int)
  where
  lenAcc :: Int -> [a] -> [a] -> Int
  lenAcc !n []      = n
  lenAcc !n (_ : l) = lenAcc (n + 1) l
  -- “Bang notation”: @lenAcc !n ...@ means that, if the
  -- image of @lenAcc@ is to be evaluated, then @n@ is also
  -- to be evaluated (immediately, if GHC is not dumb).

length [a, b, c] = lenAcc 0 [a, b, c]
                 = lenAcc 1 [b, c]
                 = lenAcc 2 [c]
                 = lenAcc 3 []
                 = 3
```

The following operations with lists are useful:

```haskell
map :: (a -> b) -> [a] -> [b]
map _ []       = []
map f (x : xs) = f x : map f xs

foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' f = go
  where
  go !n []       = n
  go !n (x : xs) = go (f n x) xs

filter :: (a -> Bool) -> [a] -> [a]
filter _ [] = []
filter p (x : xs)
  | p x       = x : filter p xs
  | otherwise = filter p x

or :: [Bool] -> Bool
or = foldr (||) False
-- Why not @foldl'@?

any :: (a -> Bool) -> [a] -> Bool
any p x = or (map p xs)
```

It is very useful that in Haskell we postpone evaluations by default; if it
were not, the function `any` would evaluate the entire list, even if we used
only a part.

### List fusion

Which is more efficient, `map g . map f` or `map (g . f)`? Maybe the second
option, because the first traverses a list two times? But both generate the
same code, due to _list fusion_!

The trick is to consider the operations:

```haskell
foldr :: (a -> b -> b) -> b -> [a] -> b
foldr k z = go
  where
  go []       = z
  go (y : ys) = y `k` go ys

build :: (forall b. (a -> b -> b) -> b -> b) -> [a]
build g = g (:) []
```

We have `foldr k z (build g) = g k z`. Why? (Not exactly correct; see
reference[^buildseq] if interested.)

[^buildseq]: _Correctness of short cut fusion_, Haskell Wiki (april 2016), [wiki.haskell.org](https://wiki.haskell.org/Correctness_of_short_cut_fusion).

Many operations, such as `map` and `any`, can easily be expressed in terms of
`foldr` and `build`. Given the `foldr`/`build` rule, the compiler can easily
derive many other rules. Try deriving the `map` composition rule!

Each application of the `foldr`/`build` rule eliminates one list, and we can
write elegant functional code, without performance impact.

More informations:

* _A short cut to deforestation_, A Gill, J Launchbury, SL Peyton Jones (1993),
  ACM Press, in
  <https://www.microsoft.com/en-us/research/publication/a-short-cut-to-deforestation/>.

There are other fusion methods, such as _stream fusion_.

## Type class

Try to define a function which says if an element is in a list. Maybe you got:

```haskell
elemBy :: (a -> a -> Bool) -> a -> [a] -> Bool
elemBy eq a = any (eq a)
```

We would have to pass that `eq` around, but letting it implicit is better. To
do this, we use type classes.

```haskell
class Eq a where
  (==) :: a -> a -> Bool

elem :: Eq a => a -> [a] -> Bool
elem a = any ((==) a)
```

Using these classes is a matter of defining instances.

```haskell
instance Eq Bool where
  True  == True  = True
  False == False = True
  _     == _     = False

-- This is a function of “dictionaries”.
instance Eq a => Eq [a] where
  []       == []       = True
  (x : xs) == (y : ys) = x == y && xs == ys
  _        == _        = False
```

If we open GHC interactive and ask the type of `2`, it will answer `Num a =>
a`. In fact, we have the class below.

```haskell
class Num a where
  (+), (-), (*) :: a -> a -> a
  negate, abs, signum :: a -> a
  fromInteger :: Integer -> a
```

Then, `2` means `fromInteger (2 :: Integer)`.

Some instances can be derived automatically.

```haskell
data Either a b = Left a | Right b
  deriving (Eq, Ord, Show)
```

You can only give instances for applications `T a ... z`, where `T` is a type
constructor. For example, you can give a instance for `Either a b`, `Either a`
and `Either`; if you want to, e.g., change the order of the variables, a
newtype is needed.

# Functors and monads

Haskell programmers like to use mathematical abstractions (mainly from
category theory). We have functors, applicative functors and monads.

```haskell
class Functor f where
  fmap :: (a -> b) -> f a -> f b

class Functor f => Applicative f where
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b

class Applicative f => Monad f where
  (>>=) :: f a -> (a -> f b) -> f b
```

(Read `Functor f` implies `Applicative f` if we have `pure` and `(<*>)`
satisfying conditions etc.) The operation `(>>=)` is the Kleisli adjunction
analogue. Here are some instances.

```haskell
(++) :: [a] -> [a] -> [a]
[]       ++ ys = ys
(x : xs) ++ ys = x : (xs ++ ys)

instance Functor [] where
  fmap = map
instance Applicative [] where
  pure x = [x]
  []       <*> _  = []
  (f : fs) <*> xs = map f xs ++ (fs <*> xs)
instance Monad [] where
  []       >>= _ = []
  (x : xs) >>= f = f x ++ (xs >>= f)
```

```haskell
instance Functor (Either a) where
  fmap _ (Left x) = Left x
  fmap f (Right y) = Right (f y)
instance Applicative (Either a) where
  pure = Right
  Left e  <*> _ = Left e
  Right f <*> r = fmap f r
instance Monad (Either e) where
  Left l  >>= _ = Left l
  Right r >>= k = k r

instance Functor ((->) r) where
  fmap = (.)
instance Applicative ((->) r) where
  pure = const
  f <*> g = \ x -> f x (g x)
instance Monad ((->) r) where
  f >>= k = \ r -> k (f r) r
```

```haskell
class Semigroup a where
  (<>) :: a -> a -> a
  sconcat :: NonEmpty a -> a
  stimes :: Integral b => b -> a -> a

class Semigroup a => Monoid a where
  mempty :: a
  mconcat :: [a] -> a

instance Functor ((,) a) where
  fmap (x, y) = (x, f y)
instance Monoid a => Applicative ((,) a) where
  pure x = (mempty, x)
  (u, f) <*> (v, x) = (u <> v, f x)
instance Monoid a => Monad ((,) a) where
  (u, a) >>= k
    | (v, b) <- k a = (u <> v, b)
```

## Laws for classes

In category theory, functors have to preserve compositions and monads have to
satisfy the associativity and unit laws. Haskell programmers also like to
specify laws that type class members satisfy, but the compiler will not check
them (unless you use advanced tricks, such as “dependent types”); you can write
them in the pragma `{-# RULES ... #-}` for specific instances, as rewrite rules
(beware non normalization or non confluence!). Below are the rules for some type
classes.

| Functor law | `Functor f`                          |
|-------------|--------------------------------------|
| Identity    | `fmap id x = x`                      |
| Composition | `fmap (g . f) x = fmap g (fmap f x)` |

| Applicative law | `Applicative f`                                |
|-----------------|------------------------------------------------|
| Identity        | `pure id <*> u = u`                            |
| Composition     | `pure (.) <*> u <*> v <*> w = u <*> (v <*> w)` |
| Homomorphism    | `pure f <*> pure x = pure (f x)`               |
| Interchange     | `u <*> pure y = pure ($ y) <*> u`              |

| Monad law     | `Monad f`                                  |
|---------------|--------------------------------------------|
| Left unit     | `pure x >>= k = k x`                       |
| Right unit    | `m >>= pure = m`                           |
| Associativity | `(m >>= k) >>= h = m >>= \ x -> k x >>= h` |

We can prove that `fmap` can be coherently defined in terms of `(<*>)` and
`pure`, which can also be coherently defined in terms of `pure` and `(>>=)`.
(So that condition `Functor f` in the definition of `Applicative f` is no loss
of generality, and the same for `Monad f`.)

Given `Applicative f`, define `fmap f x = pure f <*> x`. Thus:

```haskell
-- Identity.
fmap id x = pure id <*> x
          = x -- By identity for applicative.

-- Composition.
fmap (g . f) x = pure (g . f) <*> x
               = pure ((.) g) <*> pure f <*> x -- By homomorphism.
               = pure (.) <*> pure g <*> pure f <*> x -- Idem.
               = pure g <*> (pure f <*> x) -- By composition.
               = fmap g (fmap f x)
```

Given `Monad f`, define `u <*> m = u >>= \ f -> m >>= \ x -> pure (f x)`:

```haskell
-- Identity.
pure id <*> m
  = pure id >>= \ f -> m >>= \ x -> pure (f x)
  = m >>= \ x -> pure (id x)
  = m >>= \ x -> pure x
  = m
```

```haskell
-- Composition.
pure (.) <*> u <*> v <*> w
  = pure (.) <*> u <*> v >>= \ f -> w >>= \ x -> pure (f x)
  = (pure (.) <*> u >>= \ g -> v >>= \ y -> pure (g y)) >>=
    \ f -> w >>= \ x -> pure (f x)
  = ((pure (.) >>= \ h -> u >>= \ z -> pure (h z)) >>=
    \ g -> v >>= \ y -> pure (g y)) >>=
    \ f -> w >>= \ x -> pure (f x)
  = ((u >>= \ z -> pure ((.) z)) >>= -- By left unit.
    \ g -> v >>= \ y -> pure (g y)) >>=
    \ f -> w >>= \ x -> pure (f x)
  = (u >>= \ z -> v >>= \ y -> pure (z . y)) >>= -- Associativity.
    \ f -> w >>= \ x -> pure (f x)
  = u >>= \ z -> (v >>= \ y -> pure (z . y)) >>= -- Idem.
    \ f -> w >>= \ x -> pure (f x)
  = u >>= \ z -> v >>= \ y -> pure (z . y) >>= -- Idem.
    \ f -> w >>= \ x -> pure (f x)
  = u >>= \ z -> v >>= \ y -> w >>= \ x -> pure (z (y x))
  = u >>= \ z -> v >>= \ y -> w >>= \ x -> pure (y x) >>=
    \ t -> pure (z t)
  = u >>= \ z -> v >>= \ y -> (w >>= \ x -> pure (y x)) >>=
    \ t -> pure (z t)
  = u >>= \ z -> (v >>= \ y -> w >>= \ x -> pure (y x)) >>=
    \ t -> pure (z t)
  = u >>= \ z -> v <*> w >>= \ t -> pure (z t)
  = u <*> (v <*> w)
```

```haskell
-- Homomorphism.
pure f <*> pure x
  = pure f >>= \ g -> pure x >>= \ y -> pure (g y)
  = pure x >>= \ y -> pure (f y)
  = pure (f x)
```

```haskell
-- Interchange.
u <*> pure y
  = u >>= \ f -> pure y >>= \ x -> pure (f x)
  = u >>= \ f -> pure (f y)
  = u >>= \ f -> pure (($ y) f)
  = pure ($ y) >>= \ g -> u >>= \ f -> pure (g f)
  = pure ($ y) <*> u
```

(Above, we supposed that `pure = \ x -> pure x`, that is, `pure` is not bottom
(see denotational semantics later); such strictness hypotheses are common in
Haskell reasoning.)

Also, `fmap f m = pure f <*> m = m >>= pure . f`.

While `u <*> m = u >>= \ f -> m >>= \ x -> pure (f x)`, `u <*> m` may not be
equal to `m >>= \ x -> u >>= \ f -> pure (f x)`. For example, if `f = Either
e`, `u = Left a`, `m = Left b`, then `u <*> m` is `Left a`, but the right hand
is `Left b`.

## “Do” notation

A useful monad is `IO`. As Haskell is _pure_, any definition which manipulates
global state (not _purely_ mathematical) shall be marked with `IO`. (Unless you
use “unsafe” operations.) A typical program is:

```haskell
type String = [Char] -- Slow strings; avoid.
getLine :: IO String
putStrLn :: String -> IO ()
main = pureStrLn "Name?" >> getLine >>= putStrLn . ("Hi " <>)

-- “Do” notation:
main = do
  putStrLn "Name?"
  name <- getLine
  putStrLn ("Hi " <> name)
```

The “do” notation applies to each monad, and is translated as follows:

```haskell
do { m } = m
do { m ; ... } = m >> do { ... }
do { a <- m ; ... } = m >>= \ a -> do { ... }
```

| Monad rules   | `Monad f`                                           |
|---------------|-----------------------------------------------------|
| Left unit     | `do { y <- pure x ; k y } = do { k x }`             |
| Right unit    | `do { x <- m ; pure x } = do { m }`                 |
| Associativity | `do{x<-m; y<-k x; h y} = do{y<-do{x<-m; k x}; h y}` |

With pure values, calculations can be reordered, shared and simplified
arbitrarily, without changing the result of program. However, `IO` gets into
the way, blocking these optimizations; thus, we frequently avoid `IO`.

## Foldable and Traversable

The following classes and instances are very important.

```haskell
class Foldable t where
  foldMap :: Monoid m => (a -> m) -> t a -> m
  ...
class (Functor t, Foldable t) => Traversable t where
  traverse :: Applicative f => (a -> f b) -> t a -> f (t b)
  ...

instance Foldable [] where
  foldMap f = foldr (\ a m -> mappend (f a) m) mempty
instance Traversable [] where
  traverse f = foldr (\ x ys -> (:) <$> f x <*> ys) (pure [])

instance Foldable (Either a) where
  foldMap _ (Left _) = mempty
  foldMap f (Right a) = f a
instance Traversable (Either a) where
  traverse _ (Left x) = pure (Left x)
  traverse f (Right y) = Right <$> f y
```

# Denotational semantics

We map each Haskell concrete type to a mathematical set. (These sets are as in
“domain theory”.) For example, `Integer` (type of arbitrarily large integers)
is mapped to the set $ℤ ∪ \{⊥\}$. The “bottom” $⊥$ is a new member denoting
exceptions, crashes and non termination; its usefulness is due to the fact we
have many terminating programs in Haskell which call non terminating arguments.

The operation `(*) :: Integer -> Integer -> Integer` is sent to the function
extending the multiplication, such that $⊥ ⋅ a = ⊥ = a ⋅ ⊥$. The definition
below corresponds to almost the same function, but with $0 ⋅ ⊥ = 0$ and $⊥ ⋅ 0
= ⊥$. (Some GHC versions have this incorrect implementation or the mirrored
one.)

```haskell
mul 0 _ = 0
mul x y = x * y
```

The type `(a, b)` has interpretation $|a| × |b| ∪ \{⊥\}$; thus, $(⊥, ⊥)$ is not
the same as $⊥$.

We could consider the strict pairs: `data SPair a b = SPair !a !b`; now $(⊥,
b)$, $(a, ⊥)$ and $⊥$ are identified.

Lists are more complicated. If `a` has interpretation $|a|$, then the
interpretation of `[a]` has the typical values $[], [x], [x, y], …$ for $x, y,
… ∈ |a|$, but also contains infinite lists $[x, y, …]$ and lists involving
bottoms, such as $⊥, x : ⊥, …$.

A `newtype A = A B` is semantically equivalent to a strict ADT `data A' = A'
!B`. However, `case bot of A _ -> ()` is `()` and `case bot of A' _ -> ()` is
bottom, so the isomorphism does not preserve `case` expressions.

Functions `a -> b` can be bottom, but the $η$-expansion `\ x -> f x` is never
bottom. Bottom functions are so annoying that even GHC messes with their
semantics by default (unless you use `-fpedantic-bottoms`). For example, `id .
f` and `f . id` are equal to `f` unless `f` is bottom.

In the interpretation sets we have a partial order, called “refined by”. In $ℤ
∪ \{⊥\}$, the order is $a ≤ b$ iff $a = ⊥$ or $a = b$. In $|[a]|$, we have
relations such as $⊥ ≤ x : ⊥ ≤ x : y : ⊥ ≤ …$ and $x : ⊥ ≤ [x, y]$. All these
orders are $ω$-cpos (partial orders with suprema of increasing sequences).

Now, let us define the least fixpoint operator.

```haskell
fix :: (a -> a) -> a
fix f = let a = f a in a
```

Its semantics is defined as:
$$\operatorname{fix}(f) = \sup\{⊥, f(⊥), f(f(⊥)), …\} .$$

(The interpretation $|a → b|$ is defined to contain bottom, and also all
increasing functions preserving suprema of increasing sequences. So $⊥ ≤ f(⊥) ≤
f(f(⊥)) ≤ …$.)

For instance, `fix (\ (x :: Int) -> x + 1)` is bottom, but `fix (x :)` is the
infinite list `[x, x, …]`.

A function $f$ is called _strict_ when $f(⊥) = ⊥$. Then, if `f` is strict, `fix
f` is bottom.

## Imprecise exceptions

In the denotational semantics above, `let x = x in x`, `quot 1 0` and `error
"impossible"` are represented by the same value, $⊥$. In fact, all “pure
exceptions” (or “imprecise exceptions”) are considered equivalent by the
optimizer.

First, does `error "a" + error "b"` print “a” or “b” to stderr? The output is
arbitrary, because there is no specified order for the evaluation of `+`.

Second, does `case error "a" of !_ -> case error "b" of !_ -> error "c"` print
“a”, “b” or “c” to stderr? Any of these; changing single branch cases order is
permitted.

These examples may appear boring, but sometimes only one term in a strict
position is known to be bottom.

Sometimes this can be unexpected; see
<https://gitlab.haskell.org/ghc/ghc/-/issues/19917>.

Sequencing exceptions is possible with `IO`.

## Lazy pairs

There are two ways to case match an element of a single constructor ADT:

```haskell
-- Strict deconstruct:
case p of (a, b) -> ...
  = let !(a, b) = p in ...
  =
    let
      !_ = p
      a = case p of (x, _) -> x
      b = case p of (_, y) -> y
    in ...

-- Lazy deconstruct:
case p of ~(a, b) -> ...
  = let (a, b) = p in ...
  =
    let
      a = case p of (x, _) -> x
      b = case p of (_, y) -> y
    in ...
```

Difference is that `case bot of ~(a, b) -> (a, b)` is equal to `(bot, bot)`. It
may appear trivial, but it sometimes matters.

There are two monad instances for the state monad, the strict one and the non
strict one:

```haskell
newtype State s a = State (s -> (s, a))
```

Some operations with infinite lists are possible with the non strict instance
but not with the strict one; however, the non strict instance can cause memory
leaks,[^kseostate] when you store thunks that had better be simplified.

[^kseostate]: _Lazy vs Strict State Monad_, Kwang Yul Seo (december 2016),
  [kseo.github.io](https://kseo.github.io/posts/2016-12-28-lazy-vs-strict-state-monad.html).

(Technically, the strict instance is not a monad unless you restrict to non
bottom functions `s -> (s, a)`. Also, the non strict instance is not a monad
unless you restrict to non strict functions `s -> (s, a)`. GHC will not check
these invariants.)

# GHC extensions

GHC includes many extensions to standard Haskell in order to make programming
more comfortable. Many extensions are not rigorously specified, so that their
behaviour can be changed a bit in new releases.

Extensions can be activated with `{-# LANGUAGE ... #-}` at the header, and also
by command line.

Here we use dozens of extensions. Here is a very good documentation:
<https://ghc.gitlab.haskell.org/ghc/doc/users_guide/exts/table.html>.
The most important are `ConstraintKinds`, `DataKinds`, `GADTs`,
`PatternSynonyms`, `PolyKinds`, `RankNTypes`, `RoleAnnotations`,
`ScopedTypeVariables`, `TypeApplications`, `TypeFamilies` and `ViewPatterns`.
