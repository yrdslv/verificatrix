{-|
Module      : Veri.Dict
Description : Dictionaries (stores constraints)
Copyright   : © 2021 Yuri da Silva
License     : GPL-3.0-or-later
Maintainer  : {{{email|yrdslv|protonmail.com}}}
Stability   : experimental

Tiny version of constraints package.
-}

{-# LANGUAGE Safe #-}

module Veri.Dict
  ( Dict (..)
  , withDict
  , withDictEq
  , withDictHeq
  ) where

{-
In GHC there are two important kinds, @Type@ and @Constraint@. A constraint
(that is, a member of @Constraint@ kind) is a type whose elements are assumed
interchangeable; this is called /coherence/. For example, there is only one
element of @Eq Int@, which is the typical equality of integers. That property
is guaranteed by a series of restrictions, the first of them is that we (by
default) can not pass members of constraints directly; the compiler infers
which member to pass by searching instances. However, notationally, this
restriction is favorable, as one need not to explicit name arguments.
-}

import Data.Type.Equality (type (:~:) (..), type (:~~:) (..), type (~~))

{-|
Stores a constraint. Pattern match on @Dict@ to get it.
-}
data Dict c = c => Dict

{-|
Pattern matching in a constraint.
-}
withDict :: Dict c -> (c => r) -> r
withDict Dict e = e

{-|
Conversion between nominal equalities.
-}
withDictEq :: Dict (a ~ b) -> a :~: b
withDictEq Dict = Refl

{-|
Conversion between nominal heterogeneous equalities.
-}
withDictHeq :: Dict (a ~~ b) -> a :~~: b
withDictHeq Dict = HRefl
