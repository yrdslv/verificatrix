{-|
Module      : Veri.KnownWord
Description : Analogue of 'KnownNat' for 'Word'
Copyright   : © 2021 Yuri da Silva
License     : GPL-3.0-or-later
Maintainer  : {{{email|yrdslv|protonmail.com}}}
Stability   : experimental

See "GHC.TypeNats" module for more information.
-}

{-# LANGUAGE CPP #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MagicHash #-}

{-# LANGUAGE Trustworthy #-}

module Veri.KnownWord
  ( Nat
  , type (+)
  , type (-)
  , type (<=)
  , type (<=?)
  , KnownWord
  , Word
  , wordVal_
  , wordVal
  , seqKnown
  , withWordDict
  , WordDict (WordDict)
  , pattern WordVal
  , fromKWord#
  , toKWord#
  , kWord#
  , KWord#
  , pattern KWord#
  ) where

{-
It is possible to directly pass elements of constraints if we use some
unsafe extensions, preferentially with care! Let us define a constraint
@KnownWord m@, which is to be a singleton of the natural @m@ (fitting
in a CPU word). This way, GHC will not let us to apply a function to a
value other than the requested! Many bugs are eliminated.
-}

#include "MachDeps.h"

import Data.Word (Word)
import Data.Proxy (Proxy (Proxy))
import Unsafe.Coerce (unsafeCoerce)
import GHC.TypeNats
  ( Nat, KnownNat
  , type (+), type (-), type (^)
  , type (<=), type (<=?)
  , natVal
  )
-- Says that GHC.TypeNats is an internal GHC module, but it remained stable
-- for a long time; also it is small, not having much to change.

import qualified GHC.Exts as Exts

{-|
Like 'KnownNat', this class gives a 'Word' (natural which fits in a CPU
register) for a type-level natural, instead of a 'Natural' (which uses more
memory and processing time).
-}
class KnownWord (m :: Nat) where
  wordSing :: WordDict m

{-
The naturals fitting in a word are the smaller than 2^32 or 2^64,
depending on the computer.
-}
instance (KnownNat m, m + 1 <= 2 ^ (WORD_SIZE_IN_BITS)) => KnownWord m where
  wordSing = WordDict_ (fromIntegral (natVal (Proxy @m)))

-- Due to failure of simplification in some versions of GHC.
instance {-# INCOHERENT #-} KnownWord 0 where
  wordSing = WordDict_ 0
instance {-# INCOHERENT #-} KnownWord 1 where
  wordSing = WordDict_ 1
instance {-# INCOHERENT #-} KnownWord 2 where
  wordSing = WordDict_ 2

{-|
@wordVal_ \@m@ is the natural associated to a type term @m@.
-}
wordVal_ :: forall m. KnownWord m => Word
wordVal_ = case wordSing :: WordDict m of WordDict_ w -> w
{-# INLINE wordVal_ #-}

{-|
@wordVal _ = 'wordVal_'@. Usable without TypeApplications extension.
-}
wordVal :: forall m proxy. KnownWord m => proxy m -> Word
wordVal _ = wordVal_ @m
{-# INLINE wordVal #-}

{-|
Strict in the dictionary.
-}
seqKnown :: forall m a. KnownWord m => a -> a
seqKnown x = seq (wordVal_ @m) x
{-# INLINE seqKnown #-}

{-|
@WordDict m@ stores the value returned by @'natVal_' \@m@. Can be UNPACKed to
@Word\#@, eliminating one indirection.
-}
newtype WordDict (m :: Nat) = WordDict_ Word
type role WordDict nominal -- Prevent coercion.

{-
Forgetting to mark the type role as nominal would be catastrophe.
A user could use @coerce :: WordDict m -> WordDict n@ to mess the numbers.
-}

instance Eq (WordDict m) where
  _ == _ = True -- Singleton.
instance Ord (WordDict m) where
  compare _ _ = EQ -- Singleton.
instance Show (WordDict m) where
  show (WordDict_ w) = "(WordDict @" ++ show w ++ ")"

newtype Aux m a = Aux (KnownWord m => a)

withWordDict :: forall m a. WordDict m -> (KnownWord m => a) -> a
withWordDict (WordDict_ w) e = unsafeCoerce (Aux e :: Aux m a) w
-- Correct in tested versions of GHC because @KnownWord m => a@ is
-- representationally equal to @Word -> a@.  Needs to put under newtype because
-- there is no impredicative instantiation (by default), and constraint arrows
-- @=>@ count as impredicative.
{-# NOINLINE withWordDict #-}

{-# RULES
"withWordDict"
  withWordDict = withWordDictMagic
  #-}
-- Make Core output shorter (if rewriting is activated).

{-
@magicDict@ (or @withDict@ in recent versions) is the “correct way” to locally
inhabit constraints. However uses of @magicDict@ crash the program if rewriting
rules are disabled. That is why there is a alternative definition
@withWordDict@.
-}

withWordDictMagic :: forall m a. WordDict m -> (KnownWord m => a) -> a
withWordDictMagic (WordDict_ w) e =
#if __GLASGOW_HASKELL__ >= 904
  Exts.withDict @Word @(KnownWord m) w e
#else
  Exts.magicDict (WrapKW (\ _ -> e) :: WrapKW m a) w (Proxy @a)

data WrapKW m a = WrapKW (KnownWord m => Proxy m -> a)
#endif

data AuxPat m = KnownWord m => AuxPat (Proxy m)

wordDictToAuxPat :: WordDict m -> AuxPat m
wordDictToAuxPat d = withWordDict d (AuxPat Proxy)

{-|
Get value stored in @'WordDict'@; implicit in the dictionary.
-}
pattern WordDict :: () => (KnownWord m) => WordDict m
pattern WordDict <- (wordDictToAuxPat -> AuxPat _) where
  WordDict = wordSing

{-# COMPLETE WordDict #-}

{-|
Converts a 'Word' to a existentially quantified type-level natural variable.
-}
pattern WordVal :: () => (KnownWord m) => Proxy m -> Word
pattern WordVal p_m <- (toSomeWord -> SomeWord WordDict p_m) where
  WordVal (_ :: Proxy m) = wordVal_ @m

data SomeWord = forall m.
  SomeWord {-# UNPACK #-} !(WordDict m) {-# UNPACK #-} !(Proxy m)
-- Existentially quantified.

data SomeNatVar = forall (m :: Nat). SomeNatVar (Exts.Proxy# m)

newTypeVarWord# :: Exts.Word# -> SomeNatVar
newTypeVarWord# _ = SomeNatVar Exts.proxy#
{-# NOINLINE newTypeVarWord# #-}

toSomeWord :: Word -> SomeWord
toSomeWord w@(Exts.W# w#) = case newTypeVarWord# w# of
  SomeNatVar (_ :: Exts.Proxy# m) ->
    SomeWord (WordDict_ w :: WordDict m) Proxy

{-# COMPLETE WordVal #-}

{-
The pattern synonym @WordVal@ converts a @Word@ to some element of some
@KnownWord m@. But why that complication? Why not simply @\\ (w :: Word) ->
WordDict_ w@?

The type of that function @Word -> WordDict m@, but @m@ is not existential!  If
we do not specify it (even under a pattern synonym), GHC defaults to @Any@! If
we applied that operation to different numbers, we would get different elements
of @KnownWord Any@, contradicting coherence. Everything would be possibly
wrong, as GHC could suppose that all numbers are equal! This bug
already happened: <https://gitlab.haskell.org/ghc/ghc/-/issues/16586>.

How do we create existentially quantified type variables? The only way is using
case matching on existential types, like @SomeNatVar@. This is what we do in
@toSomeWord@. But @newTypeVarWord#@ is defined as simply having that
existential variable as @Any@; what is preventing GHC from simply expanding the
definition of @newTypeVarWord#@ and noticing that @m = Any@? Precisely the
pragma NOINLINE newTypeVarWord#, that is “do not expand”.

Not expanding the definition causes a performance impact, but very mild; we
even apply @newTypeVarWord#@ to a @Word#@ argument instead of @Word@ (which
is a pointer to @Word#@) to reduce even more the impact.
-}

{-
Frequently we want only words, not pointers to words (as in @Word@). A direct
word is called an “unboxed argument”; the only benefit of boxed words is that
they can be lazy, but we do not use such laziness here. Many times GHC does
unboxing optimizations, but not always, so sometimes we help her. But GHC still
does not support unboxed constraints, so the unboxed analogue of @KnownWord@ is
a bit annoying to use.
-}

-- | Unboxed @KnownWord@ dictionaries.
type family KWord# :: Nat -> Exts.TYPE 'Exts.WordRep where { }

-- | Like @W# :: Word# -> Word@.
fromKWord# :: KWord# m -> WordDict m
fromKWord# = unsafeCoerce Exts.W#

-- | Like selector @Word -> Word#@.
toKWord# :: WordDict m -> KWord# m
toKWord# = unsafeCoerce (\ (Exts.W# w) -> w)

-- | Implicit argument version of @fromKnownWord#@.
kWord# :: KnownWord m => KWord# m
kWord# = toKWord# WordDict

-- | Like @kWord#@.
pattern KWord# :: () => KnownWord m => KWord# m
pattern KWord# <- (fromKWord# -> WordDict) where
  KWord# = kWord#

{-# COMPLETE KWord# #-}
