{-|
Module      : Veri.ZIneqs
Description : Deciding inequalities between integer expressions.
Copyright   : © 2021 Yuri da Silva
License     : GPL-3.0-or-later
Maintainer  : {{{email|yrdslv|protonmail.com}}}
Stability   : experimental

Algorithm for deciding if a system of inequalities involving zero, variables,
addition by integer constant and maximum is satisfiable for some integral
choice of variables.

Exponential time on the number of maximums. If there is no maximum, algorithm
is of polynomial time.
-}

module Veri.ZIneqs
  ( ZVal
  , ZLe
  , impliesZ
  , isPossibleZ
  , impliesN
  , isPossibleN
  , zvalFromInteger
  , zvalZero
  , zvalAdd
  , zvalFromVar
  , zvalMax
  , zvalAlwaysLe
  ) where

{-
Not too optimized, as level polymorphism (the main usefulness) is little used.
-}

import Data.Proxy (Proxy)
import Data.Foldable (Foldable (..))
import qualified Data.IntMap.Strict as I
import qualified Data.IntSet as IS

import Veri.KnownWord
import Veri.SmallArray

-- Max-plus tropical integers.
data Trop = MinusInf | TropFin !Integer deriving (Eq, Ord, Show)

-- | Decides if inequalities imply another inequality over integer variables.
-- If you want to restrict to natural variables, use 'impliesN'
impliesZ :: [ZLe ZVal] -> ZLe ZVal -> Bool
impliesZ xys (!a, !b) = zvalAlwaysLe a b || do
  let !b' = zvalAdd 1 b
  not (isPossibleZ ((b', a) : xys))

-- | Decides if inequalities hold for some attribution of integers to
-- the variables.
isPossibleZ :: [ZLe ZVal] -> Bool
isPossibleZ xys = any isPossibleP (maximumOptions xys)

-- | Like 'impliesZ', but restricted to natural variables.
impliesN :: [ZLe ZVal] -> ZLe ZVal -> Bool
impliesN xys (!a, !b) = zvalAlwaysLe a b || do
  let !b' = zvalAdd 1 b
  not (isPossibleN ((b', a) : xys))

-- | Like 'isPossibleZ', but restricted to natural variables.
isPossibleN :: [ZLe ZVal] -> Bool
isPossibleN xys = isPossibleZ
  (map (\ i -> (zvalZero, zvalFromVar i)) (IS.toList useds) ++ xys)
  where
  useds = IS.delete 0 $ foldl' (\ r (x, y) -> IS.union r
    (IS.union (I.keysSet x) (I.keysSet y)))
    IS.empty xys

-- | Represents tropical integer expressions involving @max@, @0@, addition
-- by constants and variables.
--
-- Interpretation:
-- \[\max\left(f(0), \max_{i=1,...}{v_i + f(i)}\]
-- where \(f(i)\) is the value of key \(i\), and minus infinity if not
-- present.
type ZVal = I.IntMap Integer

-- | Get from key, returning minus infinity if not found.
getCoefs :: ZVal -> Int -> Trop
getCoefs zval i = case I.lookup i zval of
  Just x -> TropFin x
  Nothing -> MinusInf

-- | Add an integer to each coefficient.
zvalAdd :: Integer -> ZVal -> ZVal
zvalAdd c = I.map (c +) -- Strict.

-- | Pointwise compare.
zvalAlwaysLe :: ZVal -> ZVal -> Bool
zvalAlwaysLe zval1 zval2 = I.foldrWithKey op True zval1
  where
  op i c1i next = TropFin c1i <= getCoefs zval2 i && next

-- | Represents a constant.
zvalFromInteger :: Integer -> ZVal
zvalFromInteger c = I.singleton 0 c

-- | Represents zero.
zvalZero :: ZVal
zvalZero = zvalFromInteger 0

-- | Represents a variable, named by a positive integer.
zvalFromVar :: Int -> ZVal
zvalFromVar i
  | i > 0 = I.singleton i 0
  | otherwise = error "zvalFromVar: not positive."

-- | Pointwise maximum.
zvalMax :: ZVal -> ZVal -> ZVal
zvalMax = I.mergeWithKey (\ _ a b -> Just (max a b)) id id

instance Num Trop where
  (+) = max
  (-) = error "(-) @Trop"

  TropFin a * TropFin b = TropFin (a + b)
  _         * _         = MinusInf

  negate = error "negate @Trop"
  abs = error "abs @Trop"
  signum = error "signum @Trop"

  fromInteger 0 = MinusInf -- Addition neutral.
  fromInteger 1 = TropFin 0 -- Multiplication neutral.
  fromInteger _ = error "fromInteger @Trop"


type ZLe a = (a, a)

-- | Rewrites as disjunctions of conjunctions:
--
-- * @max a b <= c@ iff @a <= c@ and @b <= c@;
-- * @a <= max b c@ iff @a <= b@ or @a <= c@.
maximumOptions :: [ZLe ZVal] -> [[ZLe (Int, Integer)]]
maximumOptions [] = [[]] -- Always true.
maximumOptions ((sm, bg) : l) = do
  big <- I.toList bg
  opt <- maximumOptions l
  pure $ ((\ small -> (small, big)) <$> I.toList sm) ++ opt

getUsedVars :: [ZLe (Int, Integer)] -> IS.IntSet
getUsedVars = foldr (\ ((i, _), (j, _)) s -> op i (op j s)) IS.empty
  where
  op i s = case compare i 0 of
    GT -> IS.insert i s
    EQ -> s
    LT -> error "Veri.ZIneqs.getUsedVar: variable has negative index."

positionIn :: Int -> IS.IntSet -> Int
positionIn el s = case IS.split el s of
  (lessers, _) -> IS.size lessers

squareCheckOflo :: Int -> Int
squareCheckOflo i = fromEnum ((toEnum i :: Integer) ^ (2 :: Int))

isPossibleP :: [ZLe (Int, Integer)] -> Bool
isPossibleP xysinit = do
  let
    usedVars = getUsedVars xysinit
    !n = IS.size usedVars

    -- Drop unused variables.
    xys = map (\ ((i, ci), (j, cj)) -> (op i ci, op j cj)) xysinit
    op 0 ci = (0, ci)
    op i ci = (1 + positionIn i usedVars, ci)

    !cols = n + 1

    -- Let us denote the variables by @v(1), v(2), ...@ and zero by @v(0)@.
    -- The matrix @weights@ has elements @a(i,j)@ where each inequality
    -- @a(i,j) <= -v(i) + v(j)@ is evident consequence of @xys@.
    weights = runSmallArray $ do
      msm <- newSmallArray (squareCheckOflo cols) MinusInf
      flip mapM_ [0 .. n] $ \ i ->
        writeSmallArray msm (i * cols + i) (TropFin 0)
      flip mapM_ xys $ \ ((i, ci), (j, cj)) -> do
        let idx = i * cols + j
        a <- readSmallArray msm idx
        writeSmallArray msm idx $! a + TropFin (ci - cj)
      pure msm
  -- Algorithm could be faster, but this is pretty simple.
  case toEnum cols of
    WordVal (_ :: Proxy c) ->
      -- If a tropical matrix stores weights of a digraph,
      -- its powers store maximum weights of paths of certain sizes.
      -- Given these considerations, prove that the inequalities are
      -- possible iff there is no positive cycle of length @c@.
      case (SquareMatrix weights :: SquareMatrix c Trop) ^ cols of
        SquareMatrix !wp ->
          all (\ i -> indexSmallArray wp (i * cols + i) <= TropFin 0)
            [0 .. n]

newtype SquareMatrix c a = SquareMatrix (SmallArray a)
type role SquareMatrix nominal representational

instance (KnownWord c, Num a) => Num (SquareMatrix c a) where
  (+) = error "dead code"
  (-) = error "dead code"

  fromInteger x = SquareMatrix $ runSmallArray $ do
    let !c = fromEnum (wordVal_ @c)
    let !x' = fromInteger x
    msm <- newSmallArray (squareCheckOflo c) 0
    flip mapM_ [0 .. c - 1] $ \ i ->
      writeSmallArray msm (i * c + i) x'
    pure msm

  (SquareMatrix !m1) * (SquareMatrix !m2) = SquareMatrix $ runSmallArray $ do
    let c = fromEnum (wordVal_ @c) :: Int
    msm <- newSmallArray (c ^ (2 :: Int)) (error "matrix mult, impossible")
    flip mapM_ [0 .. c - 1] $ \ i ->
      flip mapM_ [0 .. c - 1] $ \ j -> do
        writeSmallArray msm (i * c + j) $!
          sum $ flip map [0 .. c - 1] $ \ k ->
            indexSmallArray m1 (i * c + k) * indexSmallArray m2 (k * c + j)
    pure msm

  negate = error "dead code"
  abs = negate
  signum = negate
