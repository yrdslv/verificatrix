{-|
Module      : Veri.TC
Description : Type checking, universe level substitution and basic parsing
Copyright   : © 2021 Yuri da Silva
License     : GPL-3.0-or-later
Maintainer  : {{{email|yrdslv|protonmail.com}}}
Stability   : experimental

This file contains the main type checking algorithms and primitive syntax
parsing (without sugar). Prerequisites for understanding the code:

* de Bruijn indices;
* basics of type theory, specifically Martin–Löf type theory (MLTT) with
  inductives and infinitely many universes (if you do not know about
  inductive types, you will not understand this file!);
* Tarski universes, as in /A Calculus of Constructions with Explicit
  Subtyping/, by Ali Assaf, available in
  <https://hal.archives-ouvertes.fr/hal-01097401v2/document>.
* explicit substitutions (suspension calculus), as in the article
  /Implementation of Explicit Substitutions from λσ to the Suspension
  Calculus/, by Vincent Archambault-Bouffard and Stefan Monnier, available in
  <https://www.iro.umontreal.ca/~monnier/HOR-2016.pdf>.

__Warning__: This code contains many defects; in particular, I would be very
thankful if someone explained how to improve the performance.
-}

{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE MagicHash #-}
{-# LANGUAGE PartialTypeSignatures #-}

module Veri.TC
   -- EXPORTS
   where

{-
READING TIP:
Ignore all occurrences of @withDict@.
-}

import Control.Applicative (Alternative (..))
import Control.Monad (replicateM)
import Control.Monad.State.Strict (MonadState (..), State, runState)
import Control.Monad.Trans (MonadTrans (..))
import Data.Bits (Bits (..))
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as B8
import Data.Foldable (Foldable (..))
import qualified Data.IntMap.Strict as I
import Data.Kind (Type)
import qualified Data.Map.Strict as M
import Data.Type.Bool (If)
import Data.Type.Equality (type (:~:) (..))
import Data.Void (Void)
import Data.Word (Word, Word32, Word64, Word8)
import Debug.Trace (trace)

import Numeric.Natural (Natural)

import GHC.Exts (isTrue#, reallyUnsafePtrEquality#, Int#)
import System.IO.Unsafe (unsafePerformIO)
import System.Mem.StableName (StableName)
import Unsafe.Coerce (unsafeCoerce)


-- Parsing:
import qualified Crypto.Hash as CH
import qualified Data.ByteArray as BA
import Data.ByteString.Base64.URL as B64
import qualified Text.Megaparsec as P

import Veri.Dict
import Veri.KnownWord
import qualified Veri.Dict.KnownWord as K
import Veri.SmallArray
import Veri.ZIneqs
import Veri.UnsafeGlobalDefToHashLevels

-- | @bug fn what@ complains about a coding mistake detected in function @fn@,
-- with description @what@, so that users can report more easily.
bug :: String -> String -> a
bug fn what = error $ "Bug in " ++ fn ++ " (" ++ what ++ ")!!!"

-- * Syntax structures

-- | Universe level expression as 'ZVal'. It is supposed that all non minus
-- infinity coefficients are non-negative, and that at least one coefficient
-- is not minus infinity.
type Level = ZVal
type V = SmallArray

pattern R :: () => (a ~ b) => a :~: b
pattern R = Refl
{-# COMPLETE R #-}

type T = Term

{-|
Manipulatable syntax: no explicit reference to global and local definitions, as
they are implicitly shared, and evaluations are stored as thunks.

This syntax is chosen to permit efficient reduction and also conversion to
textual syntax (that is why there are type arguments for Lam and Cn).

The type-level argument @m@ is the size of the scope (how many @Refer@ are
permitted).

There were universe level casts, but now these are implicit; there is no
subtyping.
-}
data Term (m :: Nat) =
  -- | @Refer Refl (kWord# @i)@ is a variable of de Bruijn index @i@.
  forall i. Refer
    {-# UNPACK #-} !((i + 1 <=? m) :~: 'True)
    (KWord# i)
  -- | Universe of level.
  | Univ !Level
  -- | @Pi a b_a _@ is the dependent product of the “family of types” @b_a@,
  -- indexed by type @a@. The term @b_a :: s (m + 1)@ depends on a new
  -- variable, which is the indexing variable.
  | Pi !(T m) !(T (m + 1)) {-# UNPACK #-} !RBN
  -- | @Lam a e_a _@ is the function of domain @a@, defined according to @e_a@.
  -- The term @e_a@ depends on a new variable, the function argument.
  | Lam !(T m) !(T (m + 1)) {-# UNPACK #-} !RBN
  -- | @App f c _@ is the application of @f@ (whose type shall reduce to Pi)
  -- to argument @c@.
  | App !(T m) !(T m) (Whnf (T m)) {-# UNPACK #-} !RBN
  -- | Inductive family.
  | Ind {-# UNPACK #-} !(IndData m)
  -- | @Cn i ti as@ is the @i@-th constructor of inductive family @ti@ (shall
  -- reduce to an Ind) applied to arguments @as@.
  | Cn {-# UNPACK #-} !Int !(T m) {-# UNPACK #-} !(V (T m)) {-# UNPACK #-} !RBN
  -- | @RecW rd a _@ is a recursor applied to @a@, with miscellanea @rd@.
  -- See 'Rec'.
  | forall n d cond. RecW {-# UNPACK #-} !(RecDataW n m d cond)
    !(T m) (Whnf (T m)) {-# UNPACK #-} !RBN
  -- | @PrimUnNat x@ is the unary natural @S (… (S 0) …)@, where there are
  -- @x@ successors @S@.
  | PrimUnNat !Natural
  -- | @PrimBbNat x@ is the bijective binary natural corresponding to @x@.
  -- These naturals are written: (empty), 1, 2, 11, 12, 21, 22, 111, 112, …
  | PrimBbNat !Natural
  -- | Substitution suspension. See 'Subs'. Invariants:
  --
  -- * Can't have Subs Refer (apply the substitution to the index).
  -- * Can't have Subs Subs (compose the substitutions);
  -- * Can't have Subs Univ, Subs PrimUnNat or Subs PrimBbNat (discard
  --   the substitution, as these terms are constant).
  | forall n d cond. SubsW
    !(T (d + n)) {-# UNPACK #-} !(ImmEsubsData n m d cond)
    (Whnf (T m))
  -- | Used as filler for places which will not be used.
  | DontCare
-- No Eq, Ord, Show instances as these don't take in consideration
-- the sharing.
{-# COMPLETE Refer, Univ, Pi, Lam, App, Ind, Cn, Rec,
  PrimUnNat, PrimBbNat, Subs, DontCare #-}


-- | @Rec@ is the more comfortable version of @RecW@, which was defined
-- that way only because GHC does not support unpacking existential types.
pattern Rec ::
  RecData n m -> T m -> Whnf (T m) -> RBN -> T m
pattern Rec rd a whnf rbn <- RecW (fromRecDataW -> rd) a whnf rbn
  where
  Rec rd a whnf rbn = withToRecDataW rd $ \ rdw -> RecW rdw a whnf rbn

-- | @Subs@ is the more comfortable version of @SubsW@.
pattern Subs ::
  T n -> ImmEsubs n m -> Whnf (T m) -> T m
pattern Subs s imm whnf <- SubsW s (ImmEsubs -> imm) whnf
  where
  Subs s (ImmEsubs immd) whnf = SubsW s immd whnf

-- | Stores weak head-normal forms (WHNF) of terms. Ignoring substitution
-- suspensions, a term is in WHNF if its head can not change after reductions;
-- that is a term is in WHNF if is @App h c _@ where @h@ does not reduce
-- to @Lam{}@, or is @Rec rd c _@ where @c@ does not reduce to constructor,
-- or is any other thing.
newtype Whnf a = Whnf a

-- | Get weak head-normal form: of App, Rec and Subs there is a Whnf field,
-- which is returned; other terms have the weak head-normal form equal to
-- itselves.
getWhnf :: T a -> T a
getWhnf (App _ _ (Whnf v) _) = v
getWhnf (Rec _ _ (Whnf v) _) = v
getWhnf (Subs _ _ (Whnf v)) = v
getWhnf v = v

-- | Stores terms derived from others and which may or may not be used.
newtype Built a = Built{unBuilt :: a}

-- | Pair of @Register@ and @BndrNm@; see 'RBN' pattern.
newtype RBN = RBN_ Word64

-- | No specific information.
defaultRBN :: RBN
defaultRBN = RBN_ 0

-- | Get register and binder name number corresponding to the top syntax
-- construct.  For example, given Lam, the binder name number corresponding to
-- the introduced variable is returned; given Univ, the RBN return is zero.
getRBN :: T a -> RBN
getRBN a = (case a of
  Subs b _ _ -> go b
  b -> go b)
  where
  go (Pi _ _ rbn) = rbn
  go (Lam _ _ rbn) = rbn
  go (App _ _ _ rbn) = rbn
  go (Cn _ _ _ rbn) = rbn
  go (Rec _ _ _ rbn) = rbn
  go _ = defaultRBN

-- Quotient and remainder.
int64ToPair :: Word64 -> (Word32, Word32)
int64ToPair i = (fromIntegral (shiftR i 32), fromIntegral i)

int64FromPair :: (Word32, Word32) -> Word64
int64FromPair (a, b) = shiftL (fromIntegral a) 32 .|. fromIntegral b

-- | “Registers”, informations about a term possibly relevant to reduction.
-- As we have primitive naturals (so that we can write 8 instead of SSSSSSSS0,
-- or waste 20 times the memory storing binary bijective naturals in terms
-- of Cn), we shall have basic primitive operations. Registers are trusted
-- to be correct.
--
-- Registers are preserved under substitutions and reductions. But the type of
-- a term has to have the register erased; that applies for example to a type
-- Pi of a Lam.
newtype Register = Register Word32 deriving (Eq, Ord, Show)

-- | “Binder names”, represented by integers. Can be used to make legible
-- choice of names of variables; for example, @BndrNm 1@ may make a GUI use the
-- names @y, y1, y2, ...@ for a variable. The binder name is not used for
-- anything more.
newtype BndrNm = BndrNm Word32 deriving (Eq, Ord, Show)

-- | No specific name.
defaultBndrNm :: BndrNm
defaultBndrNm = BndrNm 0

pattern RBN :: Register -> BndrNm -> RBN
pattern RBN r n <- RBN_ (int64ToPair -> (Register -> r, BndrNm -> n))
  where
  RBN (Register r) (BndrNm n) = RBN_ (int64FromPair (r, n))
{-# COMPLETE RBN #-}

eraseRegister :: RBN -> RBN
eraseRegister (RBN _ nm) = RBN NoRegister nm

-- | No additional information.
pattern NoRegister :: Register
pattern NoRegister =  Register 0
-- | Element of unary naturals, so that we can simplify
-- @mkCn 1 _ [mkPrimUnNat x]@ to @mkPrimUnNat (x + 1)@.
pattern UnNatElem  :: Register
pattern UnNatElem  =  Register 10
-- | Some definition of unary natural addition, so that we can simplify
-- @mkApp (mkApp plus (mkPrimUnNat x)) (mkPrimUnNat y)@ to
-- @mkPrimUnNat (x + y)@ instead of doing the calculation in unary.
--
-- We can not simplify @mkApp (mkApp plus (mkPrimUnNat 0)) m@ to @m@ as it
-- is not specified if the addition is done by induction on the first or
-- the second variable, or if it is define differently. Only the behaviour
-- on closed naturals (without variables) is specified.
pattern OpUnAdd    :: Register
pattern OpUnAdd    =  Register 11
-- | Some definition of unary natural multiplication. Comments of 'OpUnAdd'
-- apply here and in the following definitions.
pattern OpUnMul    :: Register
pattern OpUnMul    =  Register 12
-- | Some definition of unary natural comparison, returning into the three element inductive type (LT, EQ, GT).
pattern OpUnCmp    :: Register
pattern OpUnCmp    =  Register 13
-- | Some definition of unary natural truncated subtraction, that is,
-- subtraction with negative results changed to zero.
pattern OpUnTrSub  :: Register
pattern OpUnTrSub  =  Register 14
-- | Some definition of unary natural quotient and remainder. If applied to
-- natural literals @a, b@ where @b /= 0@, returns @mkCn 0 _ [q, r]@. If
-- applied to @a, 0@ returns @mkCn 0 _ [0, a]@; this is reasonable, as the
-- rest is to be congruent to the dividend modulo the divisor.
pattern OpUnQtRem  :: Register
pattern OpUnQtRem  =  Register 15
-- | Same as 'UnNatElem', but for binary bijective naturals.
pattern BbNatElem  :: Register
pattern BbNatElem  =  Register 20
pattern OpBbAdd    :: Register
pattern OpBbAdd    =  Register 21
pattern OpBbMul    :: Register
pattern OpBbMul    =  Register 22
pattern OpBbCmp    :: Register
pattern OpBbCmp    =  Register 23
pattern OpBbTrSub  :: Register
pattern OpBbTrSub  =  Register 24
pattern OpBbQtRem  :: Register
pattern OpBbQtRem  =  Register 25
-- | Element of an application of some “equality-like inductive”, that is,
-- a inductive family with only one constructor, having no arguments. It is
-- useful because closed elements of that families reduce to the single
-- constructor (the “reflexivity”), so that, for example, proofs of
-- commutativity of specific naturals can be quickly simplified to
-- reflexivity instead of causing dozens of unnecessary calculations.
--
-- (I would like someone to confirm that MLTT really satisfies that property,
-- which is not satisfied by many extensions, such as HOTTs.)
pattern EqIndElem  :: Register
pattern EqIndElem  =  Register 30
-- | Unary natural from binary bijective natural.
pattern OpUnFromBb :: Register
pattern OpUnFromBb =  Register 40
-- | Bijective natural from unary natural
pattern OpBbFromUn :: Register
pattern OpBbFromUn =  Register 50

-- | Data in an inductive family definition:
--
-- * a “sort”, which is a sequence of Pis ending in an universe, which can not
-- be smaller than the arguments in the Pis;
-- * “constructor signatures”, specifying how to build elements and how to do
-- recursion.
data IndData m =
  IndData {-# UNPACK #-} !(IndSort m) {-# UNPACK #-} !(CnSigs m)

-- | A “telescope” is a sequence of things whose indices keep increasing
-- by one until one reaches the end. When the indices represent sizes of
-- scopes, one may say that each element on a telescope may depend on
-- the previous ones.
--
-- The RBN arguments here have null Registers.
data GTele a e m =
  TeleEnd !(e m)
  | TeleCons !(a m) !(GTele a e (m + 1)) {-# UNPACK #-} !RBN
type Tele = GTele Term

{-|
@IndSort l t@ contains a level @l@ and a “telescope” @t@, containing the components of the Pis. See 'CnSig' for examples.
-}
data IndSort m = IndSort !Level !(IndSortTele m)
type IndSortTele = Tele TeleEndEmpty
data TeleEndEmpty m = TeleEndEmpty

type CnSigs m = V (CnSig m)

{-|
In Coq and Agda, inductive types are defined by means of the “strict positivity
condition”. Here we make that condition almost trivial to check using
“constructor signatures”.

Some examples of sorts and constructor signatures (Coq surface syntax and
internal representation here):

@
Coq:
  Inductive Empty := .
Here:
  IndData
    (IndSort l0 (TeleEnd TeleEndEmpty))
    []

Coq:
  Inductive unit := tt.
Here:
  IndData
    (IndSort l0 (TeleEnd TeleEndEmpty))
    [CnSigAnns []]

Coq:
  Inductive bool := false | true.
  (* here we use the inverse order of constructors than in Coq StdLib,
  so that false < true *)
Here:
  IndData
    (IndSort l0 (TeleEnd TeleEndEmpty))
    [CnSigAnns []
    ,CnSigAnns []]

Coq:
  Inductive nat := O | S (_ : nat).
Here:
  IndData
    (IndSort l0 (TeleEnd TeleEndEmpty))
    [CnSigAnns []
    ,CnSigR _ (TeleEnd (CnInnEnd [])) (CnSigAnns []) _]

Coq: (* without level polymorphism *)
  Inductive prod A B := pair (_ : A) (_ : B).
Here:
  Lam (Univ l0)
    (Lam (Univ l0)
      (IndData
        (IndSort l0 (TeleEnd TeleEndEmpty))
        [CnSigNR refer1 (CnSigNR refer1 (CnSigAnns []) _) _])
      _)
    _
@

Coq:
  Inductive identity A (a:A) : A -> Type :=
    identity_refl : identity a a.
Here:
  Lam (Univ l0)
    (Lam (Univ refer0)
      (IndData
        (IndSort l0 (TeleCons refer1 (TeleEnd TeleEndEmpty) _))
        [CnSigAnns [refer0]])
      _)
    _

Coq:
  Inductive JMidentity A (a:A) : forall B, B -> Type :=
    JMidentity_refl : JMidentity A a A a
Here:
  Lam (Univ l0)
    (Lam (Univ refer0)
      (IndData
        (IndSort l0 (TeleCons (Univ l0)
          (TeleCons refer0 (TeleEnd TeleEndEmpty) _) _))
          [CnSigAnns [refer1, refer0]])
      _)
    _

Coq:
  Inductive sigT A (P : A -> Type) : Type :=
    existT : forall a, P a -> sigT A P.
Here:
  Lam (Univ l0)
    (Lam (Pi refer0 (Univ l0) _)
      (IndData
        (IndSort l0 (TeleEnd TeleEndEmpty))
        [CnSigNR refer1 (CnSigNR (App refer1 refer0 _ _)
          (CnSigAnns []) _) _])
      _)
    _

Coq:
  Inductive brouwer := bo | bs (_ : brouwer) | blim (_ : nat -> brouwer).
Here:
  IndData
    (IndSort (TeleEnd TeleEndEmpty))
    [CnSigAnns []
    ,CnSigR _ (TeleEnd (CnInnEnd [])) (CnSigAnns []) _
    ,CnSigR _ (TeleCons nat (TeleEnd (CnInnEnd [])) _) (CnSigAnns []) _]
@

It is preferred to have the sort small so that induction is easier; parameters
outside of the sort go into @Lam …@.
-}
data GCnSig a m =
  -- | The inductive family to be defined applied to some annotations.
  -- As we have index @m@ in @V (T m)@, the inductive is prohibited from
  -- appearing in that annotations.
  CnSigAnns {-# UNPACK #-} !(V (a m))
  -- | Non recursive argument, that is, a @Pi a …@, where the inductive
  -- family can not appear at @a@.
  | CnSigNR !(a m) !(GCnSig a (m + 1)) {-# UNPACK #-} !RBN
  -- | Recursive argument, that is a @Pi a …@, where the inductive family
  -- appears at @a@ in a specific way.
  --
  -- The built argument stores the @CnInn@ in terms of Pis and cyclically
  -- refers to the same Ind.
  | CnSigR (Built (a m)) !(GCnInn a m) !(GCnSig a m) {-# UNPACK #-} !RBN
type CnSig = GCnSig Term

-- | An “inner constructor signature” is a sequence of Pis ending in
-- an application of the inductive family. These Pis are indexed over
-- types which do not mention the inductive family to be defined.
type GCnInn a = GTele a (GCnInnEnd a)
type CnInn = GCnInn Term
type CnInnEnd = GCnInnEnd Term
newtype GCnInnEnd a m = CnInnEnd (V (a m))

-- | “Recursor data”. @RecData imm ti p rcs@ contains an explicit substitution
-- @imm, a term @ti@ reducing to Ind, a “recursion predicate” @p@ and
-- “recursion cases” @rcs@, one for each constructor signature of @ti@.
data RecData n m =
  RecData !(ImmEsubs n m) !(T n) !(RecPred n)
    {-# UNPACK #-} !(RecCases n)

-- | Mostly the same as @RecData@, only here cause GHC does not support
-- existential UNPACKing.
data RecDataW n m d cond =
  RecDataW
    {-# UNPACK #-} !(ImmEsubsData n m d cond)
    (T (d + n))
    !(RecPred (d + n))
    {-# UNPACK #-} !(RecCases (d + n))

fromRecDataW :: RecDataW n m d cond -> RecData (d + n) m
fromRecDataW (RecDataW immd ti p rcs) = RecData (ImmEsubs immd) ti p rcs

withToRecDataW :: RecData dn m ->
  (forall d n cond. ((d + n) ~ dn) => RecDataW n m d cond -> r) -> r
withToRecDataW (RecData (ImmEsubs immd) ti p rcs) cont =
  cont (RecDataW immd ti p rcs)

{-|
“Recursion predicate” or “recursion family”. It is a sequence of Lams,
whose arguments are the same as in the sort; after them, there is one
more Lam, over an appropriate application of the inductive family, then
ends with a type in a universe @Univ r@, where @l <= r@, @l@ being the
sort level. This level @r@ is stored in the Built argument.

See 'RecCase' for examples.
-}
newtype GRecPred a n = RecPred (GRecPredTele a n)
type RecPred = GRecPred Term

-- | Fill the term arguments with anything, they will be unused.
type RecPredTele = Tele RecPredTeleEnd
type GRecPredTele a = GTele a (GRecPredTeleEnd a)

type RecPredTeleEnd = GRecPredTeleEnd Term
data GRecPredTeleEnd a n = RecPredTeleEnd !(a (n + 1)) {-# UNPACK #-} !RBN

type RecCases n = V (RecCase n)

{-|
The recursion lets us define elements of predicate applications, and the recursion cases imitate the constructor signatures.

Examples (see also 'CnSig'), not rigorous notation:

@
Empty:
  requires no case

unit:
  requires case
    RecCaseEnd (el : P tt)
  such that there is reduction
    Rec(tt) ~> el

bool:
  requires cases
    RecCaseEnd (elF : P false)
    RecCaseEnd (elT : P true)
  such that
    Rec(false) ~> elF
    Rec(true) ~> elT

nat:
  requires cases
    RecCaseEnd (elZ : P O)
    RecCaseR (fun n h => elS(n, h) : P (S n))
  such that
    Rec(O) ~> elZ
    Rec(S n) ~> elS(n, Rec(n))

prod:
  requires case
    RecCaseNR (fun a => RecCaseNR (fun b =>
      RecCaseEnd (elP : P (pair a b))))
  such that
    Rec(pair a b) ~> elP(a, b)

identity:
  requires case
    RecCaseEnd (elR : P a refl)
  such that
    Rec(refl) ~> elR

JMidentity:
  requires case
    RecCaseEnd (elR : P A a JMrefl)
  such that
    Rec(JMrefl) ~> elR

sigT:
  requires case
    RecCaseNR (fun a => RecCaseNR (fun e =>
      RecCaseEnd (elE : P (existT a e))))
  such that
    Rec(existT a e) ~> elE(a, e)

brouwer:
  requires cases
    RecCaseEnd (elZ : P bo)
    RecCaseR (fun a h => elS : P (bs a))
    RecCaseR (fun s h => elL : P (blim s))
  such that
    Rec(bo) ~> elZ
    Rec(bs a) ~> elS(a, Rec(a))
    Rec(bo s) ~> elL(s, fun n => Rec(s n))
@
-}
data GRecCase a n =
  -- | End of recursor case, a element whose type is the application
  -- of the recursion predicate to the constructor corresponding to this case,
  -- applied to the “generic variables”.
  RecCaseEnd !(a n)
  -- | On non recursive arg: a new “generic variable” is introduced, whose type
  -- is the same as in 'CnSigNR'.
  | RecCaseNR !(GRecCase a (n + 1)) {-# UNPACK #-} !RBN
  -- | One recursive arg: a new ”generic variable” is introduced, and also the
  -- inductive hypothesis.
  --
  -- In Built we store the types of the arguments of the recursive hypothesis,
  -- the same as in 'CnInn'.
  | RecCaseR !(GRecCase a (n + 1 + 1))
    {-# UNPACK #-} !RBN {-# UNPACK #-} !RBN
    (Built (GTele a TeleEndEmpty n))
type RecCase = GRecCase Term

-- * Explicit substitutions

{-
(Read the article by Vincent and Stefan!)

A explicit substitution has two parts, a head (Esubs) and the tail.
-}

-- | Explicit substitutions head. An element of type @Esubs s a b@ is
-- to be understood as a function @{0 .. a - 1} -> s b@.
data Esubs (a :: Nat) (b :: Nat) where
 -- | The empty function.
  EsNil :: Esubs 0 b
  -- | @EsCons Refl (b - b') s es@ is the substitution sending @0@ to
  -- @L(s)@ and @i + 1@ to @L(es(i))@, where @L@ is 'termAdd#' applied
  -- to @b - b'@.
  EsCons ::
    {-# UNPACK #-} !((b' <=? b) :~: 'True) ->
    KWord# (b - b') ->
    !(T b') ->
    (Esubs a b') ->
    Esubs (a + 1) b

-- | Is it zero?
data IsZero (b :: Bool) (d :: Nat) where
  IsUnk :: IsZero 'False d
  IsZero :: IsZero 'True 0

{-
What the article by Vincent and Stefan calls @ol@ is here the domain size of
@Esubs@, and what is called @nl@ is the tail increment @b - d@, as in
'ImmEsubsData'.

Differently from the article by Vincent and Stefan, here the domain and the
codomain of a substitution are well marked, but only on type level, because
they do not need to be stored (in fact, there is no @KWord# d@ below). Later we
will note the usefulness of detecting if a explicit substitution has empty
tail, but how we do it, if we don't have the value of @d@?

Because of that, there is the option of giving the increment @b - d@ or
a proof that @d@ is zero (in which case the increment is not needed).
-}

-- | Data of “explicit substitution joined with immersion”.
-- @ImmEsubsData Refl _ nl ol es@ represents the function sending index @i@
-- to @es(i)@ if @i < ol@, and sending to @i - ol + nl@ otherwise.
data ImmEsubsData a b d cond =
  ImmEsubsData
    {-# UNPACK #-} !(If cond 'True (d <=? b) :~: 'True)
    !(IsZero cond d)
    (KWord# (If cond 0 (b - d)))
    (KWord# a)
    (Esubs a b)

-- | The true “explicit substitution with immersion”, here only because
-- GHC does not UNPACK existentials.
data ImmEsubs a b where
  ImmEsubs ::
    {-# UNPACK #-} !(ImmEsubsData a b d cond) ->
    ImmEsubs (d + a) b

-- GHC didn't support Work/Wrapper on existentials.
newtype FromImmEsubs da b r =
  FromImmEsubsW (forall a d cond. ((d + a) ~ da) =>
    ImmEsubsData a b d cond -> r)

applyImmEsubs :: FromImmEsubs da b r -> ImmEsubs da b -> r
applyImmEsubs (FromImmEsubsW g) (ImmEsubs immd) = g immd

pattern FromImmEsubs ::
  (ImmEsubs da b -> r) -> FromImmEsubs da b r
pattern FromImmEsubs f <- (applyImmEsubs -> f) where
  FromImmEsubs f = FromImmEsubsW (\ immd -> f (ImmEsubs immd))
{-# COMPLETE FromImmEsubs #-}

newtype FromRecData da m r =
  FromRecDataW (forall a d cond. ((d + a) ~ da) =>
    RecDataW a m d cond -> r)

applyRecData :: FromRecData da b r -> RecData da b -> r
applyRecData (FromRecDataW g) rd = withToRecDataW rd g

pattern FromRecData ::
  (RecData da b -> r) -> FromRecData da b r
pattern FromRecData f <- (applyRecData -> f) where
  FromRecData f = FromRecDataW (\ rdw -> f (fromRecDataW rdw))
{-# COMPLETE FromRecData #-}

{-
XXX. 'FromImmEsubs' and 'FromRecData' are signs of misguided optimizations.
Still, GHC generates bloated and unsimplified code, in part to the high
number of mutually recursive operations, and also to the high number of
arguments to many functions.
-}

data Subsend e m = forall n d cond. SubsendW !(e (d + n))
  {-# UNPACK #-} !(ImmEsubsData n m d cond)

pattern Subsend :: e dn -> ImmEsubs dn m -> Subsend e m
pattern Subsend s imm <- SubsendW s (ImmEsubs -> imm) where
  Subsend s (ImmEsubs immd) = SubsendW s immd
{-# COMPLETE Subsend #-}

subsendToTerm :: Subsend Term m -> Term m
subsendToTerm (Subsend a imm) = mkSubs a imm

-- | @termAdd# m@ tells to add @m@ to each variable index not introduced by
-- inner binding; also called “lifting”. This corresponds to a term with does
-- not depend on the @m@ newest variables.
termAdd# :: forall m a. KWord# m -> T a -> T (a + m)
termAdd# KWord#
  | Just Dict <- K.eqKnown @m @0 = id
  | otherwise = go
  where
  -- The explicit substitution adding @m@.
  immM :: forall b. ImmEsubs b (b + m)
  immM = immAdd# @m KWord#
  go (Refer R (KWord# :: KWord# i)) =
    -- Prove that @i + 1 <= a@ implies @i + m + 1 <= a + m@.
    withDict (K.plusKnown @i @m) $
    withDict (K.rightPlusComm @i @m @1) $
    withDict (K.leRightPlus @(i + 1) @a @m) $
    Refer R (KWord# :: KWord# (i + m))
  -- Universes and literals, discard the substitution.
  go (Univ l) = mkUniv l
  go (PrimUnNat x) = mkPrimUnNat x
  go (PrimBbNat x) = mkPrimBbNat x
  -- Subs, compose the substitution (using mkImmAdd#).
  go (Subs s imm (Whnf r)) =
    let
      immAM = mkImmAdd# @m KWord# imm
      res = case whnfA of
        ((), whnf) ->
          Subs s immAM (Whnf whnf)
      whnfA = case s of
        Pi{} -> ((), res)
        Lam{} -> ((), res)
        Ind{} -> ((), res)
        Cn{} -> ((), res)
        _ -> case isImmAddOrZero imm of
          True -> ((), mkSubs (getWhnf s) immAM)
          False -> ((), go r)
    in res
  go t =
    let
      res = Subs t immM (Whnf whnf)
      whnf = case t of
        Pi{} -> res
        Lam{} -> res
        Ind{} -> res
        Cn{} -> res
        _ -> go (getWhnf t)
    in res

-- | @termAdd1@ tells to add 1 to each free variable index; that gives a term
-- which does not depend on the newest variable. We use this operation to put
-- terms inside lambdas.
termAdd1 :: T a -> T (a + 1)
termAdd1 = termAdd# @1 KWord#

-- | Substitution which adds @m@ to each variable.
immAdd# :: forall m a. KWord# m -> ImmEsubs a (a + m)
immAdd# KWord# =
  withDict (K.rightPlusExpans @a @m) $
  withDict (K.leftPlusRightMinus @a @m) $
  ImmEsubs $
  ImmEsubsData R IsUnk (kWord# @m) (kWord# @0) EsNil

-- | Identity substitution.
immId :: forall a. ImmEsubs a a
immId = immAdd# @0 KWord#

-- | Empty substitution.
immZero :: forall a. ImmEsubs 0 a
immZero = withDict (K.rightPlusExpans @0 @a) $
  ImmEsubs (ImmEsubsData R IsZero KWord# (kWord# @0) EsNil)

-- | Compose substitution with addition by @m@.
mkImmAdd# :: forall m a b. KWord# m ->
  ImmEsubs a b -> ImmEsubs a (b + m)
mkImmAdd# KWord# (ImmEsubs (immd :: ImmEsubsData _ _ d _))
  | ImmEsubsData R IsUnk KWord# KWord# es <- immd =
    withDict (K.leWeakRightPlus @d @b @m) $
    withDict (K.rightMinusPlusComm @b @d @m) $
    withDict (K.plusKnown @(b - d) @m) $
    ImmEsubs (ImmEsubsData R IsUnk KWord# KWord# (mkEsAdd# @m KWord# es))
  | ImmEsubsData R IsZero _ KWord# es <- immd =
    withDict (K.leWeakRightPlus @d @b @m) $
    ImmEsubs (ImmEsubsData R IsZero KWord# KWord# (mkEsAdd# @m KWord# es))

-- | Like 'mkImmAdd#', but for Esubs.
mkEsAdd# :: forall m a b. KWord# m ->
  Esubs a b -> Esubs a (b + m)
mkEsAdd# KWord# es
  | Just Dict <- K.eqKnown @m @0 = es
mkEsAdd# KWord# EsNil = EsNil
mkEsAdd# KWord# (EsCons R KWord# s (es' :: Esubs a' b')) =
  withDict (K.leWeakRightPlus @b' @b @m) $
  withDict (K.rightMinusPlusComm @b @b' @m) $
  withDict (K.plusKnown @(b - b') @m) $
  EsCons R KWord# s es'

-- | Is substitution addition by some number? Answers @Nothing@ if unknown.
isImmAdd :: ImmEsubs a b -> Maybe (Dict (a <= b, KnownWord (b - a)))
isImmAdd (ImmEsubs immd)
  | ImmEsubsData R IsUnk KWord# _ es <- immd, EsNil <- es = Just Dict
  | otherwise = Nothing

-- | Is substitution identity? Answers @Nothing@ if unknown.
isImmId :: ImmEsubs a b -> Maybe (Dict (a ~ b))
isImmId (ImmEsubs (immd :: ImmEsubsData _ _ d _))
  | ImmEsubsData R IsUnk KWord# _ es <- immd, (EsNil :: Esubs _ b) <- es
  , Just Dict <- K.eqKnown @(b - d) @0 =
    withDict (K.rightMinusLeftPlus @d @b) $
    Just Dict
  | otherwise = Nothing

-- | Is substitution empty? Answers @Nothing@ if unknown.
isImmZero :: ImmEsubs a b -> Maybe (Dict (a ~ 0))
isImmZero (ImmEsubs immd)
  | ImmEsubsData R IsZero _ _ es <- immd, EsNil <- es = Just Dict
  | otherwise = Nothing

-- | Is substitution addition by some number or empty? Answers @False@ if
-- unknown.
isImmAddOrZero :: ImmEsubs a b -> Bool
isImmAddOrZero (ImmEsubs (ImmEsubsData _ _ _ _ EsNil)) = True
isImmAddOrZero _ = False

-- | Prepending to substitution: @mkImmCons s imm@ sends @0@ to @s@ and
-- @i + 1@ to @imm(i)@.
mkImmCons :: forall a b. T b -> ImmEsubs a b -> ImmEsubs (a + 1) b
mkImmCons s (ImmEsubs (immd :: ImmEsubsData _ _ d _))
  | ImmEsubsData R isZero KWord# KWord# (es :: Esubs a' b') <- immd =
    withDict (K.plusAss @d @a' @1) $
    withDict (K.plusKnown @a' @1) $
    ImmEsubs (ImmEsubsData R isZero KWord# KWord# (mkEsCons s es))

-- | Like 'mkImmCons', but for Esubs.
mkEsCons :: forall a b. T b -> Esubs a b -> Esubs (a + 1) b
mkEsCons s es =
  withDict (K.leRefl @b) $
  withDict (K.minusRefl @b) $
  EsCons R KWord# s es

-- | Prefixing substitution: @mkImmPre s imm@ sends @0@ to @s@ and
-- @i + 1@ to @termAdd1 (imm(i))@.
mkImmPre :: forall a b. T (b + 1) -> ImmEsubs a b ->
  ImmEsubs (a + 1) (b + 1)
mkImmPre s imm = mkImmCons s (mkImmAdd# @1 KWord# imm)

-- | Prefixing substitution with newest variable; sends identity to
-- identity.
mkImmPreVar :: forall a b. ImmEsubs a b ->
  ImmEsubs (a + 1) (b + 1)
mkImmPreVar imm
  | Just Dict <- isImmId imm = immId
mkImmPreVar imm = mkImmPre (newestVar @b) imm

-- | Variable of index zero.
newestVar :: forall a. T (a + 1)
newestVar =
  withDict (K.leftPlusExpans @1 @a) $ Refer R (kWord# @0)

newestVarProxy :: proxy b -> T (b + 1)
newestVarProxy (_ :: _ b) = newestVar @b

-- | Lookup variable in substitution. If first argument is not Refer{},
-- then throws exception.
termLookup :: forall m n. T m -> ImmEsubs m n -> T n
termLookup r@(Refer{}) !imm
  | Just Dict <- isImmId imm = r
termLookup (Refer R (KWord# :: KWord# i)) imm
  | ImmEsubs (immd :: ImmEsubsData _ _ d _) <- imm
  , ImmEsubsData R isZero KWord# KWord# (es :: Esubs a n) <- immd =
    case (K.ltOrGeKnown @i @a, isZero) of
      -- If @i < a@, then look at the Esubs and lift.
      (Left Dict, _) -> withIndexEsubs @i KWord# es $
        \ KWord# (a :: Term o) ->
          withDict (K.rightMinusLeftPlus @o @n) $
          termAdd# (kWord# @(n - o)) a
      -- If @i >= a@, we add the obvious value.
      (Right Dict, IsUnk) ->
        -- Summary of code below: result is the
        -- variable @(n - d) + (i - a)@.
        withDict (K.rightPlusComplRightPlus @m @i @1) $
        let
          r' :: Term (m - i)
          r' = newestVar @(m - (i + 1))
        in
        withDict (K.rightMinusPlus @n @d) $
        withDict (K.rightPlusMinus @d @a) $
        withDict (K.leCoweakRightPlus @i @1 @m) $
        withDict (K.rightMinusPlus @m @i) $
        withDict (K.leftPlusRightMinusComm @(m - i) @i @a) $
        withDict (K.plusAss @(n - d) @(m - i) @(i - a)) $
        withDict (K.rightPlusComm @(n - d) @(m - i) @(i - a)) $
        withDict (K.rightPlusMinus @(n - d + (i - a)) @(m - i)) $
        withDict (K.leftPlusExpans @(m - i) @(n - d + (i - a))) $
        withDict (K.rightMinusLeftPlus @(m - i) @n) $
        withDict (K.minusKnown @i @a) $
        withDict (K.plusKnown @(n - d) @(i - a)) $
        termAdd# @(n - (m - i)) KWord# r'
      -- If @i >= a@ but @d == 0@, contradiction.
      (Right Dict, IsZero) ->
        withDict (K.leTrans @(i + 1) @a @i) $
        case withDictEq (K.rightPlusLeSame @i @1) of {}
termLookup _ _ = bug "termLookup" "not Refer"

-- | Lookup in Esubs. The last argument is a continuation so that we can lift
-- the term all in one go.
withIndexEsubs :: forall i m n r. (i + 1 <= m) =>
  KWord# i -> Esubs m n ->
  (forall o. (o <= n) => KWord# (n - o) -> T o -> r) -> r
withIndexEsubs k esinit cont =
  withDict (K.minusRefl @n) $ withDict (K.leRefl @n) $
  go k (kWord# @0) esinit
  where
  -- The last argument is the part which was not observed yet.
  go :: forall i' m' n'. (i' + 1 <= m', n' <= n) =>
    KWord# i' -> KWord# (n - n') -> Esubs m' n' -> r
  -- The substitution can't be empty.
  go KWord# KWord# EsNil =
    case withDictEq (K.leCoweakLeftPlus @i' @1 @0) of {}
  go KWord# KWord# (EsCons R KWord# s (es :: Esubs m'' n'')) =
    withDict (K.leTrans @n'' @n' @n) $
    withDict (K.minusTrans @n @n' @n'') $
    withDict (K.plusKnown @(n - n') @(n' - n'')) $
    -- If @i' = 0@, return @s@, else look at @es@.
    case K.zeroOrPosKnown @i' of
      Left Dict -> cont KWord# s
      Right Dict ->
        withDict (K.minusKnown @i' @1) $
        withDict (K.rightMinusPlus @i' @1) $
        withDict (K.coleRightPlus @i' @m'' @1) $
        go (kWord# @(i' - 1)) KWord# es

-- | Restriction of substitution. Alternatively, it is composition on the right
-- by lifting @m' -> m@.
withMkEsCole# :: forall m' m n r. (m' <= m) => KWord# (m - m') ->
  Esubs m n ->
  (forall n'. (n' <= n) => KWord# (n - n') -> Esubs m' n' -> r) -> r
withMkEsCole# KWord# esinit cont =
  withDict (K.minusRefl @n) $ withDict (K.leRefl @n) $
  go @m @n KWord# KWord# esinit
  where
  -- The last argument is the part which was not observed yet.
  go :: forall m1 n1. (m' <= m1, n1 <= n) =>
    KWord# (m1 - m') -> KWord# (n - n1) ->
    Esubs m1 n1 -> r
  -- If substitution is empty, we can't restrict it anymore.
  go KWord# KWord# EsNil =
    withDict (K.leZero @m') $
    cont @n1 KWord# EsNil
  go KWord# KWord# es@(EsCons R KWord# _ (es2 :: Esubs m2 n2)) =
    withDict (K.leTrans @n2 @n1 @n) $
    withDict (K.minusTrans @n @n1 @n2) $
    withDict (K.plusKnown @(n - n1) @(n1 - n2)) $
    withDict (K.rightPlusMinus @m2 @1) $
    -- If @m1 = m'@, done; otherwise cut one element.
    case K.zeroOrPosKnown @(m1 - m') of
      Left Dict ->
        withDict (K.rightMinusPlus @m1 @m') $
        cont @n1 KWord# es
      Right Dict ->
        withDict (K.leMinusSwap @1 @m1 @m') $
        withDict (K.rightMinusComm @m1 @m' @1) $
        withDict (K.rightMinusPlus @(m1 - m') @1) $
        withDict (K.rightPlusMinusKnown @(m1 - m' - 1) @1) $
        go @(m1 - 1) @n2 KWord# KWord# es2

type S = ImmEsubs

-- | Restriction of substitutions, like 'withMkEsCole#'.
withMkImmCole# :: forall m' m n r. (m' <= m) => KWord# (m - m') ->
  S m n ->
  (forall n'. (n' <= n) => KWord# (n - n') -> S m' n' -> r) -> r
withMkImmCole# mm@KWord# imm cont
  | ImmEsubs (immd :: ImmEsubsData _ _ d _) <- imm
  , ImmEsubsData R IsUnk KWord# KWord# (es :: Esubs m'' _) <- immd =
    -- If @m''@ is too small, the Esubs will be fully discarded and only
    -- the tail of the substitution remains.
    case K.leOrGtKnown @m'' @(m - m') of
      Left Dict ->
        withDict (K.rightMinusPlus @n @d) $
        withDict (K.rightPlusMinus @d @m'') $
        withDict (K.leMinusSwap @m'' @m @m') $
        withDict (K.leftPlusRightMinusComm @(n - d) @(m - m'') @m') $
        withDict (K.rightMinusComm @m @m' @m'') $
        withDict (K.leftPlusExpans @m' @(n - d + (m - m'' - m'))) $
        withDict (K.plusAss @(n - d) @(m - m'' - m') @m') $
        withDict (K.rightMinusPlus @(m - m'') @m') $

        withDict (K.minusKnown @(m - m') @m'') $
        withDict (K.plusKnown @(n - d) @(m - m' - m'')) $
        cont KWord# immId
      Right Dict ->
        withDict (K.leCoweakRightPlus @(m - m') @1 @m'') $
        -- GHC bug(?) workaround.
        let mev = dictToKWord# (K.minusKnown @m'' @(m - m')) in
        withDict (K.complLe @m'' @(m - m')) $
        withDict (K.complCompl @m'' @(m - m')) $
        withMkEsCole# @(m'' - (m - m')) KWord# es $
          \ KWord# (esR :: Esubs _ n') ->
            withDict (K.leftPlusRightMinusComm @d @m'' @(m - m')) $
            withDict (K.complCompl @m @m') $
            case K.leOrGtKnown @(n - n') @(n - d) of
              Left Dict ->
                withDict (K.coleCompl @n @n' @d) $
                withDict (K.complMinusCompl @n @d @n') $
                let kev = dictToKWord# (K.minusKnown @(n - d) @(n - n')) in
                let
                  immdR :: ImmEsubsData _ n' d _
                  immdR = ImmEsubsData R IsUnk kev mev esR
                in cont KWord# (ImmEsubs immdR)
              Right Dict ->
                withDict (K.leCoweakRightPlus @(n - d) @1 @(n - n')) $
                withDict (K.coleCompl @n @d @n') $
                withDict (K.complMinusCompl @n @n' @d) $
                let kev = dictToKWord# (K.minusKnown @(n - n') @(n - d)) in
                withDict (K.rightMinusLeftPlus @n' @d) $
                withDict (K.leRefl @d) $
                withDict (K.minusRefl @d) $
                let
                  immdR :: ImmEsubsData _ d d _
                  immdR = ImmEsubsData R IsUnk KWord# mev
                    (mkEsAdd# kev esR)
                in cont KWord# (ImmEsubs immdR)
  | ImmEsubs immd <- imm
  , ImmEsubsData R IsZero _ k (es :: Esubs m1 _) <- immd =
    withDict (K.complLe @m @m') $
    withDict (K.complCompl @m @m') $
    withMkEsCole# @m' KWord# es $
      \ KWord# (esR :: Esubs _ n') ->
        let
          immdR :: ImmEsubsData _ n' _ _
          immdR = -- GHC bug(?) workaround.
            ImmEsubsData R IsZero KWord#
              (K.minusK# @m @(m - m') k mm)
              esR
              --  ImmEsubsData R IsZero KWord# KWord# esR
        in cont KWord# (ImmEsubs immdR)

-- | Like 'withMkImmCole#', but without continuation.
mkImmCole# :: forall m' m n. (m' <= m) => KWord# (m - m') ->
  S m n -> S m' n
mkImmCole# k imm = withMkImmCole# k imm $
  \ KWord# (immR :: S m' n') ->
    withDict (K.rightMinusLeftPlus @n' @n) $
    mkImmAdd# @(n - n') KWord# immR

dictToKWord# :: Dict (KnownWord m) -> KWord# m
dictToKWord# Dict = KWord#

-- | Composition of substitutions.
mkImmCompose :: forall m n o. S n o -> S m n -> S m o
mkImmCompose !immNO !immMN
  | Just Dict <- isImmId immNO = immMN
  | Just Dict <- isImmId immMN = immNO
  | Just Dict <- isImmZero immMN = immZero
mkImmCompose immNO immMN
  | ImmEsubs (immdNO :: ImmEsubsData _ _ d _) <- immNO
  , ImmEsubs (immdMN :: ImmEsubsData _ _ e _) <- immMN
  , ImmEsubsData R iszn KWord# KWord# (esNO :: Esubs n' _) <- immdNO
  , ImmEsubsData R IsUnk KWord# m'@KWord# (esMN :: Esubs m' _) <- immdMN =
    -- The substitution @immMN@ sends variables @i >= m'@ to
    -- @i - m' + (n - e)@. If @n' < (n - e)@, then @i - m' + (n - e) > n'@,
    -- so that the composition sends these variables @i >= m'@ to
    -- @i - m' + (n - e) - n' + (o - d)@. Then the tail of the composition
    -- has also size @e@. The head is obtained applying @mkSubs@ to
    -- each element.
    case (K.ltOrGeKnown @n' @(n - e), iszn) of
      (Left Dict, IsUnk) ->
        withDict (K.leCoweakRightPlus @n' @1 @(n - e)) $
        withDict (K.plusAss @(o - d) @(n - e - n') @e) $
        withDict (K.rightMinusPlusComm @(n - e) @n' @e) $
        withDict (K.rightMinusPlus @n @e) $
        withDict (K.rightPlusMinus @d @n') $
        withDict (K.rightMinusPlus @o @d) $
        withDict (K.rightPlusMinus @(o - d + (n - e - n')) @e) $
        withDict (Dict :: Dict ((o - d + (n - e - n')) ~ (o - e))) $
        withDict (K.minusKnown @(n - e) @n') $
        withDict (K.plusKnown @(o - d) @(n - e - n')) $
        -- GHC bug workaround.
        let k = dictToKWord# (Dict :: Dict (KnownWord (o - e))) in
        withDict (K.leftPlusExpans @e @(o - d + (n - e - n'))) $
        let
          immdRes :: ImmEsubsData _ o e _
          immdRes = ImmEsubsData R IsUnk k m'
            (immComposeEsubs immNO esMN)
        in ImmEsubs immdRes
      (Left Dict, IsZero) ->
        -- As @n' + 1 = n + 1 <= n - e@ is impossible:
        withDict (K.complLe @n @e) $
        withDict (K.leTrans @(n + 1) @(n - e) @n) $
        case withDictEq (K.rightPlusLeSame @n @1) of {}
      -- If @n' >= (n - e)@ then some of tail of the composition is smaller.
      -- Precisely smaller by @n' - (n - e)@. Thus the head has to be larger
      -- by @n' - (n - e)@. These variables added are older than the others,
      -- and are obtained by restriction of @esNO@.
      (Right Dict, _) ->
        withDict (K.complCompl @n' @(n - e)) $
        withDict (K.complLe @n' @(n - e)) $
        withDict (K.plusAss @d @(n' - (n - e)) @m') $
        withDict (K.leftPlusRightMinusComm @d @n' @(n - e)) $
        withDict (K.complCompl @n @e) $
        withDict (K.minusKnown @n' @(n - e)) $
        -- Get the @n' - (n - e)@ terms to put after.
        withMkEsCole# @(n' - (n - e)) KWord# esNO $
          \ KWord# esNO' ->
            let evp = K.plusKnown @(n' - (n - e)) @m' in
            let kevp = dictToKWord# evp in
            let
              esR :: Esubs (n' - (n - e) + m') o
              esR = immComposeEsubsBefore# immNO esMN KWord# esNO'
            in case iszn of
              IsUnk ->
                let
                  ev :: Dict (KnownWord (o - d))
                  ev = Dict
                  kev = dictToKWord# ev
                  immdR :: ImmEsubsData _ o d _
                  immdR = ImmEsubsData R IsUnk kev kevp esR
                in ImmEsubs immdR
              IsZero ->
                let
                  immdR :: ImmEsubsData _ o 0 _
                  immdR = ImmEsubsData R IsZero KWord# kevp esR
                in ImmEsubs immdR
  | ImmEsubs immdMN <- immMN
  , ImmEsubsData R IsZero _ KWord# (esMN :: Esubs m' _) <- immdMN =
      let
        immdR = ImmEsubsData R IsZero KWord# KWord#
          (immComposeEsubs immNO esMN)
      in ImmEsubs immdR

-- | Apply @mkSubs@ to each element of Esubs.
immComposeEsubs :: forall. forall m n o.
  S n o -> Esubs m n -> Esubs m o
immComposeEsubs = applyImmEsubs immComposeEsubsH
  -- Help W/W in recursive functions.
  where
  immComposeEsubsH :: forall m n o.
    FromImmEsubs n o (Esubs m n -> Esubs m o)
  immComposeEsubsH = FromImmEsubs $ \ imm1 es2 -> case isImmAdd imm1 of
    Just Dict -> withDict (K.rightMinusLeftPlus @n @o) $
      mkEsAdd# (kWord# @(o - n)) es2
    _ -> case es2 of
      EsNil -> EsNil
      EsCons R KWord# s' (es2' :: Esubs m' n') ->
        withMkImmCole# @n' KWord# imm1 $
          \ KWord# (imm1' :: S _ o') ->
            EsCons R KWord# (mkSubs s' imm1') $
            applyImmEsubs immComposeEsubsH imm1' es2'

-- | Apply @mkSubs@ to each element of Esubs, then put the other Esubs lifted.
immComposeEsubsBefore# :: forall. forall m n o o' p. (o' <= o) =>
  S n o -> Esubs m n ->
  KWord# (o - o') -> Esubs p o' ->
  Esubs (p + m) o
immComposeEsubsBefore# = applyImmEsubs immComposeEsubsBeforeH
  where
  immComposeEsubsBeforeH :: forall m n o o' p. (o' <= o) =>
    FromImmEsubs n o (Esubs m n ->
    KWord# (o - o') -> Esubs p o' ->
    Esubs (p + m) o)
  immComposeEsubsBeforeH = FromImmEsubs $ \ imm1 es2 KWord# esP -> case es2 of
    _
      | Just Dict <- isImmAdd imm1 ->
      withDict (K.rightMinusLeftPlus @n @o) $
      mkEsAddBefore# (KWord# @(o - n)) es2 KWord# esP
    EsNil ->
      withDict (K.rightMinusLeftPlus @o' @o) $
      mkEsAdd# KWord# esP
    EsCons R KWord# s' (es2' :: Esubs m' n') ->
      withDict (K.plusAss @p @m' @1) $
      withMkImmCole# @n' KWord# imm1 $
        \ KWord# (imm1' :: S _ o'') ->
        case K.leOrGtKnown @(o - o'') @(o - o') of
          -- Case @o - o'' <= o - o'@, that is @o'' >= o'@.
          Left Dict ->
            withDict (K.coleCompl @o @o'' @o') $
            withDict (K.complMinusCompl @o @o' @o'') $
            withDict (K.minusKnown @(o - o') @(o - o'')) $
            EsCons R KWord# (mkSubs s' imm1') $
            applyImmEsubs immComposeEsubsBeforeH imm1' es2' KWord# esP
          -- Case @o'' + 1 <= o'@. We reached the part of @es2@ whose
          -- scope size is smaller than of @esP@. As EsCons can only
          -- increase the scope size, we have to lift to get scope size @o'@.
          Right Dict ->
            withDict (K.leCoweakRightPlus @(o - o') @1 @(o - o'')) $
            withDict (K.coleCompl @o @o' @o'') $
            withDict (K.complMinusCompl @o @o'' @o') $
            withDict (K.minusKnown @(o - o'') @(o - o')) $
            let
              imm1'C :: S n' o'
              imm1'C =
                withDict (K.rightMinusLeftPlus @o'' @o') $
                mkImmAdd# (kWord# @(o' - o'')) imm1'
            in
            withDict (K.leRefl @o') $ withDict (K.minusRefl @o') $
            EsCons R KWord# (mkSubs s' imm1'C) $
            applyImmEsubs immComposeEsubsBeforeH imm1'C es2' KWord# esP

-- | Lift some @Esubs@ and put after other @Esubs@ lifted.
mkEsAddBefore# :: forall a m n o' p. (o' <= n + a) =>
  KWord# a -> Esubs m n ->
  KWord# (n + a - o') -> Esubs p o' ->
  Esubs (p + m) (n + a)
mkEsAddBefore# a@KWord# es KWord# EsNil = mkEsAdd# a es
mkEsAddBefore# KWord# es KWord# esP
  | EsNil <- es =
    withDict (K.rightMinusLeftPlus @o' @(n + a)) $
    mkEsAdd# (kWord# @(n + a - o')) esP
  | EsCons R KWord# s' (es' :: Esubs m' n') <- es =
    withDict (K.plusAss @p @m' @1) $
    -- Compare @n'@ with @o'@.
    withDict (K.plusComm @n @a) $
    withDict (K.leftPlusRightMinusComm @a @n @n') $
    withDict (K.plusKnown @a @(n - n')) $
    withDict (K.leWeakRightPlus @n' @n @a) $
    case K.leOrGtKnown @(n + a - n') @(n + a - o') of
      Left Dict -> -- @n' >= o'@.
        withDict (K.coleCompl @(n + a) @n' @o') $
        withDict (K.complMinusCompl @(n + a) @o' @n') $
        withDict (K.minusKnown @(n + a - o') @(n + a - n')) $
        EsCons R KWord# s' $
          mkEsAddBefore# @0 KWord# es' KWord# esP
      Right Dict -> -- @n' + 1 <= o'@.
        withDict (K.leCoweakRightPlus @(n + a - o') @1 @(n + a - n')) $
        withDict (K.rightMinusLeftPlus @n' @(n + a)) $
        withDict (K.leRefl @(n + a)) $
        withDict (K.minusRefl @(n + a)) $
        EsCons R (kWord# @0) (termAdd# @(n + a - n') KWord# s') $
          mkEsAddBefore# @0 KWord# (mkEsAdd# @(n + a - n') KWord# es')
            KWord# esP

-- | Like an application; substitute the newest variable by a chosen term.
mkSubstitute :: forall a. Term a -> Term (a + 1) -> Term a
mkSubstitute x b_a = mkSubs b_a (mkImmCons x immId)

-- | Make a substitution suspension.
mkSubs :: forall a b. T a -> S a b -> T b
mkSubs t imm
  | Just Dict <- isImmAdd imm =
    withDict (K.rightMinusLeftPlus @a @b) $
    termAdd# (kWord# @(b - a)) t
mkSubs t@(Refer{}) imm = termLookup t imm
mkSubs (Univ l) _ = mkUniv l
mkSubs (PrimUnNat x) _ = mkPrimUnNat x
mkSubs (PrimBbNat x) _ = mkPrimBbNat x
-- Like in 'termAdd#'
mkSubs t imm = withTopSubs t $ \ t' imm' ->
  let
    immC = mkImmCompose imm imm'
    eImmC = Subs t' immC (Whnf eImmC_r)
    eImmC_r = buildWhnf eImmC
  in eImmC

-- | Get outer substitution, or identity if none.
withTopSubs :: forall m r. T m -> (forall n. T n -> S n m -> r) -> r
withTopSubs (Subs t imm _) cont = cont t imm
withTopSubs t cont = cont t immId

-- | Suspension of constants.
termZero :: T 0 -> T m
termZero a = mkSubs a immZero

-- * Term construction (without checking types)

mkUniv :: forall m. Level -> T m
mkUniv = Univ

mkPi :: forall m. T m -> T (m + 1) -> T m
mkPi = mkPiRBN defaultRBN

mkPiRBN :: forall m. RBN -> T m -> T (m + 1) -> T m
mkPiRBN rbn a b_a = Pi a b_a rbn

mkLam :: forall m. T m -> T (m + 1) -> T m
mkLam = mkLamRBN defaultRBN

mkLamRBN :: forall m. RBN -> T m -> T (m + 1) -> T m
mkLamRBN rbn a e_a = Lam a e_a rbn

mkCn :: forall m. Int -> T m -> V (T m) -> T m
mkCn = mkCnRBN defaultRBN

mkCnRBN :: forall m. RBN -> Int -> T m -> V (T m) -> T m
mkCnRBN rbn@(RBN r _) i ti as
  -- Abbreviations of naturals:
  | UnNatElem <- r, 0 == i = mkPrimUnNat 0
  | UnNatElem <- r, 1 == i, Just m <- isPrimUnNat (indexSmallArray as 0) =
    mkPrimUnNat (m + 1)
  | BbNatElem <- r, 0 == i = mkPrimBbNat 0
  | BbNatElem <- r, 1 == i, Just m <- isPrimBbNat (indexSmallArray as 0) =
    mkPrimBbNat (shiftL m 1 + 1)
  | BbNatElem <- r, 2 == i, Just m <- isPrimBbNat (indexSmallArray as 0) =
    mkPrimBbNat (shiftL (m + 1) 1)
  | otherwise = Cn i ti as rbn

mkApp :: forall m. T m -> T m -> T m
mkApp = mkAppRBN defaultRBN

mkAppRBN :: forall m. RBN -> T m -> T m -> T m
mkAppRBN rbn h x =
  let
    hx = App h x (Whnf hx_r) rbn
    hx_r = buildWhnf hx
  in hx

mkRec :: forall m n. RecData n m -> T m -> T m
mkRec = mkRecRBN defaultRBN

mkRecRBN :: forall m n. RBN -> RecData n m -> T m -> T m
mkRecRBN rbn rd a =
  let
    rda = Rec rd a (Whnf rda_r) rbn
    rda_r = buildWhnf rda
  in rda

data SomeRecData m = forall n. SomeRecData !(RecData n m)

-- Special terms

mkUniv0 :: T m
mkUniv0 = Univ zvalZero

mkUniv0Sort :: IndSort m
mkUniv0Sort = IndSort zvalZero (TeleEnd TeleEndEmpty)

mkPointCnSig :: CnSig m
mkPointCnSig = CnSigAnns mempty

mkPointCnInn :: CnInn m
mkPointCnInn = TeleEnd (CnInnEnd mempty)

-- | Unary naturals. Agda notation:
--
-- @
-- data ℕ where
--   zero : ℕ
--   succ : ℕ → ℕ
-- @
mkUnNatInd :: T m
mkUnNatInd = termZero (Ind (IndData mkUniv0Sort cs))
  where
  !cs = smallArrayFromListN 2 [c0, c1]
  !c0 = mkPointCnSig
  !c1 = CnSigR (Built mkUnNatInd) mkPointCnInn mkPointCnSig defaultRBN

-- | Binary bijective naturals. Agda notation:
--
-- @
-- data ℕ where
--   0 : ℕ
--   1+2[_] : ℕ → ℕ -- suc of double
--   2[1+_] : ℕ → ℕ -- double of suc
-- @
mkBbNatInd :: T m
mkBbNatInd = termZero (Ind (IndData mkUniv0Sort cs))
  where
  !cs = smallArrayFromListN 3 [c0, c2p, c2p]
  !c0 = mkPointCnSig
  !c2p = CnSigR (Built mkBbNatInd) mkPointCnInn mkPointCnSig defaultRBN

mkPrimUnNat :: Natural -> T m
mkPrimUnNat = PrimUnNat

mkPrimBbNat :: Natural -> T m
mkPrimBbNat = PrimBbNat

-- | Ordering type. Agda notation:
--
-- @
-- data Ordering where
--   LT : Ordering
--   EQ : Ordering
--   GT : Ordering
-- @
mkOrder :: T m
mkOrder = termZero (Ind (IndData mkUniv0Sort cs))
  where
  !cs = runSmallArray $ newSmallArray 3 mkPointCnSig

mkOrderLt, mkOrderEq, mkOrderGt :: Term m
(mkOrderLt, mkOrderEq, mkOrderGt) = (f 0, f 1, f 2)
  where
  f i = Cn i mkOrder mempty defaultRBN

-- | The type @ℕ × ℕ@ (square of unary naturals).
mkSqUnNatInd :: T m
mkSqUnNatInd = termZero (Ind (IndData mkUniv0Sort cs))
  where
  cs = pure c
  c = CnSigNR mkUnNatInd
    (CnSigNR mkUnNatInd mkPointCnSig defaultRBN) defaultRBN

-- | The type @ℕ × ℕ@ (square of bb naturals).
mkSqBbNatInd :: T m
mkSqBbNatInd = termZero (Ind (IndData mkUniv0Sort cs))
  where
  cs = pure c
  c = CnSigNR mkBbNatInd
    (CnSigNR mkBbNatInd mkPointCnSig defaultRBN) defaultRBN

-- | Make a member of @mkSqUnNatInd@.
mkPairPrimUnNat :: Natural -> Natural -> T m
mkPairPrimUnNat !q !r = Cn 0 mkSqUnNatInd as defaultRBN
  where
  as = createSmallArray' 2 $ \ case
    0 -> mkPrimUnNat q
    _ -> mkPrimUnNat r

-- | Make a member of @mkSqBbNatInd@.
mkPairPrimBbNat :: Natural -> Natural -> T m
mkPairPrimBbNat !q !r = Cn 0 mkSqBbNatInd as defaultRBN
  where
  as = createSmallArray' 2 $ \ case
    0 -> mkPrimBbNat q
    _ -> mkPrimBbNat r

-- | Truncated subtraction: negative results are changed to zero.
trSub :: Natural -> Natural -> Natural
trSub a b = if a <= b then 0 else a - b

-- | Quotient, supporting division by zero, in which case quotient is zero and
-- remainder is the same as dividend.
qtRem :: Natural -> Natural -> (Natural, Natural)
qtRem !a 0 = (0, a)
qtRem !a !b = quotRem a b

-- * Implementation of reductions

-- | Matches (Subs of) App.
isApp :: T m -> Maybe (T m, T m)
isApp a = withTopSubs a $ \ a' imm' -> case a' of
  App h' c' _ _ -> Just (mkSubs h' imm', mkSubs c' imm')
  _ -> Nothing

-- | Matches (Subs of) App of App.
isApp2 :: T m -> Maybe (T m, T m, T m)
isApp2 (App (isApp -> Just (h, a)) b _ _) = Just (h, a, b)
isApp2 _ = Nothing
{-# INLINE isApp2 #-}

-- | Matches PrimUnNat only.
isPrimUnNat :: T m -> Maybe Natural
isPrimUnNat (PrimUnNat m) = Just m
isPrimUnNat _ = Nothing

-- | Matches PrimBbNat only.
isPrimBbNat :: T m -> Maybe Natural
isPrimBbNat (PrimBbNat m) = Just m
isPrimBbNat _ = Nothing

-- | True if term “obviously” has no free variables.
obviouslyClosed :: T m -> Bool
obviouslyClosed = goLt 0
  where
  -- @goLt w a = True@ implies: @a@ depends “relevantly” only on the
  -- @w@ newest variables. False means unknown.
  goLt :: forall m. Word -> T m -> Bool
  goLt !w (Refer R (KWord# :: KWord# i)) = wordVal_ @i < w
  -- The type in Lam is irrelevant.
  goLt !w (Lam _ e _) = goLt (w + 1) e
  goLt !w (App h x _ _) = goLt w h && goLt w x
  goLt !w (Cn _ _ as _) = all (goLt w) as
  goLt !_ PrimUnNat{} = True
  goLt !_ PrimBbNat{} = True
  goLt !w (Subs (s :: T n) imm _)
    | Just Dict <- isImmAdd imm, let mn = wordVal_ @(m - n) =
      goLt (if w <= mn then 0 else w - mn) s
    | Just Dict <- isImmZero imm = True
      -- The usefulness of having 'IsZero' in the definition of
      -- 'ImmEsubsData'. Without 'IsZero', we would have to
      -- pass a 'KnownWord m' argument, only to compare to
      -- zero here. But we would have to adapt most of
      -- other operations to pass that 'KnownWord m'
      -- argument.
  -- The rest I give up.
  goLt _ _ = False

-- Rules: App deletes Lam; Rec deletes Cn.

-- | Used in the implementation of 'mkSubs'.
buildWhnf :: T m -> T m
buildWhnf (isApp2 -> Just ((getRBN -> RBN !r _), PrimUnNat x, PrimUnNat y))
  -- Only /UnNat/ operations.
  | OpUnAdd <- r = mkPrimUnNat (x + y)
  | OpUnMul <- r = mkPrimUnNat (x * y)
  | OpUnCmp <- r = case compare x y of
    LT -> mkOrderLt
    EQ -> mkOrderEq
    GT -> mkOrderGt
  | OpUnTrSub <- r = mkPrimUnNat (trSub x y)
  | OpUnQtRem <- r = case qtRem x y of
    (q, rm) -> mkPairPrimUnNat q rm
buildWhnf (isApp2 -> Just ((getRBN -> RBN !r _), PrimBbNat x, PrimBbNat y))
  -- Only /BbNat/ operations.
  | OpBbAdd <- r = mkPrimBbNat (x + y)
  | OpBbMul <- r = mkPrimBbNat (x * y)
  | OpBbCmp <- r = case compare x y of
    LT -> mkOrderLt
    EQ -> mkOrderEq
    GT -> mkOrderGt
  | OpBbTrSub <- r = mkPrimBbNat (trSub x y)
  | OpBbQtRem <- r = case qtRem x y of
    (q, rm) -> mkPairPrimBbNat q rm
buildWhnf (App (getRBN -> RBN !r _) a _ _)
  | OpUnFromBb <- r, PrimBbNat x <- a = mkPrimUnNat x
  | OpBbFromUn <- r, PrimUnNat x <- a = mkPrimBbNat x
buildWhnf t@(App h x _ _) =
  withTopSubs (getWhnf h) $ \ h' imm' -> case h' of
    Lam _ e_a' _ -> getWhnf $ mkSubs e_a' (mkImmCons x imm')
    _ -> t
buildWhnf (Rec rd a _ r)
  | RBN EqIndElem _ <- r, obviouslyClosed a =
    applyRecDataToArgs rd 0 mempty immZero
buildWhnf t@(Rec rd a _ _) =
  withTopSubs (getWhnf a) $ \ a' imm' -> case a' of
    Cn i _ args' _ -> getWhnf $
      applyRecDataToArgs rd i args' imm'
    PrimUnNat x
      | 0 == x -> getWhnf $ applyRecDataToArgs rd 0 mempty immZero
      | otherwise -> getWhnf $
        applyRecDataToArgs rd 1 (pure $! mkPrimUnNat (x - 1)) immZero
    PrimBbNat x
      | 0 == x -> getWhnf $
        applyRecDataToArgs rd 0 mempty immZero
      | testBit x 0 {- @x@ is odd -} -> getWhnf $
        applyRecDataToArgs rd 1 (pure $! mkPrimBbNat $ shiftR x 1) immZero
        -- As shiftR (2 * y + 1) 1 = y.
      | otherwise -> getWhnf $
        applyRecDataToArgs rd 2 (pure $! mkPrimBbNat $ shiftR x 1 - 1) immZero
        -- As shiftR (2 * y + 2) 1 = y + 1.
    _ -> t
buildWhnf t@(Subs s _ _)
  | Pi{} <- s = t
  | Lam{} <- s = t
  | Ind{} <- s = t
  | Cn{} <- s = t
buildWhnf (Subs s imm _) =
  withTopSubs (getWhnf s) $ \ s' imm' ->
    let
      immC = mkImmCompose imm imm'
    in case s' of
      Refer{} -> getWhnf $ termLookup s' immC
      App f' a' _ rbn -> getWhnf $
        mkAppRBN rbn (mkSubs f' immC) (mkSubs a' immC)
      Rec rd' a' _ rbn -> getWhnf $
        mkRecRBN rbn (mkRecDataSubs rd' immC) (mkSubs a' immC)
      Univ l -> mkUniv l
      PrimUnNat x -> mkPrimUnNat x
      PrimBbNat x -> mkPrimBbNat x
      _ ->
        let
          -- Here @s'@ is WHNF and will not reduce anymore with substitutions.
          -- Also @s'@ is not Refer, Univ, PrimUnNat, PrimBbNat nor Subs,
          -- so we can put it under Subs.
          y = Subs s' immC (Whnf y)
        in y
buildWhnf _ = bug "buildWhnf" "not App, Rec or Subs"

-- | Like 'mkSubs'.
mkRecDataSubs :: RecData m n -> S n o -> RecData m o
mkRecDataSubs rd@(RecData immr ti rp rcs) imm
  | Just Dict <- isImmId imm = rd
  | otherwise =
    let
      immC = mkImmCompose imm immr
    in RecData immC ti rp rcs

applyRecDataToArgs :: RecData m n -> Int -> V (T o) -> S o n -> T n
applyRecDataToArgs !rd !i !args !imm
  | RecData immr _ _ rcs <- rd
  , let rc = indexSmallArray rcs i =
    applyImmEsubs
      (applyImmEsubs (applyRecData applyRecCaseHelper rd rc) immr
        args 0) imm

applyRecCaseHelper :: forall m n o. forall r.
  FromRecData m n (RecCase r -> FromImmEsubs r n
    (V (T o) -> Int -> FromImmEsubs o n (T n)))
applyRecCaseHelper = FromRecData $ \ !rd rcInit ->
  FromImmEsubs $ \ immrInit !args idxInit ->
  FromImmEsubs $ \ !imm ->
    let
      bugLackOfArgs = bug "applyRecCaseHelper" "lack of arguments"
      go :: RecCase r -> FromImmEsubs r n (Int -> T n)
      go = \ rc -> FromImmEsubs $ \ !immr !idx ->
        case rc of
          RecCaseEnd r
            | length args == idx -> mkSubs r immr
            | otherwise -> bug "applyRecCaseHelper" "arguments remain"
          RecCaseNR rc_a _
            | Just a <- maybeIndexSmallArray args idx ->
              applyImmEsubs (go rc_a) (mkImmCons (mkSubs a imm) immr) (idx + 1)
            | otherwise -> bugLackOfArgs
          RecCaseR rc_h_a _ _ (Built cin)
            | Just a <- maybeIndexSmallArray args idx ->
              let
                a' = mkSubs a imm
                h' = applyImmEsubs (applyRecData mkRecHypElem rd cin) immr a'
              in applyImmEsubs (go rc_h_a) (mkImmCons h' (mkImmCons a' immr))
                (idx + 1)
            | otherwise -> bugLackOfArgs
    in applyImmEsubs (go rcInit) immrInit idxInit

-- | The recursive hypothesis has to be a sequence of Lams (same types as in
-- CnInn), ending with evidence of adequate application of P (obtained
-- by another recursor).
mkRecHypElem :: FromRecData n m (Tele TeleEndEmpty r ->
  FromImmEsubs r m (T m -> T m))
mkRecHypElem = FromRecData $ \ !rd !cin ->
  FromImmEsubs $ \ !immr !apl -> case cin of
    TeleEnd ~TeleEndEmpty -> mkRec rd apl
    TeleCons b cin_b rbn ->
      let
        b' = mkSubs b immr
        immr1 = mkImmPreVar immr
      in mkLamRBN rbn b' $
        let rd' = mkRecDataSubs rd (immAdd# @1 KWord#) in
          applyImmEsubs (applyRecData mkRecHypElem rd' cin_b) immr1 $
          mkApp (termAdd1 apl) (newestVarProxy b')

-- * Repeated applications

{- | Computes:

@
applyThose h []     = h
applyThose h [x]    = mkApp h x
applyThose h [x, y] = mkApp (mkApp h x) y
...
@
-}
applyThose :: Foldable f => T m -> f (T m) -> T m
applyThose h xs =
  case foldl' (\ (Built apl) x -> Built (mkApp apl x)) (Built h) xs of
    Built res -> res

{- | Computes:

@
applyThose h []     = h
applyThose h [x]    = flip mkApp x h                = mkApp h x
applyThose h [x, y] = flip mkApp x (flip mkApp y h) = mkApp (mkApp h y) x
...
@
-}
applyThoseReverse :: Foldable f => T m -> f (T m) -> T m
applyThoseReverse h xsr =
  case foldr' (\ x (Built apl) -> Built (mkApp apl x)) (Built h) xsr of
    Built res -> res

{-| Computes (abbreviated syntax):

applyRecPred p       σ []                (t:I)     = p[t, σ]
applyRecPred (A:p)   σ [a:A[σ]]          (t:I a)   = p[a, t, σ]
applyRecPred (A:B:p) σ [a:A[σ],b:B[a,σ]] (t:I a b) = p[a, b, t, σ]
...
-}
applyRecPred :: Foldable f => RecPred n -> S n m -> f (T m) -> T m -> T m
applyRecPred (RecPred tl) imm = applyRecPredTele tl imm

applyRecPredTele :: Foldable f =>
  RecPredTele n -> S n m -> f (T m) -> T m -> T m
applyRecPredTele = \ rpt immrpt xs a -> go rpt immrpt (toList xs) a
  where
  go :: RecPredTele n -> S n m -> [T m] -> T m -> T m
  go (TeleCons ~DontCare tl_x _) imm (x : xs) a =
    go tl_x (mkImmCons x imm) xs a
  go (TeleEnd (RecPredTeleEnd p _)) imm [] a = mkSubs p (mkImmCons a imm)
  go _ _ _ _ = bug "applyRecPredTele" "not same quantity"

-- * Using reductions

-- | Matches when getWhnf returns Univ.
levelOfUniverse :: T m -> Maybe Level
levelOfUniverse (getWhnf -> Univ l) = Just l
levelOfUniverse _ = Nothing

-- | Matches when getWhnf returns Pi.
unPi :: T m -> Maybe (T m, T (m + 1))
unPi ty = withTopSubs (getWhnf ty) $ \ ty' imm' -> case ty' of
  Pi a' b_a' _
    | let a = mkSubs a' imm' ->
      Just (a, mkSubs b_a' (mkImmPreVar imm'))
  _ -> Nothing

-- | @isApplicationInductive ti a@ is true when @a@ is definitionally equal to
-- @applyThose ti [x, y, …]@.
isApplicationOfInductive :: T m -> T m -> Maybe (V (T m))
isApplicationOfInductive ti = \ b -> go (0 :: Int) (getWhnf b) []
  where
  -- @a@ is reduced.
  go !n a l = withTopSubs a $ \ a' imm' -> case a' of
    App h' x' _ _ -> go (n + 1) (mkSubs h' imm') (mkSubs x' imm' : l)
    Ind{}
      | defEq a ti -> Just $ smallArrayFromListN n l
    _ -> Nothing

type SubsIndData = Subsend IndData

indDataOfInductive :: T m -> Maybe (SubsIndData m)
indDataOfInductive (getWhnf -> t) = withTopSubs t $ \ t' imm' -> case t' of
  Ind indd' -> Just (Subsend indd' imm')
  _ -> Nothing

-- * Definitional equality

-- | True if arguments occupy the same place in memory. Unsafe as it is not
-- “referentially transparent”.
unsafeSame :: a -> b -> Bool
{-
unsafeSame a b = unsafeDupablePerformIO $ do
  i <- makeStableName $! a
  j <- makeStableName $! b
  pure (eqStableName i j)
-}
unsafeSame = \ a b -> isTrue# (unsafeCoerce go a b)
  where
  go :: a -> a -> Int#
  go a b = reallyUnsafePtrEquality# a b
  {-# NOINLINE go #-}
  -- @go@ compares the pointers stored in registers.
  -- Without NOINLINE, pointers could be stored in the stack,
  -- and I wonder if a GC could intervene and cause false positives.

-- | Determines if two terms are “definitionally equal”, that is, reduce to the
-- same thing.
defEq :: T m -> T m -> Bool
defEq a b
  -- First we try the cheaper syntactical equality.
  | synEqual a b = True
  | otherwise = defEqUR a b

data IsEq m n where
  IsDiff :: IsEq m n
  IsEq :: IsEq m m

sucIsEq :: IsEq m n -> IsEq (m + 1) (n + 1)
-- sucIsEq IsEq = IsEq
-- sucIsEq IsDiff = IsDiff
sucIsEq = unsafeCoerce -- Incomplete CSE in some GHC versions.

orIsEq :: IsEq m n -> IsEq m n -> IsEq m n
orIsEq IsEq _ = IsEq
orIsEq _ r = r

flipIsEq :: IsEq m n -> IsEq n m
-- flipIsEq IsEq = IsEq
-- flipIsEq IsDiff = IsDiff
flipIsEq = unsafeCoerce -- Imcomplete CSE in some GHC versions.

ifIsEq :: IsEq m n -> ((m ~ n) => r) -> r -> r
ifIsEq IsEq a _ = a
ifIsEq IsDiff _ b = b

synEqual :: Term m -> Term m -> Bool
synEqual a a' = synEqualSubs IsEq a immId a' immId

eqKWord# :: forall i j. KWord# i -> KWord# j -> Bool
eqKWord# KWord# KWord# = wordVal_ @i == wordVal_ @j

zipplySmallArray :: (a -> b -> Bool) -> SmallArray a -> SmallArray b -> Bool
zipplySmallArray rel as bs =
  length as == length bs &&
  flip all [0 .. length as - 1] (\ idx ->
    rel (indexSmallArray as idx) (indexSmallArray bs idx))

-- The functions below have arguments with different types,
-- @T m@ and @T n@, so we have few opportunities for mistakes.

-- We will not try to put memoization in @synEqualSubs@ as syntactical equality
-- is not so expensive.

-- | Determines if two terms are syntactically equal after doing the respective
-- substitutions.
--
-- If the first argument is @IsEq@, the two substitions have to be
-- syntactically equal.
synEqualSubs :: forall m n o. IsEq m n ->
  T m -> S m o -> T n -> S n o -> Bool
synEqualSubs isEq a !imm a' !imm'
  -- First the cheapest test.
  | unsafeSame a a', IsEq <- isEq = True
  -- Variables can be substituted, so check them first.
  -- Caution here.
  | Refer R ia <- a = case isEq of
    IsEq -> case a' of
      Refer R ia' -> case isImmId imm of
        Just Dict -> eqKWord# ia ia'
        -- We tested equality against identity in order to not cause
        -- an infinite loop.
        Nothing -> synEqualSubs IsEq
          (termLookup a imm) immId
          (termLookup a' imm') immId
      _ -> case isImmId imm of
        Just Dict -> False -- @Refer@ won't be substituted.
        Nothing -> synEqualSubs IsDiff
          (termLookup a imm) immId
          a' imm -- @imm = imm'@ here.
    IsDiff -> case a' of
      Refer{} -> synEqualSubs IsEq
        (termLookup a imm) immId
        (termLookup a' imm') immId
      _ -> case isImmId imm of
        Just Dict -> False -- @Refer@ won't be substituted.
        Nothing -> synEqualSubs (immSynEqual immId imm')
          (termLookup a imm) immId
          a' imm'
  -- This case is symmetrical.
  | Refer{} <- a' = synEqualSubs (flipIsEq isEq) a' imm' a imm

  | Subs b immb _ <- a = case a' of
    Subs b' immb' _ ->
      let
        immC = mkImmCompose imm immb
        immC' = mkImmCompose imm' immb'
      in synEqualSubs -- XXX. Maybe a shortcut, maybe slower.
        (ifIsEq isEq
          (immSynEqual immb immb' `orIsEq` immSynEqual immC immC')
          (immSynEqual immC immC'))
        b immC
        b' immC'
    _ ->
      let
        immC = mkImmCompose imm immb
      in synEqualSubs
        (immSynEqual immC imm')
        b immC
        a' imm'
  -- Symmetrical.
  | Subs{} <- a' = synEqualSubs (flipIsEq isEq) a' imm' a imm

  | Univ l <- a = case a' of
    Univ l' -> l == l'
    _ -> False

  | Pi c b_c _ <- a = case a' of
    Pi c' b_c' _ ->
      synEqualSubs isEq c imm c' imm' &&
      synEqualSubs (sucIsEq isEq)
        b_c (mkImmPreVar imm)
        b_c' (mkImmPreVar imm')
    _ -> False

  -- Don't check the irrelevant parts.
  | Lam _ e_c _ <- a = case a' of
    Lam _ e_c' _ ->
      synEqualSubs (sucIsEq isEq)
        e_c (mkImmPreVar imm)
        e_c' (mkImmPreVar imm')
    _ -> False

  | App f c _ _ <- a = case a' of
    App f' c' _ _ ->
      synEqualSubs isEq f imm f' imm' &&
      synEqualSubs isEq c imm c' imm'
    _ -> False

  | Ind indd <- a = case a' of
    Ind indd' -> synEqualSubsIndData isEq indd imm indd' imm'
    _ -> False

  -- Don't check the irrelevant parts.
  | Cn i _ bs _ <- a = case a' of
    Cn i' _ bs' _ -> i == i' &&
      zipplySmallArray (\ b b' -> synEqualSubs isEq b imm b' imm')
        bs bs'
    _ -> False

  | Rec rd c _ _ <- a = case a' of
    Rec rd' c' _ _ ->
      synEqualSubs isEq c imm c' imm' &&
      synEqualSubsRecData isEq rd imm rd' imm'
    _ -> False

  | PrimUnNat x <- a = case a' of
    PrimUnNat x' -> x == x'
    _ -> False

  | PrimBbNat x <- a = case a' of
    PrimBbNat x' -> x == x'
    _ -> False

  | otherwise = bug "synEqualSubs" "unexpected case"

synEqualSubsIndData :: IsEq m n ->
  IndData m -> S m o -> IndData n -> S n o -> Bool
synEqualSubsIndData =
  compareSubsIndData synEqualSubs

type CompareSubs m n o = IsEq m n -> T m -> S m o -> T n -> S n o -> Bool

{- |
Generic “zipping” operation on IndData. First argument can be the syntactical
equality operation, or the definitional equality operation.
-}
compareSubsIndData ::
  (forall m' n' o'. CompareSubs m' n' o') ->
  IsEq m n -> IndData m -> S m o -> IndData n -> S n o -> Bool
compareSubsIndData cmpsubs isEq indd !imm indd' !imm'
  | IndData (IndSort l tl) cns <- indd
  , IndData (IndSort l' tl') cns' <- indd' =
    l == l' &&
    compareSubsIndSortTele cmpsubs isEq tl imm tl' imm' &&
    zipplySmallArray
      (\ cn cn' -> compareSubsCnSig cmpsubs isEq cn imm cn' imm')
      cns cns'

compareSubsIndSortTele ::
  (forall m' n' o'. CompareSubs m' n' o') ->
  IsEq m n -> IndSortTele m -> S m o -> IndSortTele n -> S n o -> Bool
compareSubsIndSortTele cmpsubs isEq tl !imm tl' !imm' = case tl of
  TeleEnd ~TeleEndEmpty -> case tl' of
    TeleEnd ~TeleEndEmpty -> True
    _ -> False
  TeleCons a tl_a _ -> case tl' of
    TeleCons a' tl_a' _ ->
      -- First check the length of the sorts, as it is cheaper.
      -- Most sorts are small, so this does not use too much stack.
      compareSubsIndSortTele cmpsubs (sucIsEq isEq)
        tl_a (mkImmPreVar imm)
        tl_a' (mkImmPreVar imm') &&
      cmpsubs isEq a imm a' imm'
    _ -> False

compareSubsCnSig ::
  (forall m' n' o'. CompareSubs m' n' o') ->
  IsEq m n -> CnSig m -> S m o -> CnSig n -> S n o -> Bool
compareSubsCnSig cmpsubs isEq cn !imm cn' !imm' = case cn of
  CnSigAnns as -> case cn' of
    CnSigAnns as' ->
      zipplySmallArray
        (\ a a' -> cmpsubs isEq a imm a' imm')
        as as'
    _ -> False
  CnSigNR a cn_a _ -> case cn' of
    CnSigNR a' cn_a' _ ->
      compareSubsCnSig cmpsubs (sucIsEq isEq)
        cn_a (mkImmPreVar imm)
        cn_a' (mkImmPreVar imm') &&
      cmpsubs isEq a imm a' imm'
    _ -> False
  CnSigR _ cin cn1 _ -> case cn' of
    CnSigR _ cin' cn1' _ ->
      compareSubsCnInn cmpsubs isEq cin imm cin' imm' &&
      compareSubsCnSig cmpsubs isEq cn1 imm cn1' imm'
      -- Caution to not mix @cn1@ and @cn@!
    _ -> False

compareSubsCnInn ::
  (forall m' n' o'. CompareSubs m' n' o') ->
  IsEq m n -> CnInn m -> S m o -> CnInn n -> S n o -> Bool
compareSubsCnInn cmpsubs isEq cin !imm cin' !imm' = case cin of
  TeleEnd (CnInnEnd as) -> case cin' of
    TeleEnd (CnInnEnd as') ->
      zipplySmallArray
        (\ a a' -> cmpsubs isEq a imm a' imm')
        as as'
    _ -> False
  TeleCons a cin_a _ -> case cin' of
    TeleCons a' cin_a' _ ->
      compareSubsCnInn cmpsubs (sucIsEq isEq)
        cin_a (mkImmPreVar imm)
        cin_a' (mkImmPreVar imm') &&
      cmpsubs isEq a imm a' imm'
    _ -> False

synEqualSubsRecData :: IsEq m n ->
  RecData m' m -> S m o -> RecData n' n -> S n o -> Bool
synEqualSubsRecData = compareSubsRecData synEqualSubs

compareSubsRecData ::
  (forall m'' n'' o'. CompareSubs m'' n'' o') ->
  IsEq m n -> RecData m' m -> S m o -> RecData n' n -> S n o -> Bool
compareSubsRecData cmpsubs isEq rd imm rd' imm'
  | RecData immr t _ rcs <- rd
  , RecData immr' t' _ rcs' <- rd'
  , let
      immC = mkImmCompose imm immr
      immC' = mkImmCompose imm' immr'
      isEq1 = ifIsEq isEq
        (immSynEqual immr immr' `orIsEq` immSynEqual immC immC')
        (immSynEqual immC immC') =
    -- The cases are relevant, the predicate is irrelevant, but I do
    -- not known if the inductive family is relevant.
    zipplySmallArray
      (\ rc rc' -> compareSubsRecCase cmpsubs isEq1 rc immC rc' immC')
      rcs rcs' &&
    cmpsubs isEq1 t immC t' immC'

compareSubsRecCase ::
  (forall m' n' o'. CompareSubs m' n' o') ->
  IsEq m n -> RecCase m -> S m o -> RecCase n -> S n o -> Bool
compareSubsRecCase cmpsubs isEq rc !imm rc' !imm' = case rc of
  RecCaseEnd a -> case rc' of
    RecCaseEnd a' -> cmpsubs isEq a imm a' imm'
    _ -> False
  RecCaseNR rc_a _ -> case rc' of
    RecCaseNR rc_a' _ ->
      compareSubsRecCase cmpsubs (sucIsEq isEq)
        rc_a (mkImmPreVar imm)
        rc_a' (mkImmPreVar imm')
    _ -> False
  RecCaseR rc_2 _ _ _ -> case rc' of
    RecCaseR rc_2' _ _ _ ->
      compareSubsRecCase cmpsubs (sucIsEq (sucIsEq isEq))
        rc_2 (mkImmPreVar (mkImmPreVar imm))
        rc_2' (mkImmPreVar (mkImmPreVar imm'))
    _ -> False

-- | Syntactical equality of substitutions.
immSynEqual :: forall m n o. S m o -> S n o -> IsEq m n
immSynEqual immM immN
  | ImmEsubs (immMd :: ImmEsubsData _ _ d _) <- immM
  , ImmEsubs (immNd :: ImmEsubsData _ _ e _) <- immN
  , ImmEsubsData R iszM KWord# KWord# (esM :: Esubs m' _) <- immMd
  , ImmEsubsData R iszN KWord# KWord# (esN :: Esubs n' _) <- immNd =
    -- First the easier: checking the sizes.
    case K.eqKnown @m' @n' of
      Nothing -> IsDiff
      Just Dict ->
        case iszM of
          IsUnk -> case iszN of
            IsUnk -> case K.eqKnown @(o - d) @(o - e) of
              Nothing -> IsDiff
              Just Dict ->
                withDict (K.complCompl @o @d) $
                withDict (K.complCompl @o @e) $
                case esubsSynEqual esM esN of
                  False -> IsDiff
                  True -> IsEq
            IsZero -> IsDiff
          IsZero -> case iszN of
            IsZero -> case esubsSynEqual esM esN of
              False -> IsDiff
              True -> IsEq
            IsUnk -> IsDiff

esubsSynEqual :: forall m n. Esubs m n -> Esubs m n -> Bool
esubsSynEqual es1 es2 = case es1 of
  EsNil -> case es2 of
    EsNil -> True
    _ -> False
  EsCons R KWord# s1' (es1' :: Esubs m' n') -> case es2 of
    EsNil -> False
    EsCons R KWord# s2' (es2' :: Esubs m'' n'') ->
      withDict (K.rightPlusInj @m' @m'' @1) $
      case K.leOrGtKnown @(n - n') @(n - n'') of
        Left Dict ->
          withDict (K.coleCompl @n @n' @n'') $
          withDict (K.complMinusCompl @n @n'' @n') $
          withDict (K.rightMinusLeftPlus @n'' @n') $
          withDict (K.minusKnown @(n - n'') @(n - n')) $
          let k = kWord# @(n' - n'') in
          synEqual s1' (termAdd# k s2') &&
            esubsSynEqual es1' (mkEsAdd# k es2')
        Right Dict ->
          withDict (K.leCoweakRightPlus @(n - n'') @1 @(n - n')) $
          withDict (K.coleCompl @n @n'' @n') $
          withDict (K.complMinusCompl @n @n' @n'') $
          withDict (K.rightMinusLeftPlus @n' @n'') $
          withDict (K.minusKnown @(n - n') @(n - n'')) $
          let k = kWord# @(n'' - n') in
          synEqual (termAdd# k s1') s2' &&
            esubsSynEqual (mkEsAdd# k es1') es2'

etaExpand :: forall m. T m -> T (m + 1)
etaExpand f = mkApp (termAdd1 f) (newestVar @m)

defEqUR :: T m -> T m -> Bool
defEqUR = defEqUR' R

data WTS m = forall n. WTS (T n) (S n m)

wts :: T m -> WTS m
wts a = withTopSubs a WTS

{-
I hope that sharing is not broken here. I do not want to memoize @defEqUR'@,
as I think it is not a good idea to memoize functions with more than one
argument.
-}

withReflBin :: m :~: n -> (p m -> q m -> r) -> p m -> q n -> r
withReflBin R op a b = op a b

withReflBinS :: m :~: n -> (t m1 -> s m1 m -> t n1 -> s n1 m -> r) ->
  t m1 -> s m1 m -> t n1 -> s n1 n -> r
withReflBinS R op a aa b bb = op a aa b bb

sucRefl :: m :~: n -> (m + 1) :~: (n + 1)
sucRefl R = R

-- | Determines definitional equality by computing WHNF. Argument @m :~: n@ is
-- for avoiding confusion, putting its match later.
defEqUR' :: m :~: n -> T m -> T n -> Bool
defEqUR' !ev (getWhnf -> aM) (getWhnf -> aN)
  | WTS aM' immM' <- wts aM, WTS aN' immN' <- wts aN =
    let
     res -- If both are Lam, compare the bodies; otherwise, eta-expand.
      | Lam _ e_cM' _ <- aM', Lam _ e_cN' _ <- aN'
      , let immMP = mkImmPreVar immM'
      , let immNP = mkImmPreVar immN'
      , let e_cM = mkSubs e_cM' immMP
      , let e_cN = mkSubs e_cN' immNP =
        withReflBin (sucRefl ev) defEq e_cM e_cN
      | Lam _ e_cM' _ <- aM'
      , let immMP = mkImmPreVar immM'
      , let e_cM = mkSubs e_cM' immMP =
        withReflBin (sucRefl ev) defEq e_cM (etaExpand aN)
      | Lam _ e_cN' _ <- aN'
      , let immNP = mkImmPreVar immN'
      , let e_cN = mkSubs e_cN' immNP =
        withReflBin (sucRefl ev) defEq (etaExpand aM) e_cN

      -- We can't have Subs of Refer and Univ,
      -- so here we can have @aM@ instead of @aM'@.
      | Refer R iM <- aM = case aN of
        Refer R iN -> eqKWord# iM iN
        _ -> False
      | Refer{} <- aM = False

      | Univ lM <- aM = case aN of
        Univ lN -> lM == lN
        _ -> False
      | Univ{} <- aN = False

      | Pi cM' e_cM' _ <- aM' = case aN' of
        Pi cN' e_cN' _ ->
          withReflBin ev defEq (mkSubs cM' immM') (mkSubs cN' immN') &&
          withReflBin (sucRefl ev) defEq
            (mkSubs e_cM' (mkImmPreVar immM'))
            (mkSubs e_cN' (mkImmPreVar immN'))
        _ -> False
      | Pi{} <- aN' = False

      | App fM' cM' _ _ <- aM' = case aN' of
        App fN' cN' _ _ ->
          withReflBin ev defEq (mkSubs fM' immM') (mkSubs fN' immN') &&
          withReflBin ev defEq (mkSubs cM' immM') (mkSubs cN' immN')
        _ -> False
      | App{} <- aN' = False

      | Ind indM' <- aM' = case aN' of
        Ind indN' ->
          withReflBinS ev defEqSubsIndData indM' immM' indN' immN'
        _ -> False
      | Ind{} <- aM' = False

      | Cn iM _ asM' _ <- aM' = case aN' of
        Cn iN _ asN' _ ->
          iM == iN &&
          zipplySmallArray
            (\ cM' cN' -> withReflBin ev defEq
              (mkSubs cM' immM') (mkSubs cN' immN'))
            asM' asN'
        PrimUnNat xN ->
          defEqUnCn xN iM (mapSmallArray' (flip mkSubs immM') asM')
        PrimBbNat xN ->
          defEqBbCn xN iM (mapSmallArray' (flip mkSubs immM') asM')
        _ -> False
      | PrimUnNat xM <- aM' = case aN' of
        Cn iN _ asN' _ ->
          defEqUnCn xM iN (mapSmallArray' (flip mkSubs immN') asN')
        PrimUnNat xN -> xM == xN
        _ -> False
      | PrimBbNat xM <- aM' = case aN' of
        Cn iN _ asN' _ ->
          defEqBbCn xM iN (mapSmallArray' (flip mkSubs immN') asN')
        PrimBbNat xN -> xM == xN
        _ -> False

      | Rec rdM' cM' _ _ <- aM' = case aN' of
        Rec rdN' cN' _ _ ->
          withReflBin ev defEq (mkSubs cM' immM') (mkSubs cN' immN') &&
          withReflBin ev defEqRecData
            (mkRecDataSubs rdM' immM')
            (mkRecDataSubs rdN' immN')
        _ -> False

      | otherwise = bug "defEqUR" "unexpected case"
    in res

-- | @defEqUnCn m i as@ is true when @mkPrimUnNat m@ is definitionally equal to
-- @mkCn i _ as@.
defEqUnCn :: Natural -> Int -> V (T m) -> Bool
defEqUnCn !m !i !as
  | m == 0 = i == 0 && length as == 0
  | otherwise = i == 1 && length as == 1 &&
    withTopSubs (getWhnf (indexSmallArray as 0)) (\ a' imm' ->
      case a' of
        Cn ib' _ bs' _ -> defEqUnCn (m - 1) ib'
          (mapSmallArray' (flip mkSubs imm') bs')
        PrimUnNat nb' -> m - 1 == nb'
        _ -> False)

-- | Like 'defEqUnCn', but for BbNat.
defEqBbCn :: Natural -> Int -> V (T m) -> Bool
defEqBbCn !m !i !as
  | m == 0 = i == 0 && length as == 0
  | testBit m 0, let mBy2 = shiftR m 1 =
    i == 1 && length as == 1 &&
    withTopSubs (getWhnf (indexSmallArray as 0)) (\ a' imm' ->
      case a' of
        Cn ib' _ bs' _ -> defEqBbCn mBy2 ib'
          (mapSmallArray' (flip mkSubs imm') bs')
        PrimBbNat nb' -> mBy2 == nb'
        _ -> False)
  | otherwise, let mBy2Min1 = shiftR m 1 - 1 =
    i == 2 && length as == 1 &&
    withTopSubs (getWhnf (indexSmallArray as 0)) (\ a' imm' ->
      case a' of
        Cn ib' _ bs' _ -> defEqBbCn mBy2Min1 ib'
          (mapSmallArray' (flip mkSubs imm') bs')
        PrimBbNat nb' -> mBy2Min1 == nb'
        _ -> False)

defEqSubs :: T m -> S m o -> T n -> S n o -> Bool
defEqSubs a imma b immb = defEq (mkSubs a imma) (mkSubs b immb)

defEqSubsIndData ::
  IndData m -> S m o -> IndData n -> S n o -> Bool
defEqSubsIndData = compareSubsIndData (\ _ -> defEqSubs) IsDiff

defEqRecData ::
  RecData m1 m -> RecData m1' m -> Bool
defEqRecData rd rd' = compareSubsRecData (\ _ -> defEqSubs) IsEq
  rd immId rd' immId

-- * Inverting some substitutions

{-|
We use @Elift@ operations in order to propagate information about which
variables are present in a term. Each member of @Elift m n@ represents a subset
of @{0, ..., n - 1}@ of exactly @m@ elements, or, equivalently, a strictly
increasing sequence of @m@ integers between @0@ and @n - 1@.


Rules:

  * no consecutive ElAdd;
  * no ElAdd 0;
  * no ElAdd of ElZero;
  * no ElPre1 of ElId;
-}
data Elift (m :: Nat) (n :: Nat) where
  ElZero :: Elift 0 m
  ElId :: Elift m m
  ElAdd# :: KWord# o -> !(Elift m n) -> Elift m (n + o)
  ElPre1 :: !(Elift m n) -> Elift (m + 1) (n + 1)

-- | Apply @ElAdd@, simplifying if possible.
mkElAdd# :: forall o m n. KWord# o -> Elift m n -> Elift m (n + o)
mkElAdd# KWord# el
  | Just Dict <- K.eqKnown @o @0 = el
mkElAdd# KWord# (ElAdd# (KWord# :: KWord# o') (el :: Elift _ n')) =
  withDict (K.plusAss @n' @o' @o) $
  withDict (K.plusKnown @o' @o) $
  ElAdd# (kWord# @(o' + o)) el
mkElAdd# _ ElZero = ElZero
mkElAdd# k el = ElAdd# k el

mkElAdd1 :: Elift m n -> Elift m (n + 1)
mkElAdd1 = mkElAdd# (kWord# @1)

elAdd1 :: Elift m (m + 1)
elAdd1 = mkElAdd1 ElId

elAdd# :: KWord# k -> Elift m (m + k)
elAdd# k = mkElAdd# k ElId

mkElPre1 :: Elift m n -> Elift (m + 1) (n + 1)
mkElPre1 ElId = ElId
mkElPre1 el = ElPre1 el

mkElPre# :: forall o m n. KWord# o -> Elift m n -> Elift (m + o) (n + o)
mkElPre# _ ElId = ElId
mkElPre# k el = go k
  where
  go :: forall p. KWord# p -> Elift (m + p) (n + p)
  go KWord# = case K.zeroOrPosKnown @p of
    Left Dict -> el
    Right Dict -> withDict (K.minusKnown @p @1) $
      withDict (K.plusAss @m @(p - 1) @1) $
      withDict (K.plusAss @n @(p - 1) @1) $
      withDict (K.rightMinusPlus @p @1) $
      ElPre1 (go (kWord# @(p - 1)))

immFromElift :: Elift m n -> ImmEsubs m n
immFromElift ElZero = immZero
immFromElift ElId = immId
immFromElift (ElAdd# k# el) = mkImmAdd# k# (immFromElift el)
immFromElift (ElPre1 el) = mkImmPreVar (immFromElift el)

mkSubsEl :: Term m -> Elift m n -> Term n
mkSubsEl t el = mkSubs t (immFromElift el)

data Eliftend s m = forall n. Eliftend !(Elift n m) !(s n)

eliftendToTerm :: Eliftend Term m -> Term m
eliftendToTerm (Eliftend el t) = mkSubsEl t el

-- | Avoid putting redundant suspension, because of StableName things.
eliftendToTerm0 :: Eliftend Term 0 -> Term 0
eliftendToTerm0 (Eliftend ElZero t) = t
eliftendToTerm0 (Eliftend ElId t) = t
eliftendToTerm0 _ = bug "eliftendToTerm0" "not ElZero, ElId"

data UnionElift a b c = forall c'.
  UnionElift !(Elift a c') !(Elift b c') !(Elift c' c)

unionEliftAdd# :: KWord# o -> UnionElift a b c -> UnionElift a b (c + o)
unionEliftAdd# k (UnionElift el1 el2 fac) =
  UnionElift el1 el2 (mkElAdd# k fac)

{-|
Given strictly increasing functions @f : a → c@ and @g : b → c@, calculate the
minimum strictly increasing function @h : c' → c@ for which @f@ and @g@ factor
by @h@; in @UnionElift@ we store functions @f' : a → c'@ and @g' : b → c'@ such
that @f = h . f'@ and @g = h . g'@.
-}
unionElift :: forall a b c. Elift a c -> Elift b c -> UnionElift a b c
unionElift el1 el2 = case el1 of
  -- The union is the last argument to @UnionElift@.
  ElZero -> UnionElift ElZero ElId el2
  ElId -> UnionElift ElId el2 ElId
  ElAdd# (k@KWord# :: KWord# k) (el1mk :: Elift a cmk) -> case el2 of
    ElZero -> UnionElift ElId ElZero el1
    ElId -> UnionElift el1 ElId ElId
    ElAdd# (i@KWord# :: KWord# i) (el2mi :: Elift b cmi) ->
      case K.leOrGtKnown @k @i of
        Left Dict -> withDict (K.minusKnown @i @k) $
          withDict (K.plusAss @cmi @(i - k) @k) $
          withDict (K.rightMinusPlus @i @k) $
          withDict (K.rightPlusInj @(cmi + (i - k)) @cmk @k) $
          unionEliftAdd# k $
          unionElift el1mk (mkElAdd# (kWord# @(i - k)) el2mi)
        Right Dict -> withDict (K.leCoweakRightPlus @i @1 @k) $
          withDict (K.minusKnown @k @i) $
          withDict (K.plusAss @cmk @(k - i) @i) $
          withDict (K.rightMinusPlus @k @i) $
          withDict (K.rightPlusInj @(cmk + (k - i)) @cmi @i) $
          unionEliftAdd# i $
          unionElift (mkElAdd# (kWord# @(k - i)) el1mk) el2mi
    ElPre1 (el2mp :: Elift bm1 cm1) ->
      case K.zeroOrPosKnown @k of
        Left Dict -> unionElift el1mk el2
        Right Dict ->
          withDict (K.minusKnown @k @1) $
          withDict (K.rightMinusPlus @k @1) $
          withDict (K.plusAss @cmk @(k - 1) @1) $
          withDict (K.rightPlusInj @(cmk + (k - 1)) @cm1 @1) $
          case unionElift (mkElAdd# (kWord# @(k - 1)) el1mk) el2mp of
            UnionElift el1m1Corr el2mpCorr (un :: Elift c' cm1) ->
              UnionElift (mkElAdd1 el1m1Corr :: Elift a (c' + 1))
                (mkElPre1 el2mpCorr :: Elift b (c' + 1))
                (mkElPre1 un :: Elift (c' + 1) c)
  ElPre1 (el1mp :: Elift am1 cm1) -> case el2 of
    ElZero -> UnionElift ElId ElZero el1
    ElId -> UnionElift el1 ElId ElId
    ElAdd# (KWord# :: KWord# k) (el2mk :: Elift b cmk) ->
      case K.zeroOrPosKnown @k of
        Left Dict -> unionElift el1 el2mk
        Right Dict ->
          withDict (K.minusKnown @k @1) $
          withDict (K.rightMinusPlus @k @1) $
          withDict (K.plusAss @cmk @(k - 1) @1) $
          withDict (K.rightPlusInj @(cmk + (k - 1)) @cm1 @1) $
          case unionElift el1mp (mkElAdd# (kWord# @(k - 1)) el2mk) of
            UnionElift el1mpCorr el2m1Corr (un :: Elift c' cm1) ->
              UnionElift (mkElPre1 el1mpCorr :: Elift a (c' + 1))
                (mkElAdd1 el2m1Corr :: Elift b (c' + 1))
                (mkElPre1 un :: Elift (c' + 1) c)
    ElPre1 (el2mp :: Elift b1 cm1_) ->
      withDict (K.rightPlusInj @cm1 @cm1_ @1) $
      case unionElift el1mp el2mp of
        UnionElift el1mpCorr el2mpCorr (un :: Elift c' cm1) ->
          UnionElift (mkElPre1 el1mpCorr :: Elift a (c' + 1))
            (mkElPre1 el2mpCorr :: Elift b (c' + 1))
            (mkElPre1 un :: Elift (c' + 1) c)

-- | Compose strictly increasing functions.
composeElift :: forall m n o. Elift n o -> Elift m n -> Elift m o
composeElift el1 el2 = case el2 of
  ElZero -> ElZero
  ElId -> el1
  ElAdd# (KWord# :: KWord# k) (el2mk :: Elift m nmk) -> case el1 of
    ElZero ->
      withDict (K.leftPlusExpans @k @nmk) $
      withDict (K.leZero @k) $
      composeElift ElZero el2mk
    ElId -> el2
    ElAdd# (i@KWord# :: KWord# i) (el1mi :: Elift n omi) ->
      mkElAdd# i (composeElift el1mi el2)
    ElPre1 (el1mp :: Elift nm1 _) -> case K.zeroOrPosKnown @k of
      Left Dict -> composeElift el1 el2mk
      Right Dict ->
        withDict (K.minusKnown @k @1) $
        withDict (K.rightMinusPlus @k @1) $
        withDict (K.plusAss @nmk @(k - 1) @1) $
        withDict (K.rightPlusInj @(nmk + (k - 1)) @nm1 @1) $
        mkElAdd1 (composeElift el1mp (mkElAdd# (kWord# @(k - 1)) el2mk))
  ElPre1 (el2mp :: Elift mm1 nm1) -> case el1 of
    ElZero -> case withDictEq (K.leftPlusExpans @1 @nm1) of {}
    ElId -> el2
    ElAdd# (k@KWord# :: KWord# k) (el1mk :: Elift n omk) ->
      mkElAdd# k (composeElift el1mk el2)
    ElPre1 (el1mp :: Elift nm1_ om1) ->
      withDict (K.rightPlusInj @nm1 @nm1_ @1) $
      mkElPre1 (composeElift el1mp el2mp)

data Cart s t m = Cart {cartFst :: s m, cartSnd :: t m}

commonEliftend :: Eliftend s m -> Eliftend t m ->
  Eliftend (Cart (Eliftend s) (Eliftend t)) m
commonEliftend (Eliftend el1 a1) (Eliftend el2 a2) =
  case unionElift el1 el2 of
    UnionElift el1Corr el2Corr un ->
      Eliftend un (Cart (Eliftend el1Corr a1) (Eliftend el2Corr a2))

collapseEliftend :: Eliftend (Eliftend s) m -> Eliftend s m
collapseEliftend (Eliftend el1 (Eliftend el2 a)) =
  Eliftend (composeElift el1 el2) a

idEliftend :: s m -> Eliftend s m
idEliftend a = Eliftend ElId a

zeroEliftend :: s 0 -> Eliftend s m
zeroEliftend a = Eliftend ElZero a

mapEliftend :: (forall n. s n -> t n) -> Eliftend s m -> Eliftend t m
mapEliftend f (Eliftend el a) = Eliftend el (f a)

newtype ListF s m = ListF{unListF :: [s m]}
newtype SmallArrayF s m = SmallArrayF{unSmallArrayF :: V (s m)}

newtype ContEliftend r m = ContEliftend
  {unContEliftend :: forall n. Elift m n -> r n}

idContEliftend :: ContEliftend r m -> r m
idContEliftend (ContEliftend f) = f ElId

type EliftAct s s' = (forall n m. Elift n m -> s n -> s' m)

commonContEliftendListWith :: EliftAct s s' ->
  [Eliftend s m] ->
  Eliftend (ContEliftend (ListF s')) m
commonContEliftendListWith act = go
    where
  -- This is a bit hard to understand; summarizing, as composition is
  -- associative, we can group it as we like.
  -- Try searching “difference lists”.
  --
  -- This definition is O(n) in the number of compositions because, for
  -- each iteration, the continuation @hAs@ is called only once and
  -- only two more applications of @composeElift@ are done.
  go = foldr op end
  op elnA elnR = case commonEliftend elnA elnR of
    Eliftend elUn (Cart (Eliftend elAF xA) (Eliftend elRF hR)) ->
      Eliftend elUn $ ContEliftend $ \ elCont ->
        case unContEliftend hR $! composeElift elCont elRF of
          ListF hRApp ->
            ListF (act (composeElift elCont elAF) xA : hRApp)
  end = zeroEliftend $ ContEliftend $ \ _ -> ListF []

commonContEliftendSmallArrayWith :: EliftAct s s' ->
  V (Eliftend s m) -> Eliftend (ContEliftend (SmallArrayF s')) m
commonContEliftendSmallArrayWith act as
  | let !n = length as = case commonContEliftendListWith act (toList as) of
    Eliftend el h -> Eliftend el $ ContEliftend $ \ elCont ->
      SmallArrayF $ smallArrayFromListN n $ unListF $
        unContEliftend h elCont

newtype FSuc s m = FSuc (s (m + 1))

{-
Remember that @(λ A, e)[σ] == λ A[σ], e[mkImmPre σ]@.

This function tries to write @λ Const, e[τ]@ as @(λ Const, e[τ1])[τ2]@, where
@τ1@ as simple as possible. Equivalently, @τ@ is to be @mkImmPre τ2 ∘ τ1@.
-}
bindEliftend :: forall s m. Eliftend s (m + 1) ->
  Eliftend (FSuc (Eliftend s)) m
bindEliftend (Eliftend (el :: Elift n _) a) = case el of
  ElZero -> zeroEliftend (FSuc (zeroEliftend a))
  ElId -> idEliftend (FSuc (idEliftend a))
  ElAdd# (KWord# :: KWord# k) (elmk :: Elift n m1mk) ->
    case K.zeroOrPosKnown @k of
      Left Dict -> bindEliftend (Eliftend elmk a)
      Right Dict ->
        withDict (K.minusKnown @k @1) $
        withDict (K.rightMinusPlus @k @1) $
        withDict (K.plusAss @m1mk @(k - 1) @1) $
        withDict (K.rightPlusInj @(m1mk + (k - 1)) @m @1) $
        Eliftend (mkElAdd# (kWord# @(k - 1)) elmk)
          (FSuc (Eliftend elAdd1 a))
  ElPre1 (elmp :: Elift nm1 m_) ->
    withDict (K.rightPlusInj @m @m_ @1) $
    Eliftend elmp (FSuc (idEliftend a))

commonBindEliftend :: forall s t m. Eliftend s m -> Eliftend t (m + 1) ->
  Eliftend (Cart (Eliftend s) (FSuc (Eliftend t))) m
commonBindEliftend elnda elnde =
  let
    elndae :: Eliftend (Cart (Eliftend s) (Eliftend (FSuc (Eliftend t)))) m
    elndae = commonEliftend elnda (bindEliftend elnde)
    f :: Cart (Eliftend s) (Eliftend (FSuc (Eliftend t))) n ->
      Cart (Eliftend s) (FSuc (Eliftend t)) n
    f (Cart a b) = Cart a (g b)
    g :: Eliftend (FSuc (Eliftend t)) n -> FSuc (Eliftend t) n
    g (Eliftend el1 (FSuc (Eliftend el2 c))) =
      FSuc (Eliftend (composeElift (mkElPre1 el1) el2) c)
  in mapEliftend f elndae

commonContEliftendGTeleWith :: forall a a' e e'. forall m.
  EliftAct a a' -> EliftAct e e' ->
  GTele (Eliftend a) (Eliftend e) m -> Eliftend (ContEliftend (GTele a' e')) m
commonContEliftendGTeleWith acta acte = go
  where
  go :: forall m. GTele (Eliftend a) (Eliftend e) m ->
    Eliftend (ContEliftend (GTele a' e')) m
  go (TeleEnd (Eliftend el e)) = Eliftend el $ ContEliftend $ \ elCont ->
    TeleEnd (acte elCont e)
  go (TeleCons elnA gtele rbn) = case commonBindEliftend elnA (go gtele) of
    Eliftend elUn (Cart (Eliftend elA xA) (FSuc (Eliftend elG hG))) ->
      Eliftend elUn $ ContEliftend $ \ elCont ->
        case unContEliftend hG $! composeElift (mkElPre1 elCont) elG of
          hGApp -> TeleCons (acta (composeElift elCont elA) xA) hGApp rbn

commonContEliftendGRecPredTeleWith :: forall a a'. forall m.
  EliftAct a a' ->
  GRecPredTele (Eliftend a) m -> Eliftend (ContEliftend (GRecPredTele a')) m
commonContEliftendGRecPredTeleWith acta = go
  where
  go :: forall m. GRecPredTele (Eliftend a) m ->
    Eliftend (ContEliftend (GRecPredTele a')) m
  go (TeleEnd (RecPredTeleEnd elne rbn)) =
    case bindEliftend elne of
      Eliftend elF (FSuc (Eliftend el e)) -> Eliftend elF $ ContEliftend $
        \ elCont -> TeleEnd $
          RecPredTeleEnd (acta (composeElift (mkElPre1 elCont) el) e) rbn
  go (TeleCons elnA gtele rbn) = case commonBindEliftend elnA (go gtele) of
    Eliftend elUn (Cart (Eliftend elA xA) (FSuc (Eliftend elG hG))) ->
      Eliftend elUn $ ContEliftend $ \ elCont ->
        case unContEliftend hG $! composeElift (mkElPre1 elCont) elG of
          hGApp -> TeleCons (acta (composeElift elCont elA) xA) hGApp rbn

commonContEliftendGCnSigWoBuiltWith :: forall a a'. forall m. EliftAct a a' ->
  GCnSig (Eliftend a) m -> Eliftend (ContEliftend (GCnSig a')) m
commonContEliftendGCnSigWoBuiltWith act = go
  where
  go :: forall m. GCnSig (Eliftend a) m ->
    Eliftend (ContEliftend (GCnSig a')) m
  notBuilt = bug "commonContEliftendGCnSigWoBuiltWith" "not built"
  go (CnSigAnns as) = case commonContEliftendSmallArrayWith act as of
    Eliftend elAs xAs -> Eliftend elAs $ ContEliftend $ \ elCont ->
      CnSigAnns $ unSmallArrayF $ unContEliftend xAs elCont
  go (CnSigNR a gCs rbn) = case commonBindEliftend a (go gCs) of
    Eliftend elUn (Cart (Eliftend elA xA) (FSuc (Eliftend elG hG))) ->
      Eliftend elUn $ ContEliftend $ \ elCont ->
        case unContEliftend hG $! composeElift (mkElPre1 elCont) elG of
          hGApp -> CnSigNR (act (composeElift elCont elA) xA) hGApp rbn
  go (CnSigR _ gCi gCs rbn) =
    case commonEliftend (commonContEliftendGCnInnWith act gCi) (go gCs) of
      Eliftend elUn (Cart (Eliftend elI hI) (Eliftend elS hS)) ->
        Eliftend elUn $ ContEliftend $ \ elCont ->
          case unContEliftend hS $! composeElift elCont elS of
            hSApp -> case unContEliftend hI $! composeElift elCont elI of
              hIApp -> CnSigR notBuilt hIApp hSApp rbn

commonContEliftendGCnInnWith :: forall a a'. forall m. EliftAct a a' ->
  GCnInn (Eliftend a) m -> Eliftend (ContEliftend (GCnInn a')) m
commonContEliftendGCnInnWith act = go
  where
  go :: forall m. GCnInn (Eliftend a) m ->
    Eliftend (ContEliftend (GCnInn a')) m
  go (TeleEnd (CnInnEnd as)) = case commonContEliftendSmallArrayWith act as of
    Eliftend elAs xAs -> Eliftend elAs $ ContEliftend $ \ elCont ->
      TeleEnd $ CnInnEnd $ unSmallArrayF $ unContEliftend xAs elCont
  go (TeleCons a g rbn) = case commonBindEliftend a (go g) of
    Eliftend elUn (Cart (Eliftend elA xA) (FSuc (Eliftend elG hG))) ->
      Eliftend elUn $ ContEliftend $ \ elCont ->
        case unContEliftend hG $! composeElift (mkElPre1 elCont) elG of
          hGApp -> TeleCons (act (composeElift elCont elA) xA) hGApp rbn

bind2Eliftend :: forall s m. Eliftend s (m + 1 + 1) ->
  Eliftend (FSuc (FSuc (Eliftend s))) m
bind2Eliftend a11 =
  let
    a11i :: Eliftend (FSuc (Eliftend s)) (m + 1)
    a11i = bindEliftend a11
    a11ii :: Eliftend (FSuc (Eliftend (FSuc (Eliftend s)))) m
    a11ii = bindEliftend a11i
    f :: forall n. FSuc (Eliftend (FSuc (Eliftend s))) n ->
      FSuc (FSuc (Eliftend s)) n
    f (FSuc a) = FSuc (FSuc (g @n a))
    g :: forall n. Eliftend (FSuc (Eliftend s)) (n + 1) ->
      Eliftend s (n + 1 + 1)
    g (Eliftend el (FSuc (Eliftend el1 x))) =
      Eliftend (composeElift (mkElPre1 el) el1) x
  in mapEliftend f a11ii

commonContEliftendRecCaseWoBuiltWith :: forall a a'. forall m. EliftAct a a' ->
  GRecCase (Eliftend a) m -> Eliftend (ContEliftend (GRecCase a')) m
commonContEliftendRecCaseWoBuiltWith act = go
  where
  notBuilt = bug "commonContEliftendRecCaseWoBuiltWith" "not built"
  go :: forall m. GRecCase (Eliftend a) m ->
    Eliftend (ContEliftend (GRecCase a')) m
  go (RecCaseEnd (Eliftend el a)) = Eliftend el $ ContEliftend $ \ elCont ->
    RecCaseEnd $ act elCont a
  go (RecCaseNR rc rbn) = case bindEliftend (go rc) of
    Eliftend elUn (FSuc (Eliftend elH hG)) ->
      Eliftend elUn $ ContEliftend $ \ elCont ->
        case unContEliftend hG $! composeElift (mkElPre1 elCont) elH of
          hGApp -> RecCaseNR hGApp rbn
  go (RecCaseR rc rbn1 rbn2 _) = case bind2Eliftend (go rc) of
    Eliftend elUn (FSuc (FSuc (Eliftend elH hG))) ->
      Eliftend elUn $ ContEliftend $ \ elCont ->
        case unContEliftend hG $!
          composeElift (mkElPre1 $ mkElPre1 elCont) elH
        of
          hGApp -> RecCaseR hGApp rbn1 rbn2 notBuilt

{- -- Possibly useful things:
optimizeEliftend :: Eliftend Term m -> Eliftend Term m
optimizeEliftend = collapseEliftend . mapEliftend unTermEliftend

unTermEliftend :: forall m. Term m -> Eliftend Term m
unTermEliftend (Refer R (KWord# :: KWord# i)) =
  withDict (K.rightPlusComm @(m - (i + 1)) @1 @i) $
  withDict (K.plusAss @(m - (i + 1)) @i @1) $
  withDict (K.rightMinusPlus @m @(i + 1)) $
  withDict (K.plusKnown @i @1) $
  Eliftend (elAdd# (kWord# @i)) $ newestVar @(m - (i + 1))
unTermEliftend (Univ l) = zeroEliftend $ Univ l
unTermEliftend (PrimUnNat x) = zeroEliftend $ PrimUnNat x
unTermEliftend (PrimBbNat x) = zeroEliftend $ PrimBbNat x
unTermEliftend (Subs t imm _)
  | Just Dict <- isImmAdd imm = Eliftend (onlyAddToElift OnlyAdd) t
  | Just Dict <- isImmZero imm = zeroEliftend t
-- XXX. Stop-gap.
unTermEliftend t = idEliftend t

data UnEliftImmAndQuasiInverse m n =
  forall o. UnEliftImmAndQuasiInverse !(Elift o n)
    !(ImmEsubs m o) !(ImmEsubs n o)
-}

-- * Only additions

data OnlyAdd m n = OnlyAddW {-# UNPACK #-} !((m <=? n) :~: 'True)
  (KWord# (n - m))

pattern OnlyAdd :: () => (m <= n, KnownWord (n - m)) => OnlyAdd m n
pattern OnlyAdd = OnlyAddW R KWord#
{-# COMPLETE OnlyAdd #-}

onlyAddAdd1 :: forall m n. OnlyAdd m n -> OnlyAdd m (n + 1)
onlyAddAdd1 OnlyAdd = withDict (K.leWeakRightPlus @m @n @1) $
  withDict (K.rightMinusPlusComm @n @m @1) $
  withDict (K.plusKnown @(n - m) @1) $ OnlyAdd

onlyAddId :: forall m. OnlyAdd m m
onlyAddId = withDict (K.leRefl @m) $ withDict (K.minusRefl @m) $ OnlyAdd

mkSubsOnlyAdd :: forall m n. T m -> OnlyAdd m n -> T n
mkSubsOnlyAdd t OnlyAdd = withDict (K.rightMinusLeftPlus @m @n) $
  termAdd# (kWord# @(n - m)) t

onlyAddCoadd1 :: forall m n. OnlyAdd (m + 1) n -> OnlyAdd m n
onlyAddCoadd1 OnlyAdd = withDict (K.leCoweakRightPlus @m @1 @n) $
  withDict (K.rightPlusComplRightPlus @n @m @1) $
  withDict (K.plusKnown @(n - (m + 1)) @1) $
  withDict (K.rightPlusMinusKnown @(n - (m + 1)) @1) $
  OnlyAdd

onlyAddToElift :: forall m n. OnlyAdd m n -> Elift m n
onlyAddToElift OnlyAdd = withDict (K.rightMinusLeftPlus @m @n) $
  elAdd# (kWord# @(n - m))

onlyAddToImm :: forall m n. OnlyAdd m n -> ImmEsubs m n
onlyAddToImm oa = immFromElift (onlyAddToElift oa)

-- * Contexts

-- | Context of values with decreasing indices.
data Context (s :: Nat -> Type) (m :: Nat) where
  ContextNil :: Context s 0
  ContextCons :: !(s m) -> !(Context s m) -> Context s (m + 1)

-- | Context lookup.
atContext# :: forall i m s. (i + 1 <= m) => KWord# i ->
  Context s m -> s (m - (i + 1))
atContext# KWord# ctx =
  case ctx of
    ContextNil -> case withDictEq (K.leCoweakLeftPlus @i @1 @m) of {}
    ContextCons (s' :: s m') ctx' ->
      withDict (K.rightPlusMinus @m' @1) $
      case K.zeroOrPosKnown @i of
        Left Dict -> s'
        Right Dict ->
          withDict (K.minusKnown @i @1) $
          withDict (K.rightMinusPlus @i @1) $
          withDict (K.rightPlusLeRightMinus @i @1 @m) $
          withDict (K.plusComm @i @1) $
          withDict (K.complPlus @m @1 @i) $
          atContext# @(i - 1) KWord# ctx'

-- | Cut some elements of a context.
dropContext# :: forall m n s. (n <= m) => KWord# (m - n) ->
  Context s m -> Context s n
dropContext# KWord# ctx = case K.zeroOrPosKnown @(m - n) of
  Left Dict ->
    withDict (K.rightMinusPlus @m @n) $ ctx
  Right Dict -> withDict (K.complLe @m @n) $ case ctx of
    ContextNil -> case withDictEq (K.leTrans @1 @(m - n) @m) of {}
    ContextCons _ (ctx' :: Context s m') ->
      withDict (K.leMinusSwap @1 @m @n) $
      withDict (K.rightPlusMinus @m' @1) $
      withDict (K.rightMinusPlusComm @(m - 1) @n @1) $
      withDict (K.rightPlusMinusKnown @(m - 1 - n) @1) $
      dropContext# KWord# ctx'

-- | Get the tail end of context.
atContext0 :: forall m s. s m -> Context s m -> s 0
atContext0 a ContextNil = a
atContext0 _ (ContextCons a' ctx') = atContext0 a' ctx'

-- | Access context at index, or return @Nothing@ in case the index is too big.
atContextAndKnown# :: forall i m s. KWord# i -> Context s m ->
  Maybe (s (m - (i + 1)), Dict (i + 1 <= m))
atContextAndKnown# KWord# = withDict (K.rightPlusExpans @0 @i) $
  go (kWord# @0)
  where
  go :: forall k m'.
    (k <= i, (m' + k) ~ m) => KWord# k -> Context s m' ->
      Maybe (s (m - (i + 1)), Dict (i + 1 <= m))
  go _ ContextNil = Nothing
  go KWord# (ContextCons a'' (ctx'' :: _ _ m'')) =
    withDict (K.plusAss @m'' @1 @k) $
    withDict (K.plusKnown @1 @k) $
    case K.ltOrGeKnown @k @i of
      Right Dict ->
        withDict (K.leAsym @i @k) $
        withDict (K.plusComm @i @1) $
        withDict (K.leftPlusExpans @(i + 1) @m'') $
        withDict (K.rightPlusMinus @m'' @(i + 1)) $
        Just (a'', Dict)
      Left Dict ->
        withDict (K.plusComm @k @1) $
        go (kWord# @(1 + k)) ctx''

-- * Parsed syntax

-- | Source locations, used only for user error messages.
newtype SL = SL Int deriving (Eq, Show)

-- | Parsed term. Differences between 'PTerm' and 'Term'.
--
-- * 'Term' has type level argument, so that all its variables are in scope,
--   whereas variables in 'PTerm' may be wrong.
-- * 'Term' has WHNF fields for memoized reduction, whereas 'PTerm' is only
--   parsed syntax, not for reduction.
-- * 'Term' has 'ImmEsubs' explicit substitutions, for short-circuits in
--   reduction.
-- * 'Term' has implicit sharing and cyclical structures, wheareas 'PTerm'
--   has explicit sharing, with references to local and global definitions.
-- * 'PTerm' has source location fields (which refer to the start of the
--   corresponding written syntax structure).
data PTerm =
  PRefer {-# UNPACK #-} !SL {-# UNPACK #-} !Word
  | PUniv {-# UNPACK #-} !SL !Level
  | PPi {-# UNPACK #-} !SL !PTerm !PTerm {-# UNPACK #-} !RBN
  | PLam {-# UNPACK #-} !SL !PTerm !PTerm {-# UNPACK #-} !RBN
  | PApp {-# UNPACK #-} !SL !PTerm !PTerm {-# UNPACK #-} !RBN
  | PInd {-# UNPACK #-} !SL !Level !PIndSortTele [PCnSig]
  | PCn {-# UNPACK #-} !SL {-# UNPACK #-} !Int !PTerm [PTerm]
    {-# UNPACK #-} !RBN
  | PRec {-# UNPACK #-} !SL !PTerm !PRecPred [PRecCase] !PTerm
    {-# UNPACK #-} !RBN
  | PPrimUnNat {-# UNPACK #-} !SL !Natural
  | PPrimBbNat {-# UNPACK #-} !SL !Natural
  -- Not present in 'Term':
  | PGlobal {-# UNPACK #-} !SL {-# UNPACK #-} !Hash [(Int, Integer)]
  | PLet {-# UNPACK #-} !SL {-# UNPACK #-} !Int !PTerm !PTerm
  | PLocalDef {-# UNPACK #-} !SL {-# UNPACK #-} !Int

data PIndSortTele =
  PIndSortTeleCons {-# UNPACK #-} !SL !PTerm !PIndSortTele {-# UNPACK #-} !RBN
  | PIndSortTeleEnd {-# UNPACK #-} !SL

data PCnSig =
  PCnSigEnd {-# UNPACK #-} !SL [PTerm]
  | PCnSigNR {-# UNPACK #-} !SL !PTerm !PCnSig {-# UNPACK #-} !RBN
  | PCnSigR {-# UNPACK #-} !SL !PCnInn !PCnSig {-# UNPACK #-} !RBN

data PCnInn =
  PCnInnEnd {-# UNPACK #-} !SL [PTerm]
  | PCnInnPi {-# UNPACK #-} !SL !PTerm !PCnInn {-# UNPACK #-} !RBN

data PRecPred = PRecPred {-# UNPACK #-} !SL {-# UNPACK #-} !Int !PTerm [RBN]

data PRecCaseNum = PRecCase1 | PRecCase2
data PRecCase = PRecCaseCons {-# UNPACK #-} !SL
  !PRecCaseNum !PRecCase {-# UNPACK #-} !RBN {-# UNPACK #-} !RBN
  | PRecCaseEnd {-# UNPACK #-} !SL !PTerm

data Hash = Hash {-# UNPACK #-} !Word32 {-# UNPACK #-} !Word64
  deriving (Eq, Ord)

-- It would be better to have Word32 operations in the bytestring package.
-- Big-endian order of bytes.

hashToByteString :: Hash -> B.ByteString
hashToByteString (Hash a bc) =
  B.pack
    [ byteAt a  24, byteAt a  16, byteAt a   8, byteAt a  0
    , byteAt bc 56, byteAt bc 48, byteAt bc 40, byteAt bc 32
    , byteAt bc 24, byteAt bc 16, byteAt bc  8, byteAt bc  0 ]
  where
  byteAt :: (Bits a, Integral a) => a -> Int -> Word8
  byteAt v i = fromIntegral (shiftR v i)

hashToBase64 :: Hash -> B.ByteString
hashToBase64 = B64.encode . hashToByteString

byteStringToHash :: B.ByteString -> Hash
byteStringToHash bs =
  Hash (bytesTo32 (i 0) (i 1) (i 2) (i 3))
    (bytesTo64 (i 4) (i 5) (i 6) (i 7) (i 8) (i 9) (i 10) (i 11))
  where
  i = B.index bs
  bytesTo32 a b c d =
    shiftL (fromIntegral a) 24 +
    shiftL (fromIntegral b) 16 +
    shiftL (fromIntegral c) 8 +
    fromIntegral d
  bytesTo64 a b c d e f g h =
    shiftL (fromIntegral a) 56 +
    shiftL (fromIntegral b) 48 +
    shiftL (fromIntegral c) 40 +
    shiftL (fromIntegral d) 32 +
    shiftL (fromIntegral e) 24 +
    shiftL (fromIntegral f) 16 +
    shiftL (fromIntegral g) 8 +
    fromIntegral h

base64ToHash :: B.ByteString -> Hash
base64ToHash = byteStringToHash . B64.decodeLenient

instance Show Hash where
  showsPrec d a = showParen (d > 10) $
    (("base64ToHash " ++ show (hashToBase64 a)) ++)

getSL :: PTerm -> SL
getSL (PRefer sl _) = sl
getSL (PUniv sl _) = sl
getSL (PPi sl _ _ _) = sl
getSL (PLam sl _ _ _) = sl
getSL (PApp sl _ _ _) = sl
getSL (PInd sl _ _ _) = sl
getSL (PCn sl _ _ _ _) = sl
getSL (PRec sl _ _ _ _ _) = sl
getSL (PPrimUnNat sl _) = sl
getSL (PPrimBbNat sl _) = sl
getSL (PGlobal sl _ _) = sl
getSL (PLet sl _ _ _) = sl
getSL (PLocalDef sl _) = sl

-- * Type checking monad

-- | Reader-except monad on these things which change during verification:
-- context of types, and local definitions.
newtype CheckM m r = CheckM {unCheckM :: Context (Eliftend Term) m ->
  LocalDefMaps m -> Either CheckExcept r}
  deriving Functor

instance Applicative (CheckM m) where
  pure r = CheckM (\ _ _ -> Right r)
  CheckM h <*> CheckM a = CheckM (\ x y -> h x y <*> a x y)

instance Monad (CheckM m) where
  CheckM a >>= act = CheckM (\ x y -> a x y >>=
    \ r -> unCheckM (act r) x y)

askCtx :: CheckM m (Context (Eliftend Term) m)
askCtx = CheckM (\ x _ -> Right x)

newtype LocalDefMaps m = LocalDefMaps (Context LocalDefMap (m + 1))

newtype LocalDefMap m = LocalDefMap {unLocalDefMap ::
  I.IntMap (Typed (Eliftend Term m))}

data Typed a = Typed{tTerm :: !a, tType :: !a}

initLocalDefMaps :: LocalDefMaps 0
initLocalDefMaps =
  LocalDefMaps (ContextCons (LocalDefMap I.empty) ContextNil)

sucLocalDefMaps :: LocalDefMaps m -> LocalDefMaps (m + 1)
sucLocalDefMaps (LocalDefMaps ctx) =
  LocalDefMaps (ContextCons (LocalDefMap I.empty) ctx)

unconsLocalDefMaps :: forall m. LocalDefMaps m ->
  (LocalDefMap m, Context LocalDefMap m)
unconsLocalDefMaps (LocalDefMaps ctx) = case ctx of
  ContextNil -> case withDictEq (K.leftPlusExpans @1 @m) of {}
  ContextCons (a :: _ m_) rest ->
    withDict (K.rightPlusInj @m @m_ @1) $ (a, rest)

addToLocalDefMaps :: Int -> Typed (Eliftend Term m) -> LocalDefMaps m ->
  LocalDefMaps m
addToLocalDefMaps idx typd (unconsLocalDefMaps -> (lds, rest)) =
  LocalDefMaps (ContextCons (LocalDefMap
    (I.insert idx typd (unLocalDefMap lds))) rest)

indexLocalDefMaps :: forall m. Int -> LocalDefMaps m ->
  Maybe (Typed (Eliftend Term m))
indexLocalDefMaps idx = go onlyAddId
  where
  go :: forall n. OnlyAdd n m -> LocalDefMaps n ->
    Maybe (Typed (Eliftend Term m))
  go oadd (unconsLocalDefMaps -> (lds, rest)) =
    case I.lookup idx (unLocalDefMap lds) of
      Just Typed{tTerm = tt, tType = ty} ->
        let f x = collapseEliftend (Eliftend (onlyAddToElift oadd) x) in
        Just $ Typed
        { tTerm = f tt, tType = f ty }
      Nothing -> case rest of
        ContextNil -> Nothing
        ContextCons (lds' :: _ n') rest' -> go @n' (onlyAddCoadd1 oadd)
          (LocalDefMaps (ContextCons lds' rest'))

withAddToLocalDefMaps :: Int -> Typed (Eliftend Term m) ->
  CheckM m r -> CheckM m r
withAddToLocalDefMaps idx typd (CheckM f) = CheckM (\ x y ->
  f x $! addToLocalDefMaps idx typd y)

withIndexLocalDefMaps :: Int -> CheckM m (Maybe (Typed (Eliftend Term m)))
withIndexLocalDefMaps idx = CheckM (\ _ y -> Right (indexLocalDefMaps idx y))

localDecl :: Eliftend Term m -> CheckM (m + 1) r -> CheckM m r
localDecl decl (CheckM f) = CheckM $ \ x y ->
  (f $! ContextCons decl x) $! sucLocalDefMaps y

checkThrow :: CheckExcept -> CheckM m r
checkThrow exc = CheckM (\ _ _ -> Left exc)

-- * Subsumed types

subsumedByUniv :: [ZLe ZVal] -> Term m -> Level -> Bool
subsumedByUniv rs (levelOfUniverse -> Just l1) l2
  | impliesN rs (l1, l2) = True
subsumedByUniv _ _ _ = False

subsumedBy :: [ZLe ZVal] -> Term m -> Term m -> Bool
subsumedBy rs t1 t2
  | defEq t1 t2 = True
  | Just l2 <- levelOfUniverse t2 = subsumedByUniv rs t1 l2
  | Just (t1d, t1c) <- unPi t1, Just (t2d, t2c) <- unPi t2 =
    subsumedBy rs t2d t1d && subsumedBy rs t1c t2c
    -- Attention to order! On domain, contravariant.
  | otherwise = False

-- * Manipulation of @Eliftend Term@

{-
Eliftend does not change computational properties,
unlike general substitutions.
-}

elLevelOfUniverse :: Eliftend Term m -> Maybe Level
elLevelOfUniverse (Eliftend _ t) = levelOfUniverse t

elIndDataOfInductive :: Eliftend Term m -> Maybe (Eliftend SubsIndData m)
elIndDataOfInductive (Eliftend el t) = case indDataOfInductive t of
  Nothing -> Nothing
  Just sindd -> Just (Eliftend el sindd)

elDoOp00 :: (forall n. Term n -> Term n -> Term n) ->
  Eliftend Term m -> Eliftend Term m -> Eliftend Term m
elDoOp00 op elna elnb =
  mapEliftend
    (\ (Cart elnaF elnbF) ->
      op (eliftendToTerm elnaF) (eliftendToTerm elnbF))
    (commonEliftend elna elnb)

elDoOp01 :: (forall n. Term n -> Term (n + 1) -> Term n) ->
  Eliftend Term m -> Eliftend Term (m + 1) -> Eliftend Term m
elDoOp01 op eln0 eln1 =
  mapEliftend
    (\ (Cart eln0F (FSuc eln1F)) ->
      op (eliftendToTerm eln0F) (eliftendToTerm eln1F))
    (commonBindEliftend eln0 eln1)

elMkPiRBN :: RBN -> Eliftend Term m -> Eliftend Term (m + 1) -> Eliftend Term m
elMkPiRBN rbn = elDoOp01 (mkPiRBN rbn)

elMkLamRBN :: RBN -> Eliftend Term m ->
  Eliftend Term (m + 1) -> Eliftend Term m
elMkLamRBN rbn = elDoOp01 (mkLamRBN rbn)

elMkAppRBN :: RBN -> Eliftend Term m -> Eliftend Term m ->
  Eliftend Term m
elMkAppRBN rbn = elDoOp00 (mkAppRBN rbn)

elMkSubstitute :: Eliftend Term m -> Eliftend Term (m + 1) ->
  Eliftend Term m
elMkSubstitute = elDoOp01 mkSubstitute

newtype FromTermN s m = FromTermN {unFromTermN :: Term m -> s m}

elContMkIndAndBuild :: forall m.
  Level -> Eliftend (ContEliftend IndSortTele) m ->
  [Eliftend (ContEliftend CnSig) m] -> Eliftend Term m
elContMkIndAndBuild l elcont_tl elconts_cn =
  let
    op_magic_cn :: Elift n m' -> ContEliftend CnSig n -> FromTermN CnSig m'
    op_magic_cn el cont = FromTermN (\ res_ti ->
      buildCnSig res_ti onlyAddId (unContEliftend cont el))

    magic_cn :: Eliftend (ContEliftend (ListF (FromTermN CnSig))) m
    magic_cn = commonContEliftendListWith op_magic_cn elconts_cn

    op_final :: Cart (Eliftend (ContEliftend IndSortTele))
      (Eliftend (ContEliftend (ListF (FromTermN CnSig)))) n -> Term n
    op_final (Cart el_elcont_tl el_elcont_ftns_cn)
      | Eliftend elS xS <- el_elcont_tl
      , Eliftend elC xC <- el_elcont_ftns_cn =
        let
          res_ti = Ind $ IndData (IndSort l (unContEliftend xS elS)) $
            smallArrayFromList $
              map (`unFromTermN` res_ti)
              (unListF $ unContEliftend xC elC)
        in res_ti
  in mapEliftend op_final (commonEliftend elcont_tl magic_cn)

buildCnSig :: Term n -> OnlyAdd n m -> CnSig m -> CnSig m
buildCnSig ti !oa cn_pre = case cn_pre of
  CnSigAnns args -> CnSigAnns args
  CnSigNR a cn_pre_a rbn ->
    CnSigNR a (buildCnSig ti (onlyAddAdd1 oa) cn_pre_a) rbn
  CnSigR _ ci cn_pre_1 rbn ->
    (CnSigR $! Built (buildCnInn ti oa ci)) ci (buildCnSig ti oa cn_pre_1) rbn

buildCnInn :: Term n -> OnlyAdd n m -> CnInn m -> Term m
buildCnInn ti !oa ci = case ci of
  TeleEnd (CnInnEnd args) -> applyThose (mkSubsOnlyAdd ti oa) args
  TeleCons a ci_a rbn -> mkPiRBN rbn a $
    buildCnInn ti (onlyAddAdd1 oa) ci_a

elUnPi :: Eliftend Term m -> Maybe (Eliftend Term m, Eliftend Term (m + 1))
elUnPi (Eliftend el t) = case unPi t of
  Nothing -> Nothing
  Just (a, b) -> Just
    ( Eliftend el a
    , Eliftend (mkElPre1 el) b )

elSubsumedByUniv :: [ZLe ZVal] -> Eliftend Term m -> Level -> Bool
elSubsumedByUniv rs (Eliftend _ a) l = subsumedByUniv rs a l

elSubsumedBy :: [ZLe ZVal] -> Eliftend Term m -> Eliftend Term m -> Bool
elSubsumedBy rs (eliftendToTerm -> a) (eliftendToTerm -> b) =
  subsumedBy rs a b

buildIndSortTele :: Level -> IndSortTele m -> Term m
buildIndSortTele l tl = case tl of
  TeleEnd ~TeleEndEmpty -> mkUniv l
  TeleCons a tl_a rbn -> mkPiRBN rbn a $ buildIndSortTele l tl_a

elSubstituteAtList :: Eliftend Term m -> Eliftend (ListF Term) (m + 1) ->
  Eliftend (ListF Term) m
elSubstituteAtList elnda elndl =
  mapEliftend
    (\ (Cart (Eliftend elA a) (FSuc (Eliftend elL (ListF l)))) ->
      let
        res =
          (mkSubstitute (mkSubsEl a elA) . flip mkSubsEl elL) <$> l
      in foldr seq () res `seq` ListF res)
    (commonBindEliftend elnda elndl)

elMkCnRBN :: RBN -> Int -> Eliftend Term m ->
  [Eliftend Term m] -> Eliftend Term m
elMkCnRBN rbn i ti as =
  mapEliftend
    (\ (Cart (Eliftend elTi xTi) (Eliftend elC (ContEliftend f))) ->
      mkCnRBN rbn i (mkSubsEl xTi elTi) $ smallArrayFromList $
        unListF $ f elC)
    (commonEliftend ti (commonContEliftendListWith (flip mkSubsEl) as))

elApplyIndAnns :: Eliftend Term m -> Eliftend (ListF Term) m ->
  Eliftend Term m
elApplyIndAnns elnTi elnAs =
  mapEliftend
    (\ (Cart (Eliftend elTi xTi) (Eliftend elL (ListF l))) ->
      applyThose (mkSubsEl xTi elTi) (flip mkSubsEl elL <$> l))
    (commonEliftend elnTi elnAs)

composeEliftImm :: Elift n o -> S m n -> S m o
composeEliftImm el imm = mkImmCompose (immFromElift el) imm

eliftendSubsendTo :: Eliftend (Subsend s) m -> Subsend s m
eliftendSubsendTo (Eliftend el (Subsend x imm)) =
  Subsend x (composeEliftImm el imm)

elMkSubstituteAtSubsend :: Eliftend Term m ->
  Eliftend (Subsend s) (m + 1) -> Eliftend (Subsend s) m
elMkSubstituteAtSubsend elnA elnF =
  mapEliftend
    (\ (Cart (eliftendToTerm -> tA) (FSuc (eliftendSubsendTo -> sF))) ->
      case sF of
        Subsend sF' immF' -> Subsend sF'
          (mkImmCompose (mkImmCons tA immId) immF'))
    (commonBindEliftend elnA elnF)

elApplyRecPred :: Eliftend (GRecPred (Eliftend Term)) m ->
  [Term m] -> Term m -> Term m
elApplyRecPred (Eliftend elR (RecPred tl)) =
  subsendApplyRecPredTele (Subsend tl (immFromElift elR))

subsendApplyRecPredTele :: Subsend (GRecPredTele (Eliftend Term)) m ->
  [Term m] -> Term m -> Term m
subsendApplyRecPredTele (Subsend tl immR) xs xti = case tl of
  TeleEnd (RecPredTeleEnd r _) -> case xs of
    [] -> mkSubs (eliftendToTerm r) (mkImmCons xti immR)
    _ -> bug "elApplyRecPred" "excess args"
  TeleCons _ tl_t _ -> case xs of
    [] -> bug "elApplyRecPred" "lacking args"
    (x : xsRest) -> subsendApplyRecPredTele
      (Subsend tl_t (mkImmCons x immR))
      xsRest
      xti

elMkRecRBNAndBuild :: RBN -> Eliftend Term m ->
  Eliftend (ContEliftend RecPredTele) m ->
  [GRecCase (Eliftend Term) m] -> Eliftend Term m -> Eliftend Term m
elMkRecRBNAndBuild rbn el_ti contel_rp rcs_el el_a =
  mapEliftend
    (\ (Cart (Eliftend elR (SomeRecData rd)) (eliftendToTerm -> a)) ->
      mkRecRBN rbn (mkRecDataSubs rd (immFromElift elR)) a)
    (commonEliftend (elMkRecDataAndBuild el_ti contel_rp rcs_el) el_a)

data AndIntM a m = AndIntM {-# UNPACK #-} !Int !(a m)

common3Eliftend :: Eliftend a m -> Eliftend b m -> Eliftend c m ->
  Eliftend (Cart (Eliftend a) (Cart (Eliftend b) (Eliftend c))) m
common3Eliftend elna elnb elnc =
  mapEliftend
    (\ (Cart xa (Eliftend el2 (Cart x2b x2c))) ->
      Cart xa (Cart
        (collapseEliftend (Eliftend el2 x2b))
        (collapseEliftend (Eliftend el2 x2c))))
    (commonEliftend elna (commonEliftend elnb elnc))

elMkRecDataAndBuild :: forall m. Eliftend Term m ->
  Eliftend (ContEliftend RecPredTele) m ->
  [GRecCase (Eliftend Term) m] -> Eliftend SomeRecData m
elMkRecDataAndBuild el_ti contel_rp rcs_el =
  let
    icontels_rcs :: [Eliftend (AndIntM (ContEliftend RecCase)) m]
    icontels_rcs =
      zipWith (\ i rc_el -> mapEliftend
        (\ cont -> AndIntM i cont)
        (commonContEliftendRecCaseWoBuiltWith (flip mkSubsEl) rc_el))
        [0::Int ..] rcs_el

    op_magic_rc :: Elift n m' -> AndIntM (ContEliftend RecCase) n ->
      FromTermN RecCase m'
    op_magic_rc el (AndIntM idx cont) = FromTermN (\ ti ->
      buildRecCase idx ti (unContEliftend cont el))

    magic_rcs :: Eliftend (ContEliftend (ListF (FromTermN RecCase))) m
    magic_rcs = commonContEliftendListWith op_magic_rc icontels_rcs

    comm_ti_rp_rcs :: Eliftend (Cart (Eliftend Term)
      (Cart (Eliftend (ContEliftend RecPredTele))
        (Eliftend (ContEliftend (ListF (FromTermN RecCase)))))) m
    comm_ti_rp_rcs = common3Eliftend el_ti contel_rp magic_rcs
  in mapEliftend
    (\ (Cart (Eliftend elti xti)
        (Cart (Eliftend elrp xrp) (Eliftend ell xl))) ->
      let ti = mkSubsEl xti elti in
      SomeRecData $ RecData immId ti (RecPred $ unContEliftend xrp elrp) $
        smallArrayFromList $
        map (`unFromTermN` ti) $ unListF (unContEliftend xl ell))
    comm_ti_rp_rcs

buildRecCase :: Int -> Term m -> RecCase m -> RecCase m
buildRecCase idx ti pre_rc = case indDataOfInductive ti of
  Nothing -> bug "buildRecCase" "checked to be Ind"
  Just (Subsend indd imm) -> case indd of
    IndData _ cns ->
      buildRecCaseCn (Subsend (indexSmallArray cns idx) imm) pre_rc

buildRecCaseCn :: Subsend CnSig m -> RecCase m -> RecCase m
buildRecCaseCn scn pre_rc = case scn of
  Subsend cn imm -> case pre_rc of
    RecCaseEnd{} -> pre_rc
    RecCaseNR rcRest rbn -> case cn of
      CnSigNR _ cnRest _ ->
        RecCaseNR (buildRecCaseCn (Subsend cnRest (mkImmPreVar imm)) rcRest)
          rbn
      _ -> bugNoMatch
    RecCaseR rcRest rbn1 rbn2 _ -> case cn of
      CnSigR _ cin cnRest _ ->
        RecCaseR (buildRecCaseCn
          (Subsend cnRest (mkImmAdd# (kWord# @1) $ mkImmAdd# (kWord# @1) imm))
          rcRest) rbn1 rbn2
          (Built $ buildRecCaseCin (Subsend cin imm))
      _ -> bugNoMatch
  where
  bugNoMatch = bug "buildRecCaseCn" "RecCase, CnSig not matching"

buildRecCaseCin :: Subsend CnInn m -> GTele Term TeleEndEmpty m
buildRecCaseCin (Subsend cin imm) = case cin of
  TeleEnd _ -> TeleEnd TeleEndEmpty
  TeleCons t cin_t rbn -> TeleCons (mkSubs t imm)
    (buildRecCaseCin (Subsend cin_t (mkImmPreVar imm))) rbn

elIfApplicationOfIndApplyRecPred :: Eliftend Term m ->
  Eliftend (ContEliftend RecPredTele) m ->
  Eliftend Term m -> Eliftend Term m -> Maybe (Eliftend Term m)
elIfApplicationOfIndApplyRecPred el_ti contel_rp el_ty_a el_a =
  case commonEliftend el_ti el_ty_a of
    Eliftend elUn (Cart (eliftendToTerm -> xTi) (eliftendToTerm -> xTa)) ->
      case isApplicationOfInductive xTi xTa of
        Nothing -> Nothing
        Just (toList -> anns) -> Just $
          mapEliftend
            (\ (Cart (Eliftend elR xR) (Cart
                (Eliftend elAs xAs) (Eliftend elA xA))) ->
              applyRecPredTele (unContEliftend xR elR) immId
                (flip mkSubsEl elAs <$> unListF xAs) (mkSubsEl xA elA))
            (common3Eliftend contel_rp (Eliftend elUn (ListF anns)) el_a)

-- * Type checking algorithm

data DoWhat =
  -- | Only setup the term for reduction, trusting that it is correct.
  OnlyConstr
  -- | Check the term, setup it for reduction and build its type.
  | ConstrAndCheck

data SDoWhat (what :: DoWhat) where
  SOnlyConstr     :: SDoWhat 'OnlyConstr
  SConstrAndCheck :: SDoWhat 'ConstrAndCheck

type family SelDoWhat (what :: DoWhat) b where
  SelDoWhat 'OnlyConstr     b = ()
  SelDoWhat 'ConstrAndCheck b = b

data TcRes what a b = TcRes{tcMade :: !a, tcChecked :: !(SelDoWhat what b)}

doWhatCases :: Monad f => SDoWhat what -> f (TcRes what a b) ->
  (a -> f a') ->
  ((what ~ 'ConstrAndCheck) => TcRes what a b -> a' -> f b') ->
  f (TcRes what a' b')
doWhatCases what act contC contCAC = do
  tcab@(TcRes{tcMade = a, tcChecked = _}) <- act
  a' <- contC a
  case what of
    SOnlyConstr -> pure TcRes{tcMade = a', tcChecked = ()}
    SConstrAndCheck -> do
      b' <- contCAC tcab a'
      pure TcRes{tcMade = a', tcChecked = b'}

doWhatCasesInit :: Monad f => SDoWhat what ->
  f a' ->
  ((what ~ 'ConstrAndCheck) => a' -> f b') -> f (TcRes what a' b')
doWhatCasesInit what contC contCAC = do
  a' <- contC
  case what of
    SOnlyConstr -> pure TcRes{tcMade = a', tcChecked = ()}
    SConstrAndCheck -> do
      b' <- contCAC a'
      pure TcRes{tcMade = a', tcChecked = b'}

type Given = (Globals, [ZLe ZVal])

{-
Used for helping type inference, impacted by MonoLocalBinds(?).
-}
areEliftendTerms :: f (TcRes what (Eliftend Term m) (Eliftend Term m)) ->
  f (TcRes what (Eliftend Term m) (Eliftend Term m))
areEliftendTerms = id

tcRes :: forall what m. Given -> SDoWhat what -> PTerm ->
  CheckM m (TcRes what (Eliftend Term m) (Eliftend Term m))
tcRes _given !what (PRefer sl w) = do
  ctx <- askCtx
  case w of
    WordVal (_ :: _ i) -> case atContextAndKnown# (kWord# @i) ctx of
      Nothing -> checkThrow (CheckExcept CETReferOutOfScope sl [])
      Just (el_ty, Dict) ->
        withDict (K.rightPlusComm @(m - (i + 1)) @1 @i) $
        withDict (K.plusAss @(m - (i + 1)) @i @1) $
        withDict (K.rightMinusPlus @m @(i + 1)) $
        withDict (K.plusKnown @i @1) $
        doWhatCasesInit what
          (pure $ Eliftend (elAdd# (kWord# @i)) $ newestVar @(m - (i + 1)))
          (\ _ -> pure $ collapseEliftend $
            Eliftend (elAdd# (kWord# @(i + 1))) el_ty)
tcRes _given what (PUniv _sl l) = doWhatCasesInit what
  (pure $ zeroEliftend (mkUniv l))
  (\ _ -> pure $ zeroEliftend (mkUniv (zvalAdd 1 l)))
tcRes given what (PPi sl pa pb_a rbn) = do
  TcRes{tcMade = el_a, tcChecked = el_ty_a} <- areEliftendTerms $
    doWhatCases what (tcRes given what pa)
      pure
      (\ TcRes{tcChecked = el_ty_a} _ -> case elLevelOfUniverse el_ty_a of
        Just _ -> pure el_ty_a
        Nothing -> checkThrow (CheckExcept CETPiDomainNotType sl []))
  TcRes{tcMade = el_b_a, tcChecked = el_ty_b_a} <- areEliftendTerms $
    doWhatCases what
      (localDecl el_a (tcRes given what pb_a))
      pure
      (\ TcRes{tcChecked = el_ty_b_a} _ -> case elLevelOfUniverse el_ty_b_a of
        Just _ -> pure el_ty_b_a
        Nothing -> checkThrow (CheckExcept CETPiCodomainNotType sl []))
  areEliftendTerms $ doWhatCasesInit what
    (pure $ elMkPiRBN rbn el_a el_b_a)
    (\ _ -> case elLevelOfUniverse el_ty_a of
      Just la
        | Just lb <- elLevelOfUniverse el_ty_b_a ->
          pure $ zeroEliftend $ mkUniv (zvalMax la lb)
      _ -> bug "tcRes" "Pi levels, already checked")
tcRes given what (PLam sl pa pe_a rbn) = do
  TcRes{tcMade = el_a, tcChecked = _} <- areEliftendTerms $
    doWhatCases what (tcRes given what pa)
      pure
      (\ TcRes{tcChecked = el_ty_a} _ -> case elLevelOfUniverse el_ty_a of
        Just _ -> pure el_ty_a
        Nothing -> checkThrow (CheckExcept CETLamDomainNotType sl []))
  TcRes{tcMade = el_e_a, tcChecked = el_ty_e_a} <-
    localDecl el_a (tcRes given what pe_a)
  doWhatCasesInit what
    (pure $ elMkLamRBN rbn el_a el_e_a)
    (\ _ -> pure $ elMkPiRBN (eraseRegister rbn) el_a el_ty_e_a)
tcRes given what (PApp sl pf pc rbn) = do
  TcRes{tcMade = el_f, tcChecked = el_ty_f} <- areEliftendTerms $
    doWhatCases what (tcRes given what pf)
      pure
      (\ TcRes{tcChecked = el_ty_f} _ -> pure el_ty_f)
  TcRes{tcMade = el_c, tcChecked = _} <- areEliftendTerms $
    doWhatCases what (tcRes given what pc)
      pure
      (\ TcRes{tcChecked = el_ty_c} _ -> case elUnPi el_ty_f of
        Just (el_fst_ty_f, _)
          | elSubsumedBy (snd given) el_ty_c el_fst_ty_f -> pure el_ty_c
          | otherwise -> checkThrow
            (CheckExcept CETAppNotSubsumedType sl
              [CheckExceptArg (eliftendToTerm el_fst_ty_f)])
        _ -> checkThrow (CheckExcept CETAppNotFunction sl []))
  areEliftendTerms $ doWhatCasesInit what
    (pure $ elMkAppRBN rbn el_f el_c)
    (\ _ -> case elUnPi el_ty_f of
      Just (_, el_snd_ty_f) -> pure $ elMkSubstitute el_c el_snd_ty_f
      _ -> bug "tcRes" "App to function, already checked")
tcRes given what (PInd _ l ptl pcns) = do
  el_tl <- tcResSortTeleAtLevel given what l ptl
  els_cn_pre <- traverse (tcResCnSigWoBuilt given what l el_tl) pcns
  let (el_ti, el_s) = indAndSortFromWoBuiltWoUnionedElift l el_tl els_cn_pre
  areEliftendTerms $ doWhatCasesInit what
    (pure $ el_ti)
    (\ _ -> pure el_s)
tcRes given what (PCn sl i pti pas rbn) = do
  TcRes{tcMade = el_ti} <- tcRes given what pti
  (el_sindd :: SelDoWhat what (Eliftend SubsIndData m)) <- case what of
    SOnlyConstr -> pure ()
    SConstrAndCheck -> case elIndDataOfInductive el_ti of
      Nothing -> checkThrow (CheckExcept CETCnNotInd sl [])
      Just el_sindd -> pure el_sindd
  TcRes{tcMade = els_args, tcChecked = el_anns} <-
    tcResAnnsForCn given what sl i el_sindd pas
  areEliftendTerms $ doWhatCasesInit what
    (pure $ elMkCnRBN rbn i el_ti els_args)
    (\ _ -> pure $ elApplyIndAnns el_ti el_anns)
tcRes given what (PRec sl pti prp prcs pa rbn) = do
  TcRes{tcMade = el_ti} <- tcRes given what pti
  -- Even when @what@ is 'SOnlyConstr', it is necessary to reduce the
  -- inductive family, in order to get the constructor signatures,
  -- used in the Built of RecCase.
  (el_sindd :: Eliftend SubsIndData m) <- case elIndDataOfInductive el_ti of
    Nothing -> checkThrow (CheckExcept CETRecNotInd sl [])
    Just el_sindd -> pure el_sindd
  rp_el <- tcRecPred given what el_ti el_sindd prp
  rcs_el <- tcRecCasesWoBuilt given what sl el_ti rp_el el_sindd prcs
  TcRes{tcMade = el_a, tcChecked = el_ty_a} <- tcRes given what pa
  let contel_rp = commonContEliftendGRecPredTeleWith
        (flip mkSubsEl)
        (case rp_el of RecPred r -> r)
  areEliftendTerms $ doWhatCasesInit what
    (pure $ elMkRecRBNAndBuild rbn el_ti contel_rp rcs_el el_a)
    (\ _ ->
      case elIfApplicationOfIndApplyRecPred el_ti contel_rp el_ty_a el_a of
        Just el_res -> pure el_res
        Nothing -> checkThrow (CheckExcept CETRecDifferentIndType sl
          [CheckExceptArg (eliftendToTerm el_ty_a)]))
tcRes _given what (PPrimUnNat _ m) =
  areEliftendTerms $ doWhatCasesInit what
    (pure $ zeroEliftend $ mkPrimUnNat m)
    (\ _ -> pure $ zeroEliftend $ mkUnNatInd)
tcRes _given what (PPrimBbNat _ m) =
  areEliftendTerms $ doWhatCasesInit what
    (pure $ zeroEliftend $ mkPrimBbNat m)
    (\ _ -> pure $ zeroEliftend $ mkBbNatInd)
tcRes given what (PGlobal sl hash ls) =
  case accessGlobalCheckingLevels (fst given) (snd given) hash ls of
    Left NotFoundDef -> checkThrow (CheckExcept CETGlobalNotFound sl [])
    Left FoundButWrongNumLevels ->
      checkThrow (CheckExcept CETGlobalWrongNumLevels sl [])
    Left FoundButNotSatLevels ->
      checkThrow (CheckExcept CETGlobalLevelNotSatisfying sl [])
    Right (g_def_ls, g_ty_ls) -> areEliftendTerms $
      doWhatCasesInit what
        (pure $ zeroEliftend $ g_def_ls)
        (\ _ -> pure $ zeroEliftend $ g_ty_ls)
tcRes given what (PLet _ idx ploc prest) = do
  TcRes{tcMade = el_loc, tcChecked = el_ty_loc} <- tcRes given what ploc
  withAddToLocalDefMaps idx Typed
    { tTerm = el_loc
    , tType = case what of
        SOnlyConstr -> idEliftend $ DontCare
        SConstrAndCheck -> el_ty_loc
    } $ tcRes given what prest
tcRes _given what (PLocalDef sl idx) = do
  may_typed <- withIndexLocalDefMaps idx
  case may_typed of
    Nothing -> checkThrow (CheckExcept CETLocalDefOutOfScope sl [])
    Just Typed{tTerm = el_tt, tType = el_ty} ->
      doWhatCasesInit what
        (pure el_tt)
        (\ _ -> pure el_ty)

-- Ind checking:

tcResSortTeleAtLevel :: Given -> SDoWhat what -> Level -> PIndSortTele ->
  CheckM m (GTele (Eliftend Term) (Eliftend TeleEndEmpty) m)
tcResSortTeleAtLevel given what l ptl = case ptl of
  PIndSortTeleEnd _ -> pure $ TeleEnd (zeroEliftend TeleEndEmpty)
  PIndSortTeleCons sl pt ptl_1 rbn -> do
    TcRes{tcMade = el_t, tcChecked = _} <- areEliftendTerms $
      doWhatCases what (tcRes given what pt)
        pure
        (\ TcRes{tcChecked = el_ty_t} _ ->
          if elSubsumedByUniv (snd given) el_ty_t l
            then pure el_ty_t
            else checkThrow (CheckExcept CETIndSortNotInLeUniv sl []))
    tl_1 <- localDecl el_t $ tcResSortTeleAtLevel given what l ptl_1
    pure $ TeleCons el_t tl_1 rbn

tcResCnSigWoBuilt :: Given -> SDoWhat what -> Level ->
  GTele (Eliftend Term) (Eliftend TeleEndEmpty) m ->
  PCnSig -> CheckM m (GCnSig (Eliftend Term) m)
tcResCnSigWoBuilt given what l tl pcn =
  tcResCnSigWoBuiltH given what l tl pcn onlyAddId

tcResCnSigWoBuiltH :: Given -> SDoWhat what -> Level ->
  GTele (Eliftend Term) (Eliftend TeleEndEmpty) m ->
  PCnSig -> OnlyAdd m n -> CheckM n (GCnSig (Eliftend Term) n)
tcResCnSigWoBuiltH given what l tl pcn !onlyAdd = case pcn of
  PCnSigEnd sl pts -> do
    (CnSigAnns . smallArrayFromList) <$>
      tcResAnn given what sl tl pts onlyAdd
  PCnSigNR sl pt pcn_1 rbn -> do
    TcRes{tcMade = el_t, tcChecked = _} <- areEliftendTerms $
      doWhatCases what (tcRes given what pt)
        pure
        (\ TcRes{tcChecked = el_ty_t} _ ->
          if elSubsumedByUniv (snd given) el_ty_t l
            then pure el_ty_t
            else checkThrow (CheckExcept CETIndCnSigNotInLeUniv sl []))
    el_cn_1 <- localDecl el_t $ tcResCnSigWoBuiltH given what l tl
      pcn_1 (onlyAddAdd1 onlyAdd)
    pure $ CnSigNR el_t el_cn_1 rbn
  PCnSigR _sl pci pcna rbn -> do
    el_ci <- tcResCnInnH given what l tl pci onlyAdd
    el_cna <- tcResCnSigWoBuiltH given what l tl
      pcna onlyAdd
    pure $ CnSigR (bug "tcResCnSigWoBuiltH" "not built") el_ci el_cna rbn

tcResCnInnH :: Given -> SDoWhat what -> Level ->
  GTele (Eliftend Term) (Eliftend TeleEndEmpty) m ->
  PCnInn -> OnlyAdd m n -> CheckM n (GCnInn (Eliftend Term) n)
tcResCnInnH given what l tl pci !onlyAdd = case pci of
  PCnInnEnd sl pts -> do
    (TeleEnd . CnInnEnd . smallArrayFromList) <$>
      tcResAnn given what sl tl pts onlyAdd
  PCnInnPi sl pt pci_1 rbn -> do
    TcRes{tcMade = el_t, tcChecked = _} <- areEliftendTerms $
      doWhatCases what (tcRes given what pt)
        pure
        (\ TcRes{tcChecked = el_ty_t} _ ->
          if elSubsumedByUniv (snd given) el_ty_t l
            then pure el_ty_t
            else checkThrow (CheckExcept CETIndCnSigNotInLeUniv sl []))
    el_ci_1 <- localDecl el_t $ tcResCnInnH given what l tl
      pci_1 (onlyAddAdd1 onlyAdd)
    pure $ TeleCons el_t el_ci_1 rbn

tcResAnn :: Given -> SDoWhat what -> SL ->
  GTele (Eliftend Term) (Eliftend TeleEndEmpty) m ->
  [PTerm] -> OnlyAdd m n -> CheckM n [Eliftend Term n]
tcResAnn given !what !slI el_tl pts !onlyAdd =
  tcResAnnH given what slI el_tl pts (onlyAddToImm onlyAdd)

tcResAnnH :: Given -> SDoWhat what -> SL ->
  GTele (Eliftend Term) (Eliftend TeleEndEmpty) m ->
  [PTerm] -> ImmEsubs m n -> CheckM n [Eliftend Term n]
tcResAnnH given !what !slI el_tl pts !imm = case el_tl of
  TeleEnd (Eliftend _ ~TeleEndEmpty) -> case pts of
    [] -> pure []
    _ -> checkThrow (CheckExcept CETIndCnSigExcessAnn slI [])
  TeleCons el_a el_tl_a _ -> case pts of
    [] -> checkThrow (CheckExcept CETIndCnSigLackAnn slI [])
    pt : pts' -> do
      TcRes{tcMade = el_t} <- areEliftendTerms $
        doWhatCases what (tcRes given what pt)
          pure
          (\ TcRes{tcChecked = el_ty_t} _ ->
            if subsumedBy (snd given) (eliftendToTerm el_ty_t)
              (mkSubs (eliftendToTerm el_a) imm)
              then pure el_ty_t
              else checkThrow (CheckExcept CETIndCnSigAnnNotSubsumedType
                (getSL pt) [CheckExceptArg (eliftendToTerm el_ty_t)]))
      (el_t :) <$> tcResAnnH given what slI el_tl_a pts'
        (mkImmCons (eliftendToTerm el_t) imm)

indAndSortFromWoBuiltWoUnionedElift :: Level ->
  GTele (Eliftend Term) (Eliftend TeleEndEmpty) m ->
  [GCnSig (Eliftend Term) m] -> (Eliftend Term m, Eliftend Term m)
indAndSortFromWoBuiltWoUnionedElift l el_tl_el els_cn_el_pre =
  let
    elcont_tl = commonContEliftendGTeleWith
      (flip mkSubsEl) (\ _ _ -> TeleEndEmpty) el_tl_el
    elconts_cn_pre = commonContEliftendGCnSigWoBuiltWith
      (flip mkSubsEl) <$> els_cn_el_pre
  in
    ( elContMkIndAndBuild l elcont_tl elconts_cn_pre
    , mapEliftend (buildIndSortTele l . idContEliftend) elcont_tl )

-- Cn checking:

-- | Returns @TcRes{tcMade = constrArgs, tcChecked = typeAnnotations}@.
tcResAnnsForCn :: Given -> SDoWhat what -> SL -> Int ->
  SelDoWhat what (Eliftend SubsIndData m) -> [PTerm] ->
  CheckM m (TcRes what [Eliftend Term m] (Eliftend (ListF Term) m))
tcResAnnsForCn given !what !slI !i el_sindd pas = case what of
  SOnlyConstr -> do
    els_arg <- traverse (fmap tcMade . tcRes given SOnlyConstr) pas
    pure TcRes{tcMade = els_arg, tcChecked = ()}
  SConstrAndCheck -> case el_sindd of
    Eliftend el (Subsend (IndData _ cns) imm) ->
      case maybeIndexSmallArray cns i of
        Nothing -> checkThrow (CheckExcept CETCnNotExistent slI [])
        Just cn -> tcResAnnsForCnH given slI
          (Eliftend el (Subsend cn imm))
          pas

tcResAnnsForCnH :: Given -> SL ->
  Eliftend (Subsend CnSig) m ->
  [PTerm] ->
  CheckM m (TcRes 'ConstrAndCheck [Eliftend Term m] (Eliftend (ListF Term) m))
tcResAnnsForCnH given !slI !el_scns pas = case pas of
  [] -> case el_scns of
    Eliftend elC (Subsend cn immC) -> case cn of
      CnSigAnns anns -> pure TcRes
        { tcMade = []
        , tcChecked = Eliftend elC (ListF (flip mkSubs immC <$> toList anns))
        }
      _ -> checkThrow (CheckExcept CETCnLackArg slI [])
  pa : pasRest -> case el_scns of
    Eliftend elC (Subsend cn immC) -> case cn of
      CnSigAnns{} -> checkThrow (CheckExcept CETCnExcessArg slI [])
      CnSigNR t cn_t _ -> do
        TcRes{tcMade = el_a} <- areEliftendTerms $ doWhatCases SConstrAndCheck
          (tcRes given SConstrAndCheck pa)
          pure
          (\ TcRes{tcChecked = el_ty_a} _ ->
            if subsumedBy (snd given) (eliftendToTerm el_ty_a)
                (mkSubs t (composeEliftImm elC immC))
              then pure el_ty_a
              else checkThrow (CheckExcept CETCnNotSubsumedType (getSL pa)
                [CheckExceptArg (eliftendToTerm el_ty_a)]))
        TcRes{tcMade = els_asRest, tcChecked = el_anns} <-
          tcResAnnsForCnH given slI
            (elMkSubstituteAtSubsend el_a
              (Eliftend (mkElPre1 elC) (Subsend cn_t (mkImmPreVar immC))))
            pasRest
        pure TcRes
          { tcMade = el_a : els_asRest
          , tcChecked = el_anns }
      CnSigR (Built t) _ cnRest _ -> do
        TcRes{tcMade = el_a} <- areEliftendTerms $ doWhatCases SConstrAndCheck
          (tcRes given SConstrAndCheck pa)
          pure
          (\ TcRes{tcChecked = el_ty_a} _ ->
            if subsumedBy (snd given) (eliftendToTerm el_ty_a)
                (mkSubs t (composeEliftImm elC immC))
              then pure el_ty_a
              else checkThrow (CheckExcept CETCnNotSubsumedType (getSL pa)
                [CheckExceptArg (eliftendToTerm el_ty_a)]))
        TcRes{tcMade = els_asRest, tcChecked = el_anns} <-
          tcResAnnsForCnH given slI
            (Eliftend elC (Subsend cnRest immC))
            pasRest
        pure TcRes
          { tcMade = el_a : els_asRest
          , tcChecked = el_anns }

-- Rec checking:

tcRecPred :: Given -> SDoWhat what ->
  Eliftend Term m ->
  Eliftend SubsIndData m ->
  PRecPred ->
  CheckM m (GRecPred (Eliftend Term) m)
tcRecPred given !what el_ti el_sindd prp = case el_sindd of
  Eliftend elI (Subsend indd immI) -> case indd of
    IndData (IndSort _ tl) _ -> RecPred <$> tcRecPredH given what
      (eliftendToTerm el_ti)
      (Eliftend elI (Subsend tl immI))
      prp

tcRecPredH :: Given -> SDoWhat what ->
  Term m ->
  Eliftend (Subsend IndSortTele) m ->
  PRecPred ->
  CheckM m (GRecPredTele (Eliftend Term) m)
tcRecPredH given !what !tiapp el_stl prp = case prp of
  PRecPred slI 0 pp rbns -> do
    case el_stl of
      Eliftend _elT (Subsend tl _immT) -> case tl of
        TeleEnd ~TeleEndEmpty -> do
          TcRes{tcMade = el_p} <- localDecl (idEliftend tiapp) $
            areEliftendTerms $ doWhatCases what
              (tcRes given what pp)
              pure
              (\ TcRes{tcChecked = el_ty_p} _ ->
                case elLevelOfUniverse el_ty_p of
                  Just _ -> pure el_ty_p
                  Nothing -> checkThrow (CheckExcept CETRecPredTypeNotUniv
                    slI []))
          pure $ TeleEnd $ RecPredTeleEnd el_p (case rbns of
            [] -> defaultRBN
            rbn : _ -> rbn)
        TeleCons{} -> checkThrow (CheckExcept CETRecPredLackBind slI [])
  PRecPred slI i pp rbns -> do
    case el_stl of
      Eliftend elT (Subsend tl immT) -> case tl of
        TeleEnd ~TeleEndEmpty -> checkThrow (CheckExcept
          CETRecPredExcessBind slI [])
        TeleCons t tl_t _ -> do
          rp_t <- localDecl (Eliftend elT (mkSubs t immT)) $
            tcRecPredH given what (etaExpand tiapp)
              (Eliftend (mkElPre1 elT) (Subsend tl_t (mkImmPreVar immT)))
              (PRecPred slI (i - 1) pp $! drop 1 rbns)
          pure $ TeleCons (zeroEliftend DontCare) rp_t $ case rbns of
            [] -> defaultRBN
            rbn : _ -> rbn

tcRecCasesWoBuilt :: Given -> SDoWhat what -> SL ->
  Eliftend Term m ->
  GRecPred (Eliftend Term) m ->
  Eliftend (Subsend IndData) m ->
  [PRecCase] ->
  CheckM m [GRecCase (Eliftend Term) m]
tcRecCasesWoBuilt given !what !sl el_ti rp_el el_sindd prcs = case el_sindd of
  Eliftend elI (Subsend indd immI) -> case indd of
    IndData _ cns
      | length cns == length prcs ->
        traverse
          (\ (i, prc) -> tcRecCaseWoBuilt given what el_ti
            (idEliftend rp_el)
            (Eliftend elI (Subsend (indexSmallArray cns i) immI)) i
            initContRecGVars
            prc)
          (zip [0 ..] prcs)
      | otherwise -> checkThrow (CheckExcept CETRecDifferentNumberCases sl [])

newtype ContRecGVars m = ContRecGVars {unContRecGVars :: forall n.
  OnlyAdd m n -> [Term n] -> [Term n]}
-- @[a] -> [a]@ instead of @[a]@ so that we can append elements at end,
-- without quadratical cost.

initContRecGVars :: ContRecGVars m
initContRecGVars = ContRecGVars (\ _ rest -> rest)

nonrecContRecGVars, skipContRecGVars :: ContRecGVars m -> ContRecGVars (m + 1)
nonrecContRecGVars f = ContRecGVars $ \ oa rest ->
  let !v = mkSubsOnlyAdd (newestVarProxy f) oa in
  (unContRecGVars f $! onlyAddCoadd1 oa) (v : rest)

skipContRecGVars f = ContRecGVars $ \ oa rest ->
  (unContRecGVars f $! onlyAddCoadd1 oa) rest

recContRecGVars :: ContRecGVars m -> ContRecGVars (m + 1 + 1)
recContRecGVars f = skipContRecGVars (nonrecContRecGVars f)

listFromContRecGVars :: ContRecGVars m -> [Term m]
listFromContRecGVars f = unContRecGVars f onlyAddId []

tcRecCaseWoBuilt :: Given -> SDoWhat what ->
  Eliftend Term m ->
  Eliftend (GRecPred (Eliftend Term)) m ->
  Eliftend (Subsend CnSig) m -> Int ->
  ContRecGVars m ->
  PRecCase ->
  CheckM m (GRecCase (Eliftend Term) m)
tcRecCaseWoBuilt given !what !el_ti !el_rp_el el_scn !idxprc gvars prc =
  case prc of
    PRecCaseEnd sl pr -> case el_scn of
      Eliftend elC (Subsend cn immC) -> case cn of
        CnSigAnns anns -> do
          TcRes{tcMade = el_r} <- areEliftendTerms $ doWhatCases what
            (tcRes given what pr)
            pure
            (\ TcRes{tcChecked = el_ty_r} _ ->
              if subsumedBy (snd given) (eliftendToTerm el_ty_r) $
                  elApplyRecPred el_rp_el
                    (map (flip mkSubs (composeEliftImm elC immC))
                      (toList anns))
                      (mkCn idxprc
                        (eliftendToTerm el_ti)
                        (smallArrayFromList (listFromContRecGVars gvars)))
                then pure el_ty_r
                else checkThrow (CheckExcept CETRecCaseNotSubsumedType sl []))
          pure $ RecCaseEnd el_r
        _ -> checkThrow (CheckExcept CETRecCaseLackBind sl [])
    PRecCaseCons sl PRecCase1 prcRest rbn _ -> case el_scn of
      Eliftend elC (Subsend cn immC) -> case cn of
        CnSigNR t cn_t _ -> do
          rcRest <- localDecl (Eliftend elC (mkSubs t immC)) $
            tcRecCaseWoBuilt given what
              (collapseEliftend (Eliftend elAdd1 el_ti))
              (collapseEliftend (Eliftend elAdd1 el_rp_el))
              (Eliftend (mkElPre1 elC) (Subsend cn_t (mkImmPreVar immC)))
              idxprc
              (nonrecContRecGVars gvars)
              prcRest
          pure $ RecCaseNR rcRest rbn
        _ -> checkThrow (CheckExcept CETRecCaseUnexpected1 sl [])
    PRecCaseCons sl PRecCase2 prcRest rbn1 rbn2 -> case el_scn of
      Eliftend elC (Subsend cn immC) -> case cn of
        CnSigR (Built blt) cin cnRest _ -> do
          let
            el_ty_rechyp = idEliftend $ buildRecHypType
              (Eliftend (mkElAdd1 elC) (Subsend cin immC))
              (collapseEliftend (Eliftend elAdd1 el_rp_el))
              (newestVarProxy el_rp_el)
          rcRest <- localDecl (Eliftend elC (mkSubs blt immC)) $
            localDecl el_ty_rechyp $
            tcRecCaseWoBuilt given what
              (collapseEliftend (Eliftend (mkElAdd1 elAdd1) el_ti))
              (collapseEliftend (Eliftend (mkElAdd1 elAdd1) el_rp_el))
              (Eliftend (mkElAdd1 (mkElAdd1 elC))
                (Subsend cnRest immC))
              idxprc
              (recContRecGVars gvars)
              prcRest
          pure $ RecCaseR rcRest rbn1 rbn2
            (bug "tcRecCaseWoBuilt" "not built")
        _ -> checkThrow (CheckExcept CETRecCaseUnexpected2 sl [])

buildRecHypType :: Eliftend (Subsend CnInn) r ->
  Eliftend (GRecPred (Eliftend Term)) r ->
  Term r -> Term r
buildRecHypType el_scin !el_rp_el japp = case el_scin of
  Eliftend elC (Subsend cin immC) -> case cin of
    TeleEnd ~(CnInnEnd anns) ->
      elApplyRecPred el_rp_el
        (map (flip mkSubs (composeEliftImm elC immC))
          (toList anns))
        japp
    TeleCons t cin_t rbn ->
      mkPiRBN rbn (mkSubs t (composeEliftImm elC immC)) $
        buildRecHypType
          (Eliftend (mkElPre1 elC)
            (Subsend cin_t (mkImmPreVar immC)))
          (collapseEliftend (Eliftend elAdd1 el_rp_el))
          (etaExpand japp)

-- * Exceptions for type checking

data CheckExcept = CheckExcept !CheckExceptType {-# UNPACK #-} !SL
  [CheckExceptArg] deriving Show

data CheckExceptArg = forall m. CheckExceptArg !(Term m)
instance Show CheckExceptArg where
  show _ = "_term"

data CheckExceptType =
  CETReferOutOfScope
  | CETPiDomainNotType
  | CETPiCodomainNotType
  | CETLamDomainNotType
  | CETAppNotSubsumedType
  | CETAppNotFunction
  | CETIndSortNotInLeUniv
  | CETIndCnSigNotInLeUniv
  | CETIndCnSigExcessAnn
  | CETIndCnSigLackAnn
  | CETIndCnSigAnnNotSubsumedType
  | CETCnNotInd
  | CETCnNotExistent
  | CETCnLackArg
  | CETCnExcessArg
  | CETCnNotSubsumedType
  | CETRecNotInd
  | CETRecPredTypeNotUniv
  | CETRecPredLackBind
  | CETRecPredExcessBind
  | CETRecDifferentNumberCases
  | CETRecCaseNotSubsumedType
  | CETRecCaseLackBind
  | CETRecCaseUnexpected1
  | CETRecCaseUnexpected2
  | CETRecDifferentIndType
  | CETGlobalNotFound
  | CETGlobalWrongNumLevels
  | CETGlobalLevelNotSatisfying
  | CETLocalDefOutOfScope
  deriving Show

-- * Global definitions

-- | Lazily listing sequences @ℕ -> a@. Represented in bijective base 4:
--
-- @
-- NatTrie
--   (f 0₄)
--   (NatTrie
--     (f 1₄)
--     (NatTrie
--       (f 11₄)
--       …))
--   (NatTrie
--     (f 2₄)
--     (NatTrie
--       (f 12₄)
--       …)
--     …)
--   (NatTrie
--     (f 3₄)
--     …)
--   (NatTrie
--     (f 4₄)
--     …)
-- @
data NatTrie a =
  NatTrie a (NatTrie a) (NatTrie a) (NatTrie a) (NatTrie a)

rem4 :: Word -> Word
rem4 = (.&.) 3

quot4 :: Word -> Word
quot4 = (`shiftR` 2)

exp4 :: Int -> Word
exp4 = shiftL 1 . (2 *)

fromNatTrie :: NatTrie a -> (Word -> a)
fromNatTrie (NatTrie x0 t1 t2 t3 t4) m
  | m == 0 = x0
  | otherwise = case rem4 m of
    1 -> fromNatTrie t1 (quot4 m)
    2 -> fromNatTrie t2 (quot4 m)
    3 -> fromNatTrie t3 (quot4 m)
    _ -> fromNatTrie t4 (quot4 m - 1)
         -- Attention to minus 1.

toNatTrie :: (Word -> a) -> NatTrie a
toNatTrie f = go 0 0
  where
  go :: Int -> Word -> NatTrie _
  go !e !r = NatTrie
    (f r)
    (go (e + 1) (exp4 e * 1 + r))
    (go (e + 1) (exp4 e * 2 + r))
    (go (e + 1) (exp4 e * 3 + r))
    (go (e + 1) (exp4 e * 4 + r))

data ListTrie t a = ListTrie a (t a) (t (t a))
  (t (t (t (ListTrie t a))))

fromListTrieWith :: (forall b. t b -> x -> b) -> ListTrie t a -> [x] -> a
fromListTrieWith appl (ListTrie a ta tta tttr) l = case l of
  []      -> a
  [x]     -> appl ta x
  [x, y]  -> appl (appl tta x) y
  x:y:z:r -> fromListTrieWith appl (appl (appl (appl tttr x) y) z) r

toListTrieWith :: (forall b. (x -> b) -> t b) -> ([x] -> a) -> ListTrie t a
toListTrieWith tabu f = ListTrie
  (f [])
  (tabu (\ x -> f [x]))
  (tabu (\ x -> tabu (\ y -> f [x, y])))
  (tabu (\ x -> tabu (\ y -> tabu (\ z ->
    toListTrieWith tabu (\ r -> f (x : y : z : r))))))

newtype PairTrie t1 t2 a = PairTrie (t1 (t2 a))

fromPairTrieWith :: (forall b. t1 b -> x1 -> b) ->
  (forall b. t2 b -> x2 -> b) ->
  PairTrie t1 t2 a -> (x1, x2) -> a
fromPairTrieWith ac1 ac2 (PairTrie t) (x1, x2) = ac2 (ac1 t x1) x2

toPairTrieWith :: (forall b. (x1 -> b) -> t1 b) ->
  (forall b. (x2 -> b) -> t2 b) ->
  ((x1, x2) -> a) -> PairTrie t1 t2 a
toPairTrieWith ta1 ta2 f = PairTrie
  (ta1 (\ x1 -> ta2 (\ x2 -> f (x1, x2))))

type Globals = M.Map Hash GlobalRsInfo

-- | Number of level variables, restrictions and trie.
type GlobalRsInfo = (Int, [ZLe ZVal], GlobalInfo)

-- | Trie of terms and types, indexed by lists of @(Int, Integer)@.
type GlobalInfo = ListTrie (PairTrie NatTrie NatTrie) (Term 0, Term 0)

integerToWord :: Integer -> Word
integerToWord i
  | 0 <= i && i <= fromIntegral (maxBound :: Word) = fromIntegral i
  | otherwise = error "integerToWord: overflow."

fromGlobalInfo :: GlobalInfo -> [(Int, Integer)] -> (Term 0, Term 0)
fromGlobalInfo =
  fromListTrieWith $ fromPairTrieWith
    (\ t i -> fromNatTrie t $! fromIntegral i)
    (\ t i -> fromNatTrie t $! integerToWord i)

toGlobalInfo :: ([(Int, Integer)] -> (Term 0, Term 0)) -> GlobalInfo
toGlobalInfo =
  toListTrieWith $ toPairTrieWith
    (\ f -> toNatTrie (f . (fromIntegral $!)))
    (\ f -> toNatTrie (f . (fromIntegral $!)))

data FoundDefErr = NotFoundDef | FoundButWrongNumLevels | FoundButNotSatLevels

accessGlobalCheckingLevels :: Globals -> [ZLe ZVal] -> Hash ->
  [(Int, Integer)] ->
  Either FoundDefErr (Term 0, Term 0)
accessGlobalCheckingLevels globs zles hsh ls = case M.lookup hsh globs of
  Nothing -> Left NotFoundDef
  Just (numl, needed, ginfo)
    | numl /= length ls -> Left FoundButWrongNumLevels
    | all (\ (n1, n2) -> impliesN zles (zvalSubs n1 ls, zvalSubs n2 ls))
        needed -> Right $ fromGlobalInfo ginfo ls
    | otherwise -> Left FoundButNotSatLevels

zvalFromVar0 :: Int -> ZVal
zvalFromVar0 0 = zvalZero
zvalFromVar0 i = zvalFromVar i

zvalSubs :: ZVal -> [(Int, Integer)] -> ZVal
zvalSubs a ls = unBuilt $
  I.foldlWithKey' (\ (Built r) idx coef -> Built $ case idx of
    0 -> zvalMax r (zvalFromInteger coef)
    _ -> zvalMax r (case ls !! (idx - 1) of
      (lidx, lcoef) -> zvalAdd (coef + lcoef) (zvalFromVar0 lidx)))
    (Built I.empty) a

isIdLSubs :: [(Int, Integer)] -> Bool
isIdLSubs = and .
  zipWith (\ i (idx, coef) -> i == idx && 0 == coef) [1 ..]

-- | Elements of 'Term' are a bit hard to analyse due to the implicit sharing.
-- Here is a way to analyse them, converting to a written form, without
-- changing their structure:
--
-- * use 'StableName' in order to detect identical subterms;
-- * also look up, using 'lookupHashLevelsGlobal', the stable names of
-- constant terms in a global map sending, in order to get their possible
-- proveniences (hash and levels).
--
-- Global mutable data shall be avoided, but this use is benign as each
-- term is universally valid.
insertHashLevelsGlobal :: Term 0 -> (Hash, [(Int, Integer)]) -> IO ()
insertHashLevelsGlobal = unsafeInsertInfoGlobal

lookupHashLevelsGlobal :: StableName (Term 0) ->
  IO (Maybe (Hash, [(Int, Integer)]))
lookupHashLevelsGlobal = unsafeLookupInfoGlobal

-- | Build a @GlobalInfo@, given initial @PTerm@ and already computed and
-- checked term and type (before any level substitutions).
--
-- Also inserts, at a global map, the correspondence between stable names
-- of computed terms and their provenience (hash and levels). This function
-- is still pure as these side effects cannot be detected by pure functions.
buildGlobalInfo :: Hash -> Given -> (PTerm, PTerm) -> (Term 0, Term 0) ->
  GlobalInfo
buildGlobalInfo !h !given (ptm, pty) inittmty = toGlobalInfo $ \ ls ->
  let
    res_pair = if isIdLSubs ls
      then inittmty
      else
        let
          givensubs = case given of
            (defs_pre, rs_pre) -> (defs_pre, restrsLSubs rs_pre ls)

          subsandmake :: PTerm -> Term 0
          subsandmake pt =
            case unCheckM
                (tcRes givensubs SOnlyConstr (ptermLSubs pt ls))
                ContextNil
                initLocalDefMaps of
              Left _ -> bug "buildGlobalInfo" "unexpected error"
              Right TcRes{tcMade = t} -> eliftendToTerm0 t
        in (subsandmake ptm, subsandmake pty)
  in unsafePerformIO $ do
    insertHashLevelsGlobal (fst res_pair) (h, ls)
    pure res_pair

restrsLSubs :: [ZLe ZVal] -> [(Int, Integer)] -> [ZLe ZVal]
restrsLSubs rs ls = fmap (\ (a, b) -> (f a, f b)) rs
  where
  f = (`zvalSubs` ls)

lLSubs :: (Int, Integer) -> [(Int, Integer)] -> (Int, Integer)
lLSubs (idx, coef) ls = case idx of
  0 -> (idx, coef)
  _ -> let res = (coef +) <$> (ls !! (idx - 1)) in seq (snd res) res

ptermLSubs :: PTerm -> [(Int, Integer)] -> PTerm
ptermLSubs inita ls = go inita
  where
  forceList :: [a] -> [a]
  forceList x = seq (foldr seq () x) x
  goLevel = (`zvalSubs` ls)
  go a = case a of
    PRefer{} -> a
    PUniv sl l -> PUniv sl (zvalSubs l ls)
    PPi sl b c rbn -> PPi sl (go b) (go c) rbn
    PLam sl b c rbn -> PLam sl (go b) (go c) rbn
    PApp sl b c rbn -> PApp sl (go b) (go c) rbn
    PInd sl l tele cns -> PInd sl (goLevel l) (goTele tele)
      (forceList (goCn <$> cns))
    PCn sl i b cs rbn -> PCn sl i (go b) (forceList (go <$> cs)) rbn
    PRec sl b rp rcs c rbn -> PRec sl (go b) (goRP rp)
      (forceList (goRC <$> rcs)) (go c) rbn
    PPrimUnNat{} -> a
    PPrimBbNat{} -> a
    PGlobal sl hs als -> PGlobal sl hs (forceList ((`lLSubs` ls) <$> als))
    PLet sl i b c -> PLet sl i (go b) (go c)
    PLocalDef{} -> a
  goTele (PIndSortTeleCons sl b tl rbn) =
    PIndSortTeleCons sl (go b) (goTele tl) rbn
  goTele tl@(PIndSortTeleEnd{}) = tl
  goCn (PCnSigEnd sl bs) = PCnSigEnd sl (forceList (go <$> bs))
  goCn (PCnSigNR sl b cn rbn) = PCnSigNR sl (go b) (goCn cn) rbn
  goCn (PCnSigR sl ci cn rbn) = PCnSigR sl (goCin ci) (goCn cn) rbn
  goCin (PCnInnEnd sl bs) = PCnInnEnd sl (forceList (go <$> bs))
  goCin (PCnInnPi sl b ci rbn) = PCnInnPi sl (go b) (goCin ci) rbn
  goRP (PRecPred sl i b rbns) = PRecPred sl i (go b) rbns
  goRC (PRecCaseCons sl num rc rbn1 rbn2) =
    PRecCaseCons sl num (goRC rc) rbn1 rbn2
  goRC (PRecCaseEnd sl b) = PRecCaseEnd sl (go b)

-- * Basic parsing

type P = P.ParsecT Void B.ByteString (State [BndrNm])

asciiVal :: Char -> Word8
asciiVal = toEnum . fromEnum

toAscii :: Word8 -> Char
toAscii = toEnum . fromEnum

-- No redundant left zero.
parseNatural :: P Natural
parseNatural = goI
  where
  -- Unsigned number comparation:
  -- negative numbers underflow.
  isDigit a = a - asciiVal '0' <= asciiVal '9' - asciiVal '0'
  goI = do
    a <- P.satisfy isDigit
    if a == asciiVal '0'
      then pure 0
      else goAc (fromIntegral (a - asciiVal '0') :: Natural)
  goAc !acc =
    (<|>)
      (P.try $ do
        a <- P.satisfy isDigit
        goAc (acc * 10 + fromIntegral (a - asciiVal '0')))
      (pure acc)

parseWord :: P Word
parseWord = parseNatural >>= f
  where
  f a
    | a <= fromIntegral (maxBound :: Word) = pure (fromIntegral a)
    | otherwise = fail "natural too big"

parseInt :: P Int
parseInt = parseNatural >>= f
  where
  f a
    | a <= fromIntegral (maxBound :: Int) = pure (fromIntegral a)
    | otherwise = fail "natural too big"

parseIntPos :: P Int
parseIntPos = parseInt >>= f
  where
  f a
    | a > 0 = pure a
    | otherwise = fail "not positive"

parseLevel :: P Level
parseLevel =
  fmap toAscii P.anySingle >>= \ case
    'Z' -> pure zvalZero
    'S' -> zvalAdd 1 <$> parseLevel
    '$' -> do
      _ <- P.single (asciiVal 'l')
      zvalFromVar <$> parseIntPos
    c -> fail ("(level) unexpected: " ++ [c])

parseListOf :: P a -> P [a]
parseListOf prs = P.single (asciiVal '{') *> go
  where
  go = P.try (P.single (asciiVal '}') *> pure []) <|> ((:) <$> prs <*> go)

parseSL :: P SL
parseSL = SL <$> P.getOffset

withParseSL :: (SL -> a) -> P a
withParseSL f = f <$> parseSL

parseBndrNm :: P BndrNm
parseBndrNm = lift $ state $
  \ bs -> case bs of
    [] -> (defaultBndrNm, [])
    (b : bRest) -> (b, bRest)

parseBndrNmList :: Int -> P [BndrNm]
parseBndrNmList i = lift $ state $
  \ bs -> splitAt i bs

parseRBN :: P RBN
parseRBN = RBN NoRegister <$> parseBndrNm

parseRBNList :: Int -> P [RBN]
parseRBNList i = map (RBN NoRegister) <$> parseBndrNmList i

parseTerm :: P PTerm
parseTerm = fmap toAscii P.anySingle >>= \ case
  '$' -> parseReferD
  'U' -> parseUnivD
  'Q' -> parsePiD
  'F' -> parseLamD
  'A' -> parseAppD
  'I' -> parseIndD
  'C' -> parseCnD
  'R' -> parseRecD
  'N' -> parsePrimUnOrBbNatD
  '@' -> parseGlobalD
  '|' -> parseLetD
  '&' -> parseLocalD
  c   -> fail ("(term) unexpected: " ++ [c])
  where
  parseReferD = do
    withParseSL PRefer <*> parseWord
  parseUnivD =
    withParseSL PUniv <*> parseLevel
  parsePiD =
    withParseSL PPi <*> parseTerm <*> parseTerm <*> parseRBN
  parseLamD =
    withParseSL PLam <*> parseTerm <*> parseTerm <*> parseRBN
  parseAppD =
    withParseSL PApp <*> parseTerm <*> parseTerm <*> parseRBN
  parseIndD =
    withParseSL PInd <*> parseLevel <*> parseIndSortTele <*>
      parseListOf parseCnSig
  parseCnD =
    withParseSL PCn <*> parseInt <*> parseTerm <*>
      parseListOf parseTerm <*> parseRBN
  parseRecD =
    withParseSL PRec <*> parseTerm <*> parseRecPred <*>
      parseListOf parseRecCase <*> parseTerm <*>
      parseRBN
  parsePrimUnOrBbNatD = fmap toAscii P.anySingle >>= \ case
    'u' -> withParseSL PPrimUnNat <*> parseNatural
    'b' -> withParseSL PPrimBbNat <*> parseNatural
    _ -> empty
  parseGlobalD = withParseSL PGlobal <*> parseHash <*> parseGlobalArgs
  parseLetD = withParseSL PLet <*> parseInt <*> parseTerm <*> parseTerm
  parseLocalD = withParseSL PLocalDef <*> parseInt

  parseIndSortTele = P.single (asciiVal '{') *> go
    where
    go =
      (<|>)
        (P.try $ P.single (asciiVal '}') *> withParseSL PIndSortTeleEnd)
        (withParseSL PIndSortTeleCons <*> parseTerm <*> go <*>
          parseRBN)
  parseCnSig = (<|>)
    (P.try $ P.single (asciiVal 'I') *> fmap toAscii P.anySingle >>= \ case
      'n' -> withParseSL PCnSigNR <*> parseTerm <*>
        parseCnSig <*> parseRBN
      'r' -> withParseSL PCnSigR <*> parseCnInn <*>
        parseCnSig <*> parseRBN
      _ -> empty)
    (withParseSL PCnSigEnd <*> parseListOf parseTerm)
  parseCnInn = (<|>)
    (P.try $ P.single (asciiVal 'I') *> P.single (asciiVal 'p') *>
      (withParseSL PCnInnPi <*> parseTerm <*> parseCnInn <*> parseRBN))
    (withParseSL PCnInnEnd <*> parseListOf parseTerm)
  parseRecPred = P.single (asciiVal 'R') *> P.single (asciiVal 'p') *> do
    sl <- parseSL
    i <- parseInt
    pt <- parseTerm
    rbns <- parseRBNList (i + 1)
    pure $ PRecPred sl i pt rbns
  parseRecCase = P.single (asciiVal 'R') *> P.single (asciiVal 'c') *> go
    where
    go =
      (P.try $ P.single (asciiVal '1') *>
        (withParseSL PRecCaseCons <*> pure PRecCase1 <*> go <*>
          parseRBN <*> parseRBN))
      <|>
      (P.try $ P.single (asciiVal '2') *>
        (withParseSL PRecCaseCons <*> pure PRecCase2 <*> go <*>
          parseRBN <*> parseRBN))
      <|>
      (withParseSL PRecCaseEnd <*> parseTerm)

  parseHash = (base64ToHash . B.pack) <$>
    replicateM 16 (P.satisfy isB64Char)
    where
    isB64Char a = a == asciiVal '-' || a == asciiVal '_' ||
      a - asciiVal '0' <= asciiVal '9' - asciiVal '0' ||
      a - asciiVal 'a' <= asciiVal 'z' - asciiVal 'a' ||
      a - asciiVal 'A' <= asciiVal 'Z' - asciiVal 'A'
  parseGlobalArgs = parseListOf go
    where
    go = fmap toAscii P.anySingle >>= \ case
      'Z' -> pure (0, 0)
      'S' -> (\ (i, !c) -> (i, c + 1)) <$> go
      '$' -> P.single (asciiVal 'l') *> ((\ i -> (i, 0)) <$> parseIntPos)
      _ -> empty

-- | BLAKE2b256, truncated to 96 bits (= 12 bytes).
blake2b256Trunc :: B.ByteString -> Hash
blake2b256Trunc str = byteStringToHash $ B.take 12 $
  BA.convert (CH.hash str :: CH.Digest CH.Blake2b_256)

parseRs :: P [ZLe Level]
parseRs = parseListOf ((,) <$> parseLevel <*> parseLevel)

-- | Break text in lines, each one preceded by '\n' (but the result
-- will not contain such character).
breakLinesPosix :: B.ByteString -> Maybe [B.ByteString]
breakLinesPosix b
  | B.null b = Just []
  | (hb, tb) <- B.break ((==) (asciiVal '\n')) b = case B.uncons tb of
    Nothing -> Nothing -- Not ending in '\n'.
    Just (_, tb') -> (hb :) <$> breakLinesPosix tb'

-- | Remove "D " prefix.
stripDeclPrefix :: B.ByteString -> Maybe B.ByteString
stripDeclPrefix bs
  | Just ((==) (asciiVal 'D') -> True, bs1) <- B.uncons bs
  , Just ((==) (asciiVal ' ') -> True, bs2) <- B.uncons bs1 = Just bs2
  | otherwise = Nothing

-- | Break a line in "RESTRS TYPE DEFINIENS", where the three components
-- have no space.
breakDeclLine :: B.ByteString ->
  Maybe (B.ByteString, B.ByteString, B.ByteString)
breakDeclLine = \ b -> do
  (brs, b2) <- breakAtSpace b
  (bty, b3) <- breakAtSpace b2
  if B.elem (asciiVal ' ') b3
    then Nothing
    else pure (brs, bty, b3)
  where
  breakAtSpace b = case B.break ((==) (asciiVal ' ')) b of
    (bh, btsp) -> case B.uncons btsp of
      Nothing -> Nothing
      Just (_, bt) -> Just (bh, bt)

-- * Term validation

-- XXX INCOMPLETE

zvalMaxLevelNum :: Level -> Int
zvalMaxLevelNum l = maximum (0 : I.keys l)

ptermMaxLevelNum :: PTerm -> Int
ptermMaxLevelNum = go
  where
  maxi :: (a -> Int) -> [a] -> Int
  maxi f ls = maximum (0 : map f ls)
  go = \ case
    PRefer{} -> 0
    PUniv _ l -> zvalMaxLevelNum l
    PPi _ a b _ -> go a `max` go b
    PLam _ a b _ -> go a `max` go b
    PApp _ a b _ -> go a `max` go b
    PInd _ l tl cns -> zvalMaxLevelNum l `max` goTele tl `max`
      maxi goCn cns
    PCn _ _ a as _ -> go a `max` maxi go as
    PRec _ a rp rcs b _ -> go a `max` goRP rp `max`
      maxi goRC rcs `max` go b
    PPrimUnNat{} -> 0
    PPrimBbNat{} -> 0
    PGlobal _ _ ls -> maxi fst ls
    PLet _ _ a b -> go a `max` go b
    PLocalDef{} -> 0
  goTele (PIndSortTeleCons _ a b _) = go a `max` goTele b
  goTele PIndSortTeleEnd{} = 0
  goCn (PCnSigEnd _ as) = maxi go as
  goCn (PCnSigNR _ a b _) = go a `max` goCn b
  goCn (PCnSigR _ ci b _) = goCi ci `max` goCn b
  goCi (PCnInnEnd _ as) = maxi go as
  goCi (PCnInnPi _ a b _) = go a `max` goCi b
  goRP (PRecPred _ _ a _) = go a
  goRC (PRecCaseCons _ _ r _ _) = goRC r
  goRC (PRecCaseEnd _ a) = go a

rsMaxLevelNum :: [ZLe Level] -> Int
rsMaxLevelNum ls = maximum (0 : map (\ (i, j) -> go i `max` go j) ls)
  where
  go = zvalMaxLevelNum

-- * Example checker

mapLeft :: (a -> b) -> Either a c -> Either b c
mapLeft f (Left a) = Left (f a)
mapLeft _ (Right r) = Right r

checkByteString :: B.ByteString -> Either String ()
checkByteString = \ bs -> do
  lns <- maybe (Left "Not POSIX text file.") Right (breakLinesPosix bs)
  case lns of
    [] -> Left "Lacking header."
    ((==) (B8.pack "#VERI 1") -> False) : _ -> Left "Wrong header."
    _ : dlns -> do
      dlnsWoPrefix <- maybe (Left "Some line not prefixed by \"D \".") Right
        (traverse stripDeclPrefix dlns)
      let hashs = map blake2b256Trunc dlnsWoPrefix
      substrs <- maybe (Left "Some line not containing exactly 3 spaces.")
        Right (traverse breakDeclLine dlnsWoPrefix)
      go 0 mempty (zip hashs substrs)
  where
  go :: Int -> Globals ->
    [(Hash, (B.ByteString, B.ByteString, B.ByteString))] ->
    Either String ()
  go !_linenumber !_globals [] = Right ()
  go !linenumber !globals ((hash, (brs, bty, btm)) : restToAnalyse) = do
    let
      runPR m = case runState m of
        f -> mapLeft P.errorBundlePretty (fst $ f [])
    rs <- runPR $ P.runParserT (parseRs <* P.eof) "" brs
    pty <- runPR $ P.runParserT (parseTerm <* P.eof) "" bty
    ptm <- runPR $ P.runParserT (parseTerm <* P.eof) "" btm
    let prepln = (("Line " ++ show linenumber ++ ": ") ++)
    if isPossibleN rs then pure () else
      Left (prepln "Impossible restrictions.")
    let !numlevelvars = rsMaxLevelNum rs `max` ptermMaxLevelNum pty `max`
          ptermMaxLevelNum ptm
    let chk p = unCheckM (tcRes (globals, rs) SConstrAndCheck p)
                  ContextNil initLocalDefMaps
    case chk pty of
      Left e -> Left (prepln ("At type: " ++ show e))
      Right TcRes{tcMade = el_ty, tcChecked = el_ty_ty}
        | Nothing <- elLevelOfUniverse el_ty_ty ->
          Left (prepln "Not a type.")
        | otherwise -> case chk ptm of
          Left e -> Left (prepln ("At definiens: " ++ show e))
          Right TcRes{tcMade = el_tm, tcChecked = el_ty_tm}
            | not (elSubsumedBy rs el_ty_tm el_ty) ->
              Left (prepln "Wrong type.")
            | otherwise -> do
              let
                gi = buildGlobalInfo hash (globals, rs) (ptm, pty)
                  (eliftendToTerm0 el_tm, eliftendToTerm0 el_ty)
                g = (numlevelvars, rs, gi)
                new_globals = M.insert hash g globals
              trace ("note: " ++ prepln (show $ hashToBase64 hash)) $
                go (linenumber + 1) new_globals restToAnalyse

checkString :: String -> Either String ()
checkString = checkByteString . B8.pack

checkFile :: FilePath -> IO (Either String ())
checkFile = (checkByteString <$>) . B.readFile
