{-|
Module      : Veri.UnsafeGlobalDefToHashLevels
Description : A global mutable variable
Copyright   : © 2022 Yuri da Silva
License     : GPL-3.0-or-later
Maintainer  : {{{email|yrdslv|protonmail.com}}}
Stability   : experimental

Following "System.IO.Unsafe", global mutable variables shall be defined
separately.

This is very unsafe as type mismatch is possible.
-}

{-# OPTIONS_GHC -fno-cse #-}
{-# LANGUAGE Unsafe #-}

module Veri.UnsafeGlobalDefToHashLevels
  ( unsafeInsertInfoGlobal
  , unsafeLookupInfoGlobal
  , unsafeGlobalUnspec
  ) where

import qualified Data.HashMap.Strict as H
import Data.IORef (IORef, newIORef, atomicModifyIORef', readIORef)
import System.IO.Unsafe (unsafePerformIO)
import System.Mem.StableName (StableName, makeStableName)

-- | This global mutable map will be used for mapping stable names of
-- definitions to their proveniences: hash and level variables.
--
-- This is considered append-only. In the future we may consider
-- deleting members.
unsafeGlobalUnspec :: IORef (H.HashMap (StableName t) info)
unsafeGlobalUnspec = unsafePerformIO $ newIORef H.empty
{-# NOINLINE unsafeGlobalUnspec #-}

unsafeInsertInfoGlobal :: t -> info -> IO ()
unsafeInsertInfoGlobal t info = do
  sn <- makeStableName $! t
  atomicModifyIORef' unsafeGlobalUnspec $ \ hm ->
    (H.insert sn info hm, ())

unsafeLookupInfoGlobal :: StableName t -> IO (Maybe info)
unsafeLookupInfoGlobal sn = do
  hm <- readIORef unsafeGlobalUnspec
  pure $ H.lookup sn hm

