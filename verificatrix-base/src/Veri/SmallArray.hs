{-|
Module      : Veri.SmallArray
Description : Safe small arrays.
Copyright   : © 2021 Yuri da Silva
License     : GPL-3.0-or-later
Maintainer  : {{{email|yrdslv|protonmail.com}}}
Stability   : experimental

Safe wrappers for operators in "Data.Primitive.SmallArray". No segmentation
faults.

__Warning__: Mutable Array resizing proposal
(<https://github.com/ghc-proposals/ghc-proposals/pull/121>) breaks the safety
of operations, as length can change after bounds checking. So, do not resize
mutable arrays.

__Warning 2__: The type @SmallArray@ is the same of package 'primitive', and
some of its instances (such as @Applicative@) are unsafe (due to lack of
overflow checking).
-}

{-# LANGUAGE Trustworthy #-}

module Veri.SmallArray
  ( SmallArray
  , SmallMutableArray
  , newSmallArray
  , maybeReadSmallArray
  , readSmallArray
  , writeSmallArray
  , copySmallArray
  , copySmallMutableArray
  , maybeIndexSmallArray
  , indexSmallArray
  , indexSmallArrayM
  , cloneSmallArray
  , cloneSmallMutableArray
  , freezeSmallArray
  , thawSmallArray
  , Unsafe.runSmallArray
  , sizeofSmallArray
  , sizeofSmallMutableArray
  , Unsafe.smallArrayFromList
  , Unsafe.smallArrayFromListN
  , Unsafe.mapSmallArray'
  , Unsafe.traverseSmallArrayP
  , createSmallArray
  , createSmallArray'
  ) where

{-
In GHC there are two primitive types storing consecutive generic elements:
@Array#@ and @SmallArray#@. From GHC.Prim (paraphrased):

A @SmallArray#@ works just like an @Array#@, but with different space use and
performance characteristics. The @SmallArray#@ (…) lack a “card table”, whose
purpose is to avoid having to scan every element of the array on each garbage
collection (GC) by keeping track of which elements have changed since tha last
GC and only scanning those that have changed. So, without a card table, the
array uses less memory and writes are somewhat faster, but the whole array has
to be scanned on each GC.

We mostly use arrays with sizes at most three, so @SmallArray#@ is a better
option.

However, @SmallArray#@ is less used, so we have to write some bureaucracy to
use it.
-}

import Control.Monad.Primitive (PrimMonad, PrimState)
import qualified Data.Primitive.SmallArray as Unsafe
import Data.Primitive.SmallArray (SmallArray, SmallMutableArray,
  sizeofSmallArray, sizeofSmallMutableArray)

{-
Most of operations of @Data.Primitive.SmallArray@ do not check bounds
and can easily corrupt memory. So we must write some decent wrappers.
-}


outOfBounds :: String -> a
outOfBounds str = error (str ++ ": out of bounds.")

-- | Create a new small mutable array. Throws if size is negative.
newSmallArray
  :: PrimMonad m
  => Int -- ^ size
  -> a   -- ^ initial contents
  -> m (SmallMutableArray (PrimState m) a)
newSmallArray i x
  | i < 0     = error "newSmallArray: negative size."
  | otherwise = Unsafe.newSmallArray i x

-- | Read the element at a given index in a mutable array; returns @Nothing@
-- if index is out of bounds.
maybeReadSmallArray
  :: PrimMonad m
  => SmallMutableArray (PrimState m) a -- ^ array
  -> Int                               -- ^ index
  -> m (Maybe a)
maybeReadSmallArray !sma i
  | 0 <= i && i < sizeofSmallMutableArray sma =
    Just <$> Unsafe.readSmallArray sma i
maybeReadSmallArray _ _ = pure Nothing

-- | Read the element at a given index in a mutable array; throws if index
-- is out of bounds.
readSmallArray
  :: PrimMonad m
  => SmallMutableArray (PrimState m) a -- ^ array
  -> Int                               -- ^ index
  -> m a
readSmallArray !sma i = unJust <$> maybeReadSmallArray sma i
  where
  unJust (Just a) = a
  unJust _ = outOfBounds "readSmallArray"

-- | Write an element at the given index in a mutable array; throws if index
-- is out of bounds.
writeSmallArray
  :: PrimMonad m
  => SmallMutableArray (PrimState m) a -- ^ array
  -> Int                               -- ^ index
  -> a                                 -- ^ new element
  -> m ()
writeSmallArray !sma i
  | 0 <= i && i < sizeofSmallMutableArray sma =
    Unsafe.writeSmallArray sma i
writeSmallArray _ _ = outOfBounds "writeSmallArray"

-- | Look up an element in an immutable array. Returns in an applicative so
-- that the indexing can be done without evaluating the element. Throws if
-- index is out of bounds.
indexSmallArrayM
  :: Applicative m
  => SmallArray a -- ^ array
  -> Int          -- ^ index
  -> m a
indexSmallArrayM !sa i
  | 0 <= i && i < sizeofSmallArray sa =
    case Unsafe.indexSmallArrayM sa i of
      ((), a) -> pure a
  | otherwise = outOfBounds "indexSmallArrayM"

-- | Look up an element in an immutable array. Returns @Nothing@ if index is
-- out of bounds.
maybeIndexSmallArray
  :: SmallArray a -- ^ array
  -> Int          -- ^ index
  -> Maybe a
maybeIndexSmallArray !sa i
  | 0 <= i && i < sizeofSmallArray sa =
    Unsafe.indexSmallArrayM sa i
  | otherwise = Nothing

-- | Look up an element in an immutable array. Throws if index if out of
-- bounds.
indexSmallArray
  :: SmallArray a -- ^ array
  -> Int          -- ^ index
  -> a
indexSmallArray !sa i = unJust (maybeIndexSmallArray sa i)
  where
  unJust (Just a) = a
  unJust _ = outOfBounds "indexSmallArray"

-- | Creates a copy of a slice of an immutable array. Throws if range is
-- out of bounds.
cloneSmallArray
  :: SmallArray a -- ^ source
  -> Int          -- ^ offset
  -> Int          -- ^ length
  -> SmallArray a
cloneSmallArray !sa !o !l
  | 0 <= o && 0 <= l && l <= (sizeofSmallArray sa - o) = -- Beware overflow.
    Unsafe.cloneSmallArray sa o l
  | otherwise = outOfBounds "cloneSmallArray"

-- | Creates a copy of a slice of a mutable array. Throws if range is out of
-- bounds.
cloneSmallMutableArray
  :: PrimMonad m
  => SmallMutableArray (PrimState m) a -- ^ source
  -> Int                               -- ^ offset
  -> Int                               -- ^ length
  -> m (SmallMutableArray (PrimState m) a)
cloneSmallMutableArray !sma !o !l
  | 0 <= o && 0 <= l && l <= (sizeofSmallMutableArray sma - o) =
    Unsafe.cloneSmallMutableArray sma o l
  | otherwise = outOfBounds "cloneSmallMutableArray"

-- | Creates an immutable array corresponding to a slice of a mutable array,
-- by copying. Throws if range is out of bounds.
freezeSmallArray
  :: PrimMonad m
  => SmallMutableArray (PrimState m) a -- ^ source
  -> Int                               -- ^ offset
  -> Int                               -- ^ length
  -> m (SmallArray a)
freezeSmallArray !sma !o !l
  | 0 <= o && 0 <= l && l <= (sizeofSmallMutableArray sma - o) =
    Unsafe.freezeSmallArray sma o l
  | otherwise = outOfBounds "freezeSmallArray"

-- | Creates a mutable array corresponding to a slice of an immutable array,
-- by copying. Throws if range is out of bounds.
thawSmallArray
  :: PrimMonad m
  => SmallArray a -- ^ source
  -> Int          -- ^ offset
  -> Int          -- ^ length
  -> m (SmallMutableArray (PrimState m) a)
thawSmallArray !sa !o !l
  | 0 <= o && 0 <= l && l <= (sizeofSmallArray sa - o) =
    Unsafe.thawSmallArray sa o l
  | otherwise = outOfBounds "thawSmallArray"

-- | Copy a slice of an immutable array into a mutable array. Throws if range
-- is out of bounds.
copySmallArray
  :: PrimMonad m
  => SmallMutableArray (PrimState m) a -- ^ destination
  -> Int                               -- ^ destination offset
  -> SmallArray a                      -- ^ source
  -> Int                               -- ^ source offset
  -> Int                               -- ^ length
  -> m ()
copySmallArray !dst !dof !src !sof !l
  | 0 <= sof && 0 <= l && l <= (sizeofSmallArray src - sof)
  , 0 <= dof && l <= (sizeofSmallMutableArray dst - dof) =
    Unsafe.copySmallArray dst dof src sof l
  | otherwise = outOfBounds "copySmallArray"

-- | Copy a slice of an mutable array into another. Throws if range is out
-- of bounds.
copySmallMutableArray
  :: PrimMonad m
  => SmallMutableArray (PrimState m) a -- ^ destination
  -> Int                               -- ^ destination offset
  -> SmallMutableArray (PrimState m) a -- ^ source
  -> Int                               -- ^ source offset
  -> Int                               -- ^ length
  -> m ()
copySmallMutableArray !dst !dof !src !sof !l
  | 0 <= sof && 0 <= l && l <= (sizeofSmallMutableArray src - sof)
  , 0 <= dof && l <= (sizeofSmallMutableArray dst - dof) =
    Unsafe.copySmallMutableArray dst dof src sof l
  | otherwise = outOfBounds "copySmallMutableArray"

-- | Create a SmallArray of specified length and elements.
-- Function is lazily applied.
createSmallArray ::
  Int ->        -- ^ size
  (Int -> a) -> -- ^ elements
  SmallArray a
createSmallArray l _
  | l < 0 = error "createSmallArray: negative size."
createSmallArray l f = Unsafe.runSmallArray $ do
  sma <- Unsafe.newSmallArray l (f 0)
  flip mapM_ [1 .. (l - 1 :: Int)] $ \ j ->
    Unsafe.writeSmallArray sma j (f j)
  pure sma

-- | Like @createSmallArray@, but evaluating the elements
-- before storing.
createSmallArray' ::
  Int ->        -- ^ size
  (Int -> a) -> -- ^ elements
  SmallArray a
createSmallArray' l _
  | l < 0 = error "createSmallArray': negative size."
createSmallArray' l f = Unsafe.runSmallArray $ do
  -- Can't evaluate the zeroth element if it does not exist.
  sma <- Unsafe.newSmallArray l (error "createSmallArray: impossible.")
  flip mapM_ [0 .. (l - 1 :: Int)] $ \ j ->
    Unsafe.writeSmallArray sma j $! f j
  pure sma
