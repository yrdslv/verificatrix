{-|
Module      : Veri.Dict.KnownWord
Description : Manipulations of 'KnownWord' constraints
Copyright   : © 2021 Yuri da Silva
License     : GPL-3.0-or-later
Maintainer  : {{{email|yrdslv|protonmail.com}}}
Stability   : experimental

Some axioms for Presburger Arithmetic (not comprehensive, because Presburger
Arithmetic is not finitely axiomatizable).
-}

{-# LANGUAGE CPP #-}

{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE MagicHash #-}

{-# LANGUAGE Trustworthy #-}

module Veri.Dict.KnownWord
  ( --
    -- * Axioms
    --
    plusKnown
  , minusKnown
  , CmpOpt (CmpLt, CmpEq, CmpGt)
  , compareKnown
  , eqKnown
  , plusAss
  , plusComm
  , plusNeur
  , rightPlusMinus
  , rightMinusPlus
  , rightPlusExpans
  , leZero
  , negLe
    --
    -- * Theorems
    --
  , plusNeul
  , rightPlusComm
  , leftPlusComm
  , rightPlusInj
  , leftPlusInj
  , rightMinusLeftPlus
  , leftPlusRightMinus
  , rightMinusInj
  , rightMinusPlusComm
  , leRefl
  , minusRefl
  , leTrans
  , leRightPlus
  , leLeftPlus
  , leRightMinus
  , leftPlusExpans
  , coleRightPlus
  , leAsym
  , leWeakRightPlus
  , leWeakLeftPlus
  , leCoweakRightPlus
  , leCoweakLeftPlus
  , leftPlusRightMinusComm
  , rightPlusLeRightMinus
  , leftPlusLeRightMinus
  , rightPlusColeRightMinus
  , leftPlusColeRightMinus
  , complLe
  , complMinus
  , complCompl
  , leMinusSwap
  , rightPlusLeSame
  , rightPlusBothMinus
  , rightPlusComplRightPlus
  , complPlus
  , minusTrans
  , leCompl
  , coleCompl
  , rightMinusComm
  , complMinusCompl
  , ltOrGeKnown
  , leOrGtKnown
  , zeroOrPosKnown
  , rightPlusMinusKnown
  , leftPlusMinusKnown
  --
  -- * Unboxed things
  --
  , plusK#
  , minusK#
  , compareK#
  , eqK#
  , ltOrGeK#
  , leOrGtK#
  , zeroOrPosK#
  , rightPlusMinusK#
  , leftPlusMinusK#
  ) where

{-
How we add @KnownWord@ dictionaries? How we subtract them? How we express
that @m - n + n = m@ if @n <= m@? In this file, we axiomatize some of these
operations and derive others.

Many mistakes are eliminated; I admit that it is a bit annoying to prove
basic affirmations of arithmetic; we could axiomatize everything, but that
would be more possibilities for mistakes.

I considered using a GHC plugin for automatically proving the relations, but
the ones I tested were very buggy.
-}

{-

Note __unsafeCoerce bug__:

Because of bug <https://gitlab.haskell.org/ghc/ghc/-/issues/16893> (affecting
GHC 8.8 and before), it is risky to mix axioms and case expressions. Consider
the example:

> op w w' = if w < w'
>   then Left axiomA
>   else Right axiomB

GHC comes across the code:

> case op w w' of
>   Left Dict -> some1 thing
>   Right Dict -> some2 thing

GHC sees that code as (where @|>@ means substitution of local equality):

> case op w w' of
>   Left (Dict (Eq# e1)) -> some1 (thing |> e1)
>   Right (Dict (Eq# e2)) -> some2 (thing |> e2)

Expanding @op@:

> if w < w'
>   then some1 (thing |> axiomA#)
>   else some2 (thing |> axiomB#)

Suppose that @thing@ is strict in both cases. GHC then speculates it:

> case thing |> axiomA# of
>   !x -> if w < w'
>     then some1 x
>     else some2 (x |> (~ axiomA# ; axiomB#))

Now the program can crash, because the axiom @axiomA#@ is used before
determining if it holds.

Solution is to put NOINLINE in @op@, or preferentially in a smaller operation,
so that the performance impact is not so big.
-}

import Data.Type.Equality (type (:~:) (..))
import GHC.Exts (Word (..), Word#)
import Unsafe.Coerce (unsafeCoerce)

import Veri.Dict
import Veri.KnownWord

-- GHC automatically solves few basic theorems, such as @0 + m = m@. But
-- which theorems depend on the version of GHC; so some versions may accept
-- a proof and other versions may reject it; testing many versions is
-- recommended.

{-|
Get @KnownWord@ of sum, checking if there is overflow. (Checking is necessary
to avoid unsafety, as words module 2^64 do not satisfy the rules of
arithmetic.)
-}
plusKnown ::
  forall m n. (KnownWord m, KnownWord n) => Dict (KnownWord (m + n))
plusKnown =
  let
    mn = wordVal_ @m + wordVal_ @n
    res = if mn >= wordVal_ @m then mn else error "plusKnown: overflow."
  in case unsafeCoerce res :: WordDict (m + n) of
    WordDict -> Dict

-- Evaluate equality evidence, so that @-fdefer-type-errors, ...@ do not
-- cause unsafety.
forceEq :: forall c a b. (c ~ (a ~ b), c) => ()
forceEq = seq (withDictEq (Dict :: Dict (a ~ b))) ()

{-|
Get @KnownWord@ of difference.
-}
minusKnown ::
  forall m n. (KnownWord m, KnownWord n, n <= m) => Dict (KnownWord (m - n))
minusKnown
  | let res = wordVal_ @m - wordVal_ @n, () <- forceEq @(n <= m) =
    case unsafeCoerce res :: WordDict (m - n) of
      WordDict -> Dict

{-|
Comparison options for two naturals.
-}
data CmpOpt m n =
  CmpLtW {-# UNPACK #-} !((m + 1 <=? n) :~: 'True)
  | CmpEqW {-# UNPACK #-} !(m :~: n)
  | CmpGtW {-# UNPACK #-} !((n + 1 <=? m) :~: 'True)
-- UNPACK trick to get equalities which do not use memory.

pattern ReflFromDict :: Dict (a ~ b) -> a :~: b
pattern ReflFromDict a <- ((\ Refl -> Dict) -> a) where
  ReflFromDict Dict = Refl
{-# COMPLETE ReflFromDict #-}

pattern CmpLt :: Dict (m + 1 <= n) -> CmpOpt m n
pattern CmpLt a = CmpLtW (ReflFromDict a)

pattern CmpEq :: Dict (m ~ n) -> CmpOpt m n
pattern CmpEq a = CmpEqW (ReflFromDict a)

pattern CmpGt :: Dict (n + 1 <= m) -> CmpOpt m n
pattern CmpGt a = CmpGtW (ReflFromDict a)

{-# COMPLETE CmpLt, CmpEq, CmpGt #-}

compUnsafe :: Word# -> Word# -> CmpOpt m n
compUnsafe m# n# = case compare (W# m#) (W# n#) of
  LT -> CmpLt axiom
  EQ -> CmpEq axiom
  GT -> CmpGt axiom
-- As arguments of @CmpLt, ...@ have zero-width, no allocation is
-- performed.
#if __GLASGOW_HASKELL__ <= 808
{-# NOINLINE compUnsafe #-} -- See Note __unsafeCoerce bug__.
#endif

compareKnown :: forall m n. (KnownWord m, KnownWord n) => CmpOpt m n
compareKnown = case wordVal_ @m of
  W# m# -> case wordVal_ @n of
    W# n# -> compUnsafe m# n#

eqKnown :: forall m n. (KnownWord m, KnownWord n) => Maybe (Dict (m ~ n))
eqKnown = case compareKnown @m @n of
  CmpEq Dict -> Just Dict
  _ -> Nothing


axiom :: forall a b. Dict (a ~ b)
axiom = unsafeCoerce (Dict :: Dict (a ~ a))

plusAss :: forall m n o. Dict (((m + n) + o) ~ (m + (n + o)))
plusAss = axiom

plusComm :: forall m n. Dict ((m + n) ~ (n + m))
plusComm = axiom

plusNeur :: forall m. Dict ((m + 0) ~ m)
plusNeur = axiom

rightPlusMinus :: forall m n. Dict (((m + n) - n) ~ m)
rightPlusMinus = axiom

rightMinusPlus :: forall m n. (n <= m) => Dict (((m - n) + n) ~ m)
rightMinusPlus = forceEq @(n <= m) `seq` axiom

rightPlusExpans :: forall m n. Dict (m <= m + n)
rightPlusExpans = axiom

leZero :: forall m. (m <= 0) => Dict (m ~ 0)
leZero = forceEq @(m <= 0) `seq` axiom

negLe :: forall m n. (m + 1 <= n) => Dict ((n <=? m) ~ 'False)
negLe = forceEq @(m + 1 <= n) `seq` axiom

-- Consequences:

-- Make Core output briefer. Oops, better not; see Note __unsafeCoerce bug__.
eraseEq :: Dict (a ~ b) -> Dict (a ~ b)
-- eraseEq !(withDictEq -> _) = axiom
eraseEq = id

dict :: (a ~ b) => Dict (a ~ b)
dict = Dict

{-
If two proofs are wrongly mutually recursive, we would get only non
termination, not unsafety.
-}

plusNeul :: forall m. Dict ((0 + m) ~ m)
plusNeul = eraseEq $
  withDict (plusNeur @m) $
  withDict (plusComm @m @0) $
  dict

rightPlusComm :: forall m n o. Dict ((m + n + o) ~ (m + o + n))
rightPlusComm = eraseEq $
  withDict (plusAss @m @o @n) $
  withDict (plusComm @n @o) $
  withDict (plusAss @m @n @o) $
  dict

leftPlusComm :: forall m n o. Dict ((m + (n + o)) ~ (n + (m + o)))
leftPlusComm = eraseEq $
  withDict (plusAss @n @m @o) $
  withDict (plusComm @m @n) $
  withDict (plusAss @m @n @o) $
  dict

rightPlusInj :: forall m n o. ((m + o) ~ (n + o)) => Dict (m ~ n)
rightPlusInj = eraseEq $
  withDict (rightPlusMinus @n @o) $
  withDict (rightPlusMinus @m @o) $
  dict

leftPlusInj :: forall m n o. ((m + n) ~ (m + o)) => Dict (n ~ o)
leftPlusInj = eraseEq $
  withDict (plusComm @m @n) $
  withDict (plusComm @m @o) $
  withDict (rightPlusInj @n @o @m) $
  dict

rightMinusLeftPlus :: forall m n. (m <= n) => Dict ((m + (n - m)) ~ n)
rightMinusLeftPlus = eraseEq $
  withDict (plusComm @m @(n - m)) $
  withDict (rightMinusPlus @n @m) $
  dict

leftPlusRightMinus :: forall m n. Dict (((m + n) - m) ~ n)
leftPlusRightMinus = eraseEq $
  withDict (plusComm @m @n) $
  withDict (rightPlusMinus @n @m) $
  dict

rightMinusInj :: forall m n o.
  (o <= m, o <= n, (m - o) ~ (n - o)) => Dict (m ~ n)
rightMinusInj = eraseEq $
  withDict (rightMinusPlus @m @o) $
  withDict (rightMinusPlus @n @o) $
  withDict (rightPlusInj @(m - o) @(n - o) @o) $
  dict

rightMinusPlusComm :: forall m n o.
  (n <= m) => Dict ((m - n + o) ~ (m + o - n))
rightMinusPlusComm = eraseEq $
  withDict (rightPlusComm @(m - n) @o @n) $
  withDict (rightMinusPlus @m @n) $
  withDict (rightPlusMinus @(m - n + o) @n) $
  dict

leRefl :: forall m. Dict (m <= m)
leRefl = eraseEq $
  withDict (plusNeur @m) $
  withDict (rightPlusExpans @m @0) $
  dict

minusRefl :: forall m. Dict ((m - m) ~ 0)
minusRefl = eraseEq $
  withDict (leRefl @m) $
  withDict (rightMinusPlus @m @m) $
  withDict (plusNeul @m) $
  withDict (rightPlusInj @(m - m) @0 @m) $
  dict

leTrans :: forall m n o. (m <= n, n <= o) => Dict (m <= o)
leTrans = eraseEq $
  withDict (rightMinusPlus @n @m) $
  withDict (rightMinusPlus @o @n) $
  withDict (plusAss @m @(n - m) @(o - n)) $
  withDict (rightMinusLeftPlus @m @n) $
  withDict (rightMinusLeftPlus @n @o) $
  withDict (rightPlusExpans @m @((n - m) + (o - n))) $
  dict

leRightPlus :: forall m n o. (m <= n) => Dict (m + o <= n + o)
leRightPlus = eraseEq $
  withDict (rightPlusComm @m @o @(n - m)) $
  withDict (rightMinusLeftPlus @m @n) $
  withDict (rightPlusExpans @(m + o) @(n - m)) $
  dict

leLeftPlus :: forall m n o. (n <= o) => Dict (m + n <= m + o)
leLeftPlus = eraseEq $
  withDict (leRightPlus @n @o @m) $
  withDict (plusComm @m @n) $
  withDict (plusComm @m @o) $
  dict

leRightMinus :: forall m n o.
  (m <= n, o <= m) => Dict (m - o <= n - o)
leRightMinus = eraseEq $
  withDict (rightMinusPlusComm @m @o @(n - m)) $
  withDict (rightMinusLeftPlus @m @n) $
  withDict (rightPlusExpans @(m - o) @(n - m)) $
  dict

leftPlusExpans :: forall m n. Dict (m <= n + m)
leftPlusExpans = eraseEq $
  withDict (rightPlusExpans @m @n) $
  withDict (plusComm @m @n) $
  dict

coleRightPlus :: forall m n o. (m + o <= n + o) => Dict (m <= n)
coleRightPlus = eraseEq $
  withDict (leftPlusExpans @o @m) $
  withDict (leftPlusExpans @o @n) $
  withDict (leRightMinus @(m + o) @(n + o) @o) $
  withDict (rightPlusMinus @m @o) $
  withDict (rightPlusMinus @n @o) $
  dict

leAsym :: forall m n. (m <= n, n <= m) => Dict (m ~ n)
leAsym = eraseEq $
  withDict (minusRefl @n) $
  withDict (leRightMinus @m @n @n) $
  withDict (leZero @(m - n)) $
  withDict (rightMinusPlus @m @n) $
  withDict (plusNeul @n) $
  dict

leWeakRightPlus :: forall m n o. (m <= n) => Dict (m <= n + o)
leWeakRightPlus = eraseEq $
  withDict (rightPlusExpans @n @o) $
  withDict (leTrans @m @n @(n + o)) $
  dict

leWeakLeftPlus :: forall m n o. (m <= n) => Dict (m <= o + n)
leWeakLeftPlus = eraseEq $
  withDict (leWeakRightPlus @m @n @o) $
  withDict (plusComm @n @o) $
  dict

leCoweakRightPlus :: forall m n o. (m + n <= o) => Dict (m <= o)
leCoweakRightPlus = eraseEq $
  withDict (rightPlusExpans @m @n) $
  withDict (leTrans @m @(m + n) @o) $
  dict

leCoweakLeftPlus :: forall m n o. (m + n <= o) => Dict (n <= o)
leCoweakLeftPlus = eraseEq $
  withDict (plusComm @m @n) $
  withDict (leCoweakRightPlus @n @m @o) $
  dict

leftPlusRightMinusComm :: forall m n o.
  (o <= n) => Dict ((m + (n - o)) ~ (m + n - o))
leftPlusRightMinusComm = eraseEq $
  withDict (leWeakLeftPlus @o @n @m) $
  withDict (plusAss @m @(n - o) @o) $
  withDict (rightMinusPlus @n @o) $
  withDict (rightMinusPlus @(m + n) @o) $
  withDict (rightPlusInj @(m + (n - o)) @(m + n - o) @o) $
  dict

rightPlusLeRightMinus:: forall m n o. (m + n <= o) => Dict (m <= o - n)
rightPlusLeRightMinus = eraseEq $
  withDict (leftPlusExpans @n @m) $
  withDict (leCoweakLeftPlus @m @n @o) $
  withDict (rightPlusMinus @m @n) $
  withDict (leRightMinus @(m + n) @o @n) $
  dict

leftPlusLeRightMinus :: forall m n o. (m + n <= o) => Dict (n <= o - m)
leftPlusLeRightMinus = eraseEq $
  withDict (plusComm @m @n) $
  withDict (rightPlusLeRightMinus @n @m @o) $
  dict

rightPlusColeRightMinus :: forall m n o.
  (m <= n + o, o <= m) => Dict (m - o <= n)
rightPlusColeRightMinus = eraseEq $
  withDict (leRightMinus @m @(n + o) @o) $
  withDict (rightPlusMinus @n @o) $
  dict

leftPlusColeRightMinus :: forall m n o.
  (m <= n + o, n <= m) => Dict (m - n <= o)
leftPlusColeRightMinus = eraseEq $
  withDict (plusComm @n @o) $
  withDict (rightPlusColeRightMinus @m @o @n) $
  dict

complLe :: forall m n. (n <= m) => Dict (m - n <= m)
complLe = eraseEq $
  withDict (rightPlusExpans @m @n) $
  withDict (rightPlusColeRightMinus @m @m @n)
  dict

complMinus :: forall m n o.
  (o <= n, n <= m) => Dict ((m - (n - o)) ~ (m - n + o))
complMinus = eraseEq $
  withDict (rightMinusPlus @m @n) $
  withDict (rightMinusPlus @n @o) $
  withDict (plusAss @(m - n) @(n - o) @o) $
  withDict (rightPlusComm @(m - n) @(n - o) @o) $
  withDict (rightPlusMinus @(m - n + o) @(n - o)) $
  dict

complCompl :: forall m n.
  (n <= m) => Dict ((m - (m - n)) ~ n)
complCompl = eraseEq $
  withDict (leRefl @m) $
  withDict (complMinus @m @m @n) $
  withDict (minusRefl @m) $
  dict

leMinusSwap :: forall m n o.
  (o <= n, m <= n - o) => Dict (o <= n - m)
leMinusSwap = eraseEq $
  withDict (rightMinusPlus @n @o) $
  withDict (leRightPlus @m @(n - o) @o) $
  withDict (leftPlusLeRightMinus @m @o @n) $
  dict

rightPlusLeSame :: forall m n.
  (m + n <= m) => Dict (n ~ 0)
rightPlusLeSame = eraseEq $
  withDict (plusComm @m @n) $
  withDict (rightPlusLeRightMinus @n @m @m) $
  withDict (minusRefl @m) $
  withDict (leZero @n) $
  dict

rightPlusBothMinus :: forall m n o.
  (o <= m) => Dict ((m + n - (o + n)) ~ (m - o))
rightPlusBothMinus = eraseEq $
  withDict (leRightPlus @o @m @n) $
  withDict (rightMinusPlusComm @(m + n) @(o + n) @o) $
  withDict (plusAss @m @n @o) $
  withDict (plusComm @o @n) $
  withDict (rightPlusMinus @m @(n + o)) $
  withDict (rightMinusPlus @m @o) $
  withDict (rightPlusInj @(m + n - (o + n)) @(m - o) @o) $
  dict

rightPlusComplRightPlus :: forall m n o.
  (n + o <= m) => Dict ((m - (n + o) + o) ~ (m - n))
rightPlusComplRightPlus = eraseEq $
  withDict (rightMinusPlusComm @m @(n + o) @o) $
  withDict (leCoweakRightPlus @n @o @m) $
  withDict (rightPlusBothMinus @m @o @n) $
  dict

complPlus :: forall m n o.
  (n + o <= m) => Dict ((m - (n + o)) ~ (m - n - o))
complPlus = eraseEq $
  withDict (leftPlusLeRightMinus @n @o @m) $
  withDict (rightMinusPlus @(m - n) @o) $
  withDict (rightPlusComplRightPlus @m @n @o) $
  withDict (rightPlusInj @(m - (n + o)) @(m - n - o) @o) $
  dict

minusTrans :: forall m n o.
  (n <= m, o <= n) => Dict ((m - n + (n - o)) ~ (m - o))
minusTrans = eraseEq $
  withDict (plusAss @(m - n) @(n - o) @o) $
  withDict (rightMinusPlus @n @o) $
  withDict (rightMinusPlus @m @n) $
  withDict (rightPlusMinus @(m - n + (n - o)) @o) $
  dict

leCompl :: forall m n o.
  (n <= m, o <= n) => Dict (m - n <= m - o)
leCompl = eraseEq $
  withDict (minusTrans @m @n @o) $
  withDict (rightPlusExpans @(m - n) @(n - o)) $
  dict

coleCompl :: forall m n o.
  (o <= m, n <= m, m - n <= m - o) => Dict (o <= n)
coleCompl = eraseEq $
  withDict (complLe @m @o) $
  withDict (leCompl @m @(m - o) @(m - n)) $
  withDict (complCompl @m @o) $
  withDict (complCompl @m @n) $
  dict

rightMinusComm :: forall m n o.
  (n <= m, o <= m - n) => Dict ((m - n - o) ~ (m - o - n))
rightMinusComm = eraseEq $
  withDict (leRightPlus @o @(m - n) @n) $
  withDict (rightMinusPlus @m @n) $
  withDict (plusComm @n @o) $
  withDict (complPlus @m @n @o) $
  withDict (complPlus @m @o @n) $
  dict

complMinusCompl :: forall m n o.
  (o <= m, n <= o) => Dict ((m - n - (m - o)) ~ (o - n))
complMinusCompl = eraseEq $
  withDict (minusTrans @m @o @n) $
  withDict (rightPlusMinus @(o - n)) $
  withDict (leftPlusRightMinus @(m - o) @(o - n)) $
  dict

ltOrGeKnown :: forall m n. (KnownWord m, KnownWord n) =>
  Either (Dict (m + 1 <= n)) (Dict (n <= m))
ltOrGeKnown = case compareKnown @m @n of
  CmpLt Dict -> Left Dict
  CmpEq Dict -> withDict (leRefl @m) $ Right Dict
  CmpGt Dict -> withDict (leCoweakRightPlus @n @1 @m) $ Right Dict

leOrGtKnown :: forall m n. (KnownWord m, KnownWord n) =>
  Either (Dict (m <= n)) (Dict (n + 1 <= m))
leOrGtKnown = case ltOrGeKnown @n @m of
  Left Dict -> Right Dict
  Right Dict -> Left Dict

zeroOrPosKnown :: forall m. (KnownWord m) =>
  Either (Dict (m ~ 0)) (Dict (1 <= m))
zeroOrPosKnown = case leOrGtKnown @m @0 of
  Left Dict -> withDict (leZero @m) $ Left Dict
  Right Dict -> Right Dict

rightPlusMinusKnown :: forall m n.
  (KnownWord (m + n), KnownWord n) => Dict (KnownWord m)
rightPlusMinusKnown =
  withDict (leftPlusExpans @n @m) $
  withDict (minusKnown @(m + n) @n) $
  withDict (rightPlusMinus @m @n) $
  Dict

leftPlusMinusKnown :: forall m n.
  (KnownWord (m + n), KnownWord m) => Dict (KnownWord n)
leftPlusMinusKnown =
  withDict (plusComm @m @n) $
  rightPlusMinusKnown @n @m


plusK# :: forall m n. KWord# m -> KWord# n -> KWord# (m + n)
plusK# m# n# = case fromKWord# m# of
  WordDict -> case fromKWord# n# of
    WordDict -> withDict (plusKnown @m @n) (\ _ -> toKWord# WordDict) ()

minusK# :: forall m n. (n <= m) =>
  KWord# m -> KWord# n -> KWord# (m - n)
minusK# m# n# = case fromKWord# m# of
  WordDict -> case fromKWord# n# of
    WordDict -> withDict (minusKnown @m @n) (\ _ -> toKWord# WordDict) ()

compareK# :: forall m n. KWord# m -> KWord# n -> CmpOpt m n
compareK# m# n# = case fromKWord# m# of
  WordDict -> case fromKWord# n# of
    WordDict -> compareKnown

eqK# :: forall m n. KWord# m -> KWord# n -> Maybe (Dict (m ~ n))
eqK# m# n# = case fromKWord# m# of
  WordDict -> case fromKWord# n# of
    WordDict -> eqKnown @m @n

ltOrGeK# :: forall m n. KWord# m -> KWord# n ->
  Either (Dict (m + 1 <= n)) (Dict (n <= m))
ltOrGeK# m# n# = case fromKWord# m# of
  WordDict -> case fromKWord# n# of
    WordDict -> ltOrGeKnown @m @n

leOrGtK# :: forall m n. KWord# m -> KWord# n ->
  Either (Dict (m <= n)) (Dict (n + 1 <= m))
leOrGtK# m# n# = case fromKWord# m# of
  WordDict -> case fromKWord# n# of
    WordDict -> leOrGtKnown @m @n

zeroOrPosK# :: forall m. KWord# m ->
  Either (Dict (m ~ 0)) (Dict (1 <= m))
zeroOrPosK# m# = case fromKWord# m# of
  WordDict -> zeroOrPosKnown @m

rightPlusMinusK# :: forall m n.
  KWord# (m + n) -> KWord# n -> KWord# m
rightPlusMinusK# mn# n# = case fromKWord# mn# of
  WordDict -> case fromKWord# n# of
    WordDict -> withDict (rightPlusMinusKnown @m @n)
      (\ _ -> toKWord# WordDict) ()

leftPlusMinusK# :: forall m n.
  KWord# (m + n) -> KWord# m -> KWord# n
leftPlusMinusK# mn# m# = case fromKWord# mn# of
  WordDict -> case fromKWord# m# of
    WordDict -> withDict (leftPlusMinusKnown @m @n)
      (\ _ -> toKWord# WordDict) ()
