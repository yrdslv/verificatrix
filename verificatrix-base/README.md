        Verificatrix, proof assistant for mathematicians.
        (STILL VERY RUDIMENTARY! UNRELEASED!)
        Copyright (C) 2021, Yuri da Silva.

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Introduction

Verificatrix is a project dedicated to investigate proof assistants for
mathematicians.  The objective is to have a simple implementation,
understandable without much programming knowledge, and analyze how to bring
comfort for mathematics formalization.

This directory contains the source code of the “verificatrix-base”,
implementing a Martin–Löf type theory. I expect to implement later
“verificatrix-gtk”, a graphical interface.

This project is still in very rudimentary stage. To do the following.

* Implement level polymorphism.
* Explain better the source code.
* (Done.) Add quick Haskell introduction for mathematicians.
* Add specification of VERI format.
* Investigate ways to avoid bugs.
  * For now, I am using type-level naturals for scope.
* Investigate performance.
  * I am using explicit substitutions, which got more comfortable to use due to
    type-level naturals.
  * I tried to do term sharing.
  * Non rigorous analysis shows that Verificatrix reduces as fast as Coq
    Compute.
  * I am open to ideas.
* Correct Cabal metadata.
* Create “verificatrix-gtk” GUI.
  * Add support for VERI packages.
  * Support for documentation and notations.
  * Many gimmicks.
* ???

## Source code

Mathematicians which do not know Haskell shall read
[this brief explanation](src/Reading.md).

Read the source files in the order:

* [Veri.Dict](src/Veri/Dict.hs);
* [Veri.SmallArray](src/Veri/SmallArray.hs);
* [Veri.KnownWord](src/Veri/KnownWord.hs);
* [Veri.Dict.KnownWord](src/Veri/Dict/KnownWord.hs);
* [Veri.ZIneqs](src/Veri/ZIneqs.hs);
* [Veri.TC](src/Veri/TC.hs).

## Installation

This is a library, not a standalone program. After I correct the Cabal metadata
it will be simple command `cabal v2-install` after installing GHC and
dependencies.

